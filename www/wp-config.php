<?php
/**
 * La configuration de base de votre WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/Editing_wp-config.php Modifier
 * wp-config.php} (en anglais). Vous devez obtenir les codes MySQL de votre
 * hébergeur.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //

if ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "192.168.1.20") {
	define('DB_NAME', 'paysdenhautch');
	define('DB_USER', 'paysdenhautch');
	define('DB_PASSWORD', 'password');
	define('DB_HOST', 'localhost');
}
else {
	define('DB_NAME', 'opt_paysdenhautch');
	define('DB_USER', 'opt_pdhadm');
	define('DB_PASSWORD', 'Pdh$1660');
	define('DB_HOST', 'opt.myd.infomaniak.com');
	define('IS_PRODUCTION', true);
}

if (!defined('IS_PRODUCTION')) {
	define('IS_PRODUCTION', false);
}



/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/ Le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',        't/qxKjYd<qRgQuzVk<6rZ]nSa@6p6^tC5Uh2RMz=w^ g/1&zkG+8)1U~-U3>| kp');
define('SECURE_AUTH_KEY', '.|=%s.Gg74:& +^aZD`<??f {VO~yL~d=a[}xTK+ZIP!?EsD-,EPvm7+tqj@vz?V');
define('LOGGED_IN_KEY',   'vfdtD5s![ar2gfZay_%Ey!)4}H24WGdKfy5.;jR`dytAgPuUJzsm-7-vr,PS;p`7');
define('NONCE_KEY',       'sEz.d6Q@9gDOnKW-h.MIt5aT_-?Rjnou!LC7?M(WFi;|U.*|)*f&S@y1=YtrWmfs');
define('AUTH_SALT',        'GP+JQpCku!x-Ea+KX-^5#12HM#?YD2Whh]mM|}Q|;9ESipYd^eHp*fKb21;xMXS@');
define('SECURE_AUTH_SALT', 'C2W-s(|t0S`i-^=56<ny+n]:/Kn #Q!P(h q^ JlPA^(HR:>Y!-m*DwvFhqX4ROH');
define('LOGGED_IN_SALT',   '0=fF^CUGXe($%v*ZI8GDj%H_,Vp:Nh`)Irz_p9y7;3qcO~sb;SF.ygQZ-Xv~&u*I');
define('NONCE_SALT',       ';G!|Kli7<M^OTQe-+6lPP~hm[%MxbxdO?LXq9vx+nkBi|RxqpW$[unyv)4{+J+U|');


/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define ('WPLANG', 'fr_FR');


define('WP_MEMORY_LIMIT', '96M'); // This is the maximum Infomaniak allows.  See http://www.infomaniak.ch/support/faq/faq_home.php?language=french&faq=357

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
