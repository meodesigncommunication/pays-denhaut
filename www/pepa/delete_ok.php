<?php
	require "config.php";

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	if ($HTTP_POST_VARS{'type'} == "producteur") {
		$sqlquery= "SELECT id_photo FROM producteur_photo WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}

		$sqlquery= "DELETE FROM producteur_photo WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "SELECT produit_photo.id_photo FROM produit_photo, produits WHERE produit_photo.id_produit = produits.id AND produits.id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				$sqlquery3= "DELETE FROM produit_photo WHERE id_photo =" . $photo_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}

		$sqlquery= "SELECT exploitation_photo.id_photo FROM exploitation_photo, exploitations WHERE exploitation_photo.id_exploitation = exploitations.id AND exploitations.id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				$sqlquery3= "DELETE FROM exploitation_photo WHERE id_photo =" . $photo_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}

		$sqlquery= "SELECT activite_photo.id_photo FROM activite_photo, activites WHERE activite_photo.id_activite = activites.id AND activites.id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				$sqlquery3= "DELETE FROM activite_photo WHERE id_photo =" . $photo_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}
			
		$sqlquery= "DELETE FROM producteur_langue WHERE producteurs.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "SELECT logo FROM produits WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$logo = mysql_result($resultat_sql,$i,"logo");
				if ($logo) {
					unlink($chemin_logos_pepa.$logo);
					unlink($chemin_logos_pepa."originals/".$logo);
				}
			}
			
		$sqlquery= "SELECT id FROM produits WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$produit_id = mysql_result($resultat_sql,$i,"id");

				$sqlquery3= "DELETE FROM produit_label WHERE id_produit =" . $produit_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);
			}

		$sqlquery= "DELETE FROM produits WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "SELECT id FROM exploitations WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$exploitation_id = mysql_result($resultat_sql,$i,"id");

				$sqlquery3= "DELETE FROM exploitation_animal WHERE id_exploitation =" . $exploitation_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);
			}

		$sqlquery= "DELETE FROM exploitations WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "SELECT id FROM activites WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$activite_id = mysql_result($resultat_sql,$i,"id");

				$sqlquery3= "DELETE FROM activite_type WHERE id_activite =" . $activite_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				$sqlquery3= "DELETE FROM activite_exploitation WHERE id_activite =" . $activite_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				$sqlquery3= "DELETE FROM activite_vente_produit WHERE id_activite =" . $activite_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);

				$sqlquery3= "DELETE FROM activite_vente_producteur_pepa WHERE id_activite =" . $activite_id;
					$resultat_sql3 = mysql_query($sqlquery3,$connexion);
			}

		$sqlquery= "DELETE FROM activites WHERE id_producteur =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "SELECT logo FROM producteurs WHERE producteurs.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
			$logo = mysql_result($resultat_sql,0,"logo");
			if ($logo) {
				unlink($chemin_logos_pepa.$logo);
				unlink($chemin_logos_pepa."originals/".$logo);
			}

		$sqlquery= "DELETE FROM producteurs WHERE producteurs.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$msg = "producteur supprim�";
	}
	elseif ($HTTP_POST_VARS{'type'} == "produit") {
		$sqlquery= "SELECT id_photo FROM produit_photo WHERE id_produit =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}

		$sqlquery= "DELETE FROM produit_label WHERE id_produit =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM produit_photo WHERE id_produit =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "SELECT logo FROM produits WHERE produits.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
			$logo = mysql_result($resultat_sql,0,"logo");
			if ($logo) {
				unlink($chemin_logos_pepa.$logo);
				unlink($chemin_logos_pepa."originals/".$logo);
			}
			
		$sqlquery= "DELETE FROM produits WHERE produits.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$msg = "produit supprim�";
	}
	elseif ($HTTP_POST_VARS{'type'} == "exploitation") {
		$sqlquery= "SELECT id_photo FROM exploitation_photo WHERE id_exploitation =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}

		$sqlquery= "DELETE FROM activite_exploitation WHERE id_exploitation =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM exploitation_animal WHERE id_exploitation =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM exploitation_photo WHERE id_exploitation =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM exploitations WHERE exploitations.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$msg = "exploitation supprim�e";
	}
	elseif ($HTTP_POST_VARS{'type'} == "activite") {
		$sqlquery= "SELECT id_photo FROM activite_photo WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$photo_id = mysql_result($resultat_sql,$i,"id_photo");

				$sqlquery2= "SELECT fichier FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$photo = mysql_result($resultat_sql2,0,"fichier");

				$sqlquery2= "DELETE FROM photos WHERE photos.id =" . $photo_id ;
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

				if ($photo) {
					unlink($chemin_photos_pepa.$photo);
					unlink($chemin_photos_pepa."thumbs/".$photo);
					unlink($chemin_photos_pepa."originals/".$photo);
				}
			}
			
		$sqlquery= "DELETE FROM activite_type WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM activite_exploitation WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM activite_vente_produit WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM activite_vente_producteur_pepa WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM activite_photo WHERE id_activite =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM activites WHERE activites.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$msg = "activit� supprim�e";
	}
	elseif ($HTTP_POST_VARS{'type'} == "photo") {
		$sqlquery= "SELECT fichier FROM photos WHERE id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$photo = mysql_result($resultat_sql,0,"fichier");

			if ($photo) {
				unlink($chemin_photos_pepa.$photo);
				unlink($chemin_photos_pepa."thumbs/".$photo);
				unlink($chemin_photos_pepa."originals/".$photo);
			}

		$sqlquery= "DELETE FROM producteur_photo WHERE id_photo =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM produit_photo WHERE id_photo =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM exploitation_photo WHERE id_photo =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$sqlquery= "DELETE FROM activite_photo WHERE id_photo =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
			
		$sqlquery= "DELETE FROM photos WHERE photos.id =" . $HTTP_POST_VARS{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

		$msg = "photo supprim�e";
	}
	else {
		mysql_close($connexion);
	}

	mysql_close($connexion);

	Header ("Location: ".$HTTP_POST_VARS{'referer'}."&msg=".$msg);
	exit();
?>