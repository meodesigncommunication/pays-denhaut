<?php
	require "config.php";

	$url_referer = parse_url($_SERVER['HTTP_REFERER']);

	if ($url_referer[path] == "/pepa/listes.php" ) {
		$referer = $_SERVER['HTTP_REFERER'];
	}
	else {
		$referer = $HTTP_POST_VARS['referer'];
	}

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	// Chargement des langues
	$sqlquery= "SELECT id, langue FROM producteurs_langues";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteurs_langues = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_producteurs_langues; $i++) {
			$producteurs_langues_id[$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_langues[$i] = mysql_result($resultat_sql,$i,"langue");
		}

	// Geestion de l'�dition d'un producteur existant
	if ($_GET{'action'} == "edit") {
		$sqlquery= "SELECT * FROM producteurs WHERE producteurs.id =" . $_GET{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);
	
			$nom = mysql_result($resultat_sql,0,"nom");
			$prenom = mysql_result($resultat_sql,0,"prenom");
			$pepa = mysql_result($resultat_sql,0,"pepa");
			$description_courte = mysql_result($resultat_sql,0,"description_courte");
			$description = mysql_result($resultat_sql,0,"description");
			$responsable = mysql_result($resultat_sql,0,"responsable");
			$rue = mysql_result($resultat_sql,0,"rue");
			$no_postal = mysql_result($resultat_sql,0,"no_postal");
			$ville = mysql_result($resultat_sql,0,"ville");
			$tel = mysql_result($resultat_sql,0,"tel");
			$fax = mysql_result($resultat_sql,0,"fax");
			$mobile = mysql_result($resultat_sql,0,"mobile");
			$email = mysql_result($resultat_sql,0,"email");
			$site_web = mysql_result($resultat_sql,0,"site_web");
			$vente_correspondance = mysql_result($resultat_sql,0,"vente_correspondance");
			$fonction = mysql_result($resultat_sql,0,"fonction");
			$ambassadeur = mysql_result($resultat_sql,0,"ambassadeur");
			$logo = mysql_result($resultat_sql,0,"logo");
			$id = $_GET{'id'};

			$sqlquery= "SELECT id_langue FROM producteur_langue WHERE id_producteur=".$_GET{'id'};
				$resultat_sql = mysql_query($sqlquery,$connexion);
				$nbrlignes_producteur_langue = mysql_num_rows($resultat_sql);

				for ($i=0; $i<$nbrlignes_producteur_langue; $i++) {
					$producteur_langue_id[$i] = mysql_result($resultat_sql,$i,"id_langue");
					$producteur_langue[$producteur_langue_id[$i]] = " selected";
				}

		$sqlquery= "SELECT * FROM producteur_photo WHERE id_producteur=" . $_GET{'id'} . " ORDER BY ordre, id_photo";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes_producteur_photo = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes_producteur_photo; $i++) {

				$producteur_photo_id[$i] = mysql_result($resultat_sql,$i,"id_photo");
				$sqlquery2 = "SELECT fichier, width_miniature, height_miniature FROM photos WHERE id=" . $producteur_photo_id[$i];
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$producteur_photo[$i] = mysql_result($resultat_sql2,0,"fichier");
					$producteur_photo_width[$i] = mysql_result($resultat_sql2,0,"width_miniature");
					$producteur_photo_height[$i] = mysql_result($resultat_sql2,0,"height_miniature");

			}

			$msg = "Modification du producteur ".$nom.". ";
			$msg .= $_GET{'msg'};

	}
	
	// G�n�rer un formulaire vierge
	else {
			$nom = "";
			$prenom = "";
			$pepa = "";
			$description_courte = "";
			$description = "";
			$responsable = "";
			$rue = "";
			$no_postal = "";
			$ville = "";
			$tel = "";
			$fax = "";
			$mobile = "";
			$email = "";
			$site_web = "";
			$vente_correspondance = "";
			$fonction = "producteur";
			$ambassadeur = "";
			$logo = "";
			$id = "";

			$msg = "Nouveau producteur/commerce :";
	}

	// Sauvegarde d'un nouveau producteur :
	if ($_GET{'action'} == "save") {
		// On g�re le remplacement �ventuel du logo
		if ($HTTP_POST_VARS['suppr_logo'] == "oui") {
			unlink($chemin_logos_pepa.$HTTP_POST_VARS['old_logo']);
			unlink($chemin_logos_pepa."originals/".$HTTP_POST_VARS['old_logo']);
			$chemin  = "";
		}
		else {
			if ($HTTP_POST_VARS['old_logo']) {
				$chemin  = $HTTP_POST_VARS['old_logo'];
			}
			else {
				$chemin  = "";
			}
		}

		if (isset($_FILES['logo']['tmp_name']) AND ($_FILES['logo']['error'] == UPLOAD_ERR_OK)) {
			$nom_logo = rand() . $_FILES['logo']['name'];
			$chemin_original = $chemin_logos_pepa . "originals/" . $nom_logo;
			$chemin = $chemin_logos_pepa . $nom_logo;
			move_uploaded_file($_FILES['logo']['tmp_name'], $chemin_original);
			
			// cr�ation de l'image r�duite :
			$info=getimagesize($chemin_original);

			$w_max="300"; 
			$h_max="300"; 

			$coef=min($w_max/$info[0],$h_max/$info[1]);
			$width=round($info[0]*$coef);
			$height=round($info[1]*$coef);

			if ($info[2]=="1") {
				$img=imagecreatefromgif($chemin_original);
			}
			else if ($info[2]=="2") {
				$img=imagecreatefromjpeg($chemin_original);
			}
			else if ($info[2]=="3") {
				$img=imagecreatefrompng($chemin_original);
			}

			$logo_def=imagecreatetruecolor($width,$height);
			ImageCopyResampled($logo_def,$img,0,0,0,0,$width,$height,$info[0],$info[1]);
			imagejpeg($logo_def,$chemin);

			if ($HTTP_POST_VARS['old_logo'] != "") {

				unlink($chemin_logos_pepa.$HTTP_POST_VARS['old_logo']);
				unlink($chemin_logos_pepa."originals/".$HTTP_POST_VARS['old_logo']);
			}
		}


		$site_internet  = addslashes($HTTP_POST_VARS['site_web']);


		if ($HTTP_POST_VARS['id'] == "") {
			$sqlquery= "INSERT INTO producteurs (nom, prenom, description, description_courte, responsable, rue, no_postal, ville, tel, fax, mobile, email, site_web, pepa, fonction, ambassadeur, vente_correspondance, logo, date_creation, date_modification) VALUES ('".addslashes($HTTP_POST_VARS['nom'])."', '".addslashes($HTTP_POST_VARS['prenom'])."', '".addslashes($HTTP_POST_VARS['description'])."', '".addslashes($HTTP_POST_VARS['description_courte'])."', '".addslashes($HTTP_POST_VARS['responsable'])."', '".addslashes($HTTP_POST_VARS['rue'])."', '".addslashes($HTTP_POST_VARS['no_postal'])."', '".addslashes($HTTP_POST_VARS['ville'])."', '".addslashes($HTTP_POST_VARS['tel'])."', '".addslashes($HTTP_POST_VARS['fax'])."', '".addslashes($HTTP_POST_VARS['mobile'])."', '".addslashes($HTTP_POST_VARS['email'])."', '".$site_internet."', '".addslashes($HTTP_POST_VARS['pepa'])."', '".addslashes($HTTP_POST_VARS['fonction'])."', '".addslashes($HTTP_POST_VARS['ambassadeur'])."', '".addslashes($HTTP_POST_VARS['vente_correspondance'])."', '".$nom_logo."', CURDATE(), CURDATE())";
				$resultat_sql = mysql_query($sqlquery,$connexion);
				$new_id = mysql_insert_id();

			$msg = "Nouveau producteur enregistr� (".$HTTP_POST_VARS['nom']."). Utilisez le formulaire ci-dessous pour en ajouter un suppl�mentaire.";
		}
		else {
			$sqlquery= "UPDATE producteurs SET nom='".addslashes($HTTP_POST_VARS['nom'])."', prenom='".addslashes($HTTP_POST_VARS['prenom'])."', description='".addslashes($HTTP_POST_VARS['description'])."', description_courte='".addslashes($HTTP_POST_VARS['description_courte'])."', responsable='".addslashes($HTTP_POST_VARS['responsable'])."', rue='".addslashes($HTTP_POST_VARS['rue'])."', no_postal='".addslashes($HTTP_POST_VARS['no_postal'])."', ville='".addslashes($HTTP_POST_VARS['ville'])."', tel='".addslashes($HTTP_POST_VARS['tel'])."', fax='".addslashes($HTTP_POST_VARS['fax'])."', mobile='".addslashes($HTTP_POST_VARS['mobile'])."', email='".addslashes($HTTP_POST_VARS['email'])."', site_web='".$site_internet."', pepa='".addslashes($HTTP_POST_VARS['pepa'])."', fonction='".addslashes($HTTP_POST_VARS['fonction'])."', ambassadeur='".addslashes($HTTP_POST_VARS['ambassadeur'])."', vente_correspondance='".addslashes($HTTP_POST_VARS['vente_correspondance'])."', logo='".$nom_logo."', date_modification=CURDATE() WHERE id='".addslashes($HTTP_POST_VARS['id'])."'";
				$resultat_sql = mysql_query($sqlquery,$connexion);
				$new_id = $HTTP_POST_VARS['id'];

			// Suppression des �ventuelles langues actuelles du producteur
			$sqlquery= "DELETE FROM producteur_langue WHERE id_producteur='".$new_id."'";
				$resultat_sql = mysql_query($sqlquery,$connexion);

			$msg = "Producteur \"".$HTTP_POST_VARS['nom']."\" mis � jour.";
		}

		// Ajout des nouvelles langues
		if ($HTTP_POST_VARS['id_langue']) {
			foreach($HTTP_POST_VARS['id_langue'] as $id_lang) {
				$sqlquery= "INSERT INTO producteur_langue (id_producteur, id_langue) VALUES ('".$new_id."', '".$id_lang."')";
					$resultat_sql = mysql_query($sqlquery,$connexion);
			}
		}

		if ($HTTP_POST_VARS['ordre']) {
			foreach(array_keys($HTTP_POST_VARS['ordre']) as $id_ordre) {
				if ($HTTP_POST_VARS['ordre'][$id_ordre] != "") {
					$sqlquery= "UPDATE producteur_photo SET ordre = '".$HTTP_POST_VARS['ordre'][$id_ordre]."' WHERE id_producteur = '".$new_id."' AND id_photo = '".$id_ordre."'";
						$resultat_sql = mysql_query($sqlquery,$connexion);
				}
			}
		}

		// Gestion des redirections
		if ($referer != "" AND $HTTP_POST_VARS['id'] != "") {
			Header ("Location: ".$referer."&msg=".$msg);
			exit();
		}
	}	

	mysql_close($connexion);

?>

<html>

<head>
<meta http-equiv="Content-Language" content="fr-ch">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Gestion des producteurs</title>
<script language="JavaScript">
<!--
	function afficheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='visible';
			document.getElementById(baliseId).style.display='block';
		}
	}

	function cacheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='hidden';
			document.getElementById(baliseId).style.display='none';
		}
	}


	function check_fonction() {

		if (document.formulaire.fonction[0].checked == true) {
			document.formulaire.ambassadeur[0].checked=false;
			document.formulaire.ambassadeur[1].checked=true;

			afficheId('divpepa');
			cacheId('ambassad');
		}
		else {
			document.formulaire.pepa[0].checked=false;
			document.formulaire.pepa[1].checked=true;

			cacheId('divpepa');
			afficheId('ambassad');
		}
	}

	function parametre(param) {
		var top=(screen.height-400)/2;
		var left=(screen.width-280)/2;

		window.open("parametres.php?parametre="+param, "parametre", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=yes, resizable=yes, width=280, height=400");
	}

	function confirm(type,id) {
		var top=(screen.height-150)/2;
		var left=(screen.width-400)/2;
		window.name = "Listes";

		window.open("delete.php?type="+type+"&id="+id, "delete", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=no, resizable=no, width=400, height=150");
	}

	function test_submit(){
		var ok = "oui";

		if (document.formulaire.nom.value == '') {
			ok = "non";
		}
		if (document.formulaire.no_postal.value == '') {
			ok = "non";
		}
		if (document.formulaire.ville.value == '') {
			ok = "non";
		}

		if (ok != "oui") {
			alert("Tous les champs obligatoirs n'ont pas �t� compl�t�s !");
			return false;
		}
		else {
			return true;
		}
	}

 -->
</script>
<style type="text/css">
<!-- 
p {font-family:Arial Narrow, font-size:2; color:black;}
a {font-family:Arial Narrow, font-size:2; color:black; text-decoration:underline; }
td {font-family:Arial Narrow, font-size:2; color:black;}
 -->
</style>
</head>

<body onload="javascript:check_fonction();">

<form method="POST" enctype="multipart/form-data" action="producteurs.php?action=save" name="formulaire" onsubmit="return test_submit();">
<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; border: 1px dotted #0000FF" bordercolor="#111111" width="957">
  <tr>
    <td width="250" style="border-collapse: collapse; border: 1px dotted #0000FF" align="left" valign="top"><?php

include "menu.php";

?></td>
    <td width="700" style="border-collapse: collapse; border: 1px dotted #0000FF"><b><font face="Arial Narrow">
  <table width="700">
    <tr>
      <td width="700" colspan="2" align="left" valign="top"><b><?php echo $msg; ?></b></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Nom du producteur / soci�t� : </td>
      <td width="450" align="left" valign="top"><input type="text" name="nom" value="<?php echo $nom; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Pr�nom : </td>
      <td width="450" align="left" valign="top"><input type="text" name="prenom" value="<?php echo $prenom; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Fonction : </td>
      <td width="450" align="left" valign="top"><input type="radio" value="producteur" name="fonction" <?php if ($fonction == "producteur") {echo "checked";} ?> onclick="javascript:check_fonction();">producteur <input type="radio" name="fonction" value="commerce" <?php if ($fonction == "commerce") {echo "checked";} ?> onclick="javascript:check_fonction();">commerce  <input type="radio" name="fonction" value="restaurant" <?php if ($fonction == "restaurant") {echo "checked";} ?> onclick="javascript:check_fonction();">restaurant</td>
    </tr>
  </table>
<div id="divpepa">
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Producteur Pays-d'Enhaut Produit Authentique : </td>
      <td width="450" align="left" valign="top"><input type="radio" value="oui" name="pepa" <?php if ($pepa == "oui") {echo "checked";} ?>>oui <input type="radio" name="pepa" value="non" <?php if ($pepa != "oui") {echo "checked";} ?>>non</td>
    </tr>
  </table>
</div>
<div id="ambassad">
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Ambassadeur PEPA : </td>
      <td width="450" align="left" valign="top"><input type="radio" value="oui" name="ambassadeur" <?php if ($ambassadeur == "oui") {echo "checked";} ?>>oui <input type="radio" name="ambassadeur" value="non" <?php if ($ambassadeur != "oui") {echo "checked";} ?>>non</td>
    </tr>
  </table>
</div>
  <table>
    <tr>
      <td width="250" align="right" valign="top">Description courte (chapeau) : <br><i>(<a href="code.html" target="_blank">r�gles de mise en forme</a>)</i></td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="description_courte" cols="40"><?php echo $description_courte; ?></textarea></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Description : </td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="description" cols="40"><?php echo $description; ?></textarea></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Langue(s) <br><i>(choix multiple avec le touche Ctrl)</i> : </td>
      <td width="450" align="left" valign="top"><select size="7" id="id_lang" name="id_langue[]" style="width:330px" multiple>
<?php
	for ($i=0; $i<$nbrlignes_producteurs_langues; $i++) {

		echo "      <option value=\"".$producteurs_langues_id[$i]."\"". $producteur_langue[$producteurs_langues_id[$i]].">".$producteurs_langues[$i]."<br>\n";

	}
?>
      </select> (<a href="#" onclick="javascript:parametre('producteurs_langues');">�diter la liste</a>)</td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Personne responsable <i>(si diff�rent du nom/pr�nom du producteur)</i> : </td>
      <td width="450" align="left" valign="top"><input type="text" name="responsable" value="<?php echo $responsable; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Rue : </td>
      <td width="450" align="left" valign="top"><input type="text" name="rue" value="<?php echo $rue; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* N�postal : </td>
      <td width="450" align="left" valign="top"><input type="text" name="no_postal" value="<?php echo $no_postal; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Ville : </td>
      <td width="450" align="left" valign="top"><input type="text" name="ville" value="<?php echo $ville; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">T�l�phone : </td>
      <td width="450" align="left" valign="top"><input type="text" name="tel" value="<?php echo $tel; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Fax : </td>
      <td width="450" align="left" valign="top"><input type="text" name="fax" value="<?php echo $fax; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Mobile : </td>
      <td width="450" align="left" valign="top"><input type="text" name="mobile" value="<?php echo $mobile; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">E-mail : </td>
      <td width="450" align="left" valign="top"><input type="text" name="email" value="<?php echo $email; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Site internet : </td>
      <td width="450" align="left" valign="top">http://<input type="text" name="site_web" value="<?php echo $site_web; ?>" size="45"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Vente par correspondance : </td>
      <td width="450" align="left" valign="top"><input type="radio" value="oui" name="vente_correspondance" <?php if ($vente_correspondance == "oui") {echo "checked";} ?>>oui <input type="radio" name="vente_correspondance" value="non" <?php if ($vente_correspondance != "oui") {echo "checked";} ?>>non</td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Logo <i>(image jpg, gif ou png, de taille minimale de 300 pixels, sur fond blanc)</i> : </td>
      <td width="450" align="left" valign="top">
<?php
	if ($logo != "") {
		echo "        <img src=\"" . $chemin_logos_pepa_http . $logo . "\" border=\"0\"><br><input type=\"checkbox\" name=\"suppr_logo\" Value=\"oui\"> Supprimer le logo actuel<br><input type=\"hidden\" name=\"old_logo\" value=\"".$logo."\"><br>Remplacer le logo :\n";
	}
?>
        <input type="file" name="logo" size="50">
      </td>
    </tr>
  </table>
<?php
	if ($nbrlignes_producteur_photo != 0) {
		echo "  <table width=\"700\">\n";
		echo "    <tr>\n";
		echo "      <td width=\"250\" align=\"right\" valign=\"top\">Photo(s) : </td>\n";
		echo "      <td width=\"450\" align=\"left\" valign=\"top\">\n";

		for ($i=0; $i<$nbrlignes_producteur_photo; $i++) {
			echo "        <img src=\"" . $chemin_photos_pepa_http . "thumbs/".$producteur_photo[$i]."\" border=\"0\" width=\"".$producteur_photo_width[$i]."\" height=\"".$producteur_photo_height[$i]."\"> position n�<input type=\"texte\" name=\"ordre[".$producteur_photo_id[$i]."]\" value=\"".($i+1)."\" size=\"1\"> (<a href=\"photos_edit.php?id=".$producteur_photo_id[$i]."\">editer</a>, <a href=\"#\" onclick=\"javascript:confirm('photo','".$producteur_photo_id[$i]."')\">supprimer</a>)<br>\n";
		}

		echo "      </td>\n";
		echo "    </tr>\n";
		echo "  </table>\n";
	}
?>
  <p align="center"><input type="hidden" value="<?php echo $id; ?>" name="id"><input type="hidden" value="<?php echo $referer; ?>" name="referer"><input type="submit" value="Enregister" name="enregistrer"></p>
    </td>
  </tr>
</table>
</form>

</body>

</html>