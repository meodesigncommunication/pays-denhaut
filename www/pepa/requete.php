<?php

// cr�ation de la/des requ�te(s)

	$requete = "SELECT ".$HTTP_POST_VARS['tables'].".id";

	$tables = "";
	$where = "";
	$join = "";

	$tableau = Array();

	if ($HTTP_POST_VARS['fields']) {
		foreach($HTTP_POST_VARS['fields'] as $field) {

			if ($field == "exploitations.id_commune") {

				$requete .= ", exploitations_communes.commune AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN exploitations_communes ON exploitations.id_commune = exploitations_communes.id";
			}
			elseif ($field == "produits.id_type") {

				$requete .= ", produits_types.type AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_types ON produits.id_type = produits_types.id";
			}
			elseif ($field == "produits.id_lait_type") {

				$requete .= ", produits_lait_types.type AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_lait_types ON produits.id_lait_type = produits_lait_types.id";
			}
			elseif ($field == "produits.id_lait_type_lait") {

				$requete .= ", produits_lait_types_laits.type_lait AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_lait_types_laits ON produits.id_lait_type_lait = produits_lait_types_laits.id";
			}
			elseif ($field == "produits.id_lait_traitement_lait") {

				$requete .= ", produits_lait_traitements_laits.traitement_lait AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_lait_traitements_laits ON produits.id_lait_traitement_lait = produits_lait_traitements_laits.id";
			}
			elseif ($field == "produits.id_viande_type") {

				$requete .= ", produits_viande_types.type AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_viande_types ON produits.id_viande_type = produits_viande_types.id";
			}
			elseif ($field == "produits.id_viande_conditionnement") {

				$requete .= ", produits_viande_conditionnements.conditionnement AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_viande_conditionnements ON produits.id_viande_conditionnement = produits_viande_conditionnements.id";
			}
			elseif ($field == "produits.id_marche") {

				$requete .= ", produits_marches.marche AS \"" . $field . "\"";
				$join .= " LEFT OUTER JOIN produits_marches ON produits.id_marche = produits_marches.id";
			}
			elseif (($field == "exploitation_activite") OR ($field == "activite_exploitation") OR ($field == "activite_type") OR ($field == "activite_vente_producteur_pepa") OR ($field == "activite_vente_produit") OR ($field == "exploitation_animal") OR ($field == "producteur_langue")) {
				list($table_id) = split('[_]', $field);
				$requete .= ", ".$table_id."s.id AS \"". $field . "\"";
			}
			elseif ($field == "produit_label") {
				list($table_id) = split('[_]', $field);
				$requete .= ", ".$table_id."s.id AS \"". $field . "\", produits.autre_label AS \"produits.autre_label\"";
			}
			else {
				$requete .= ", " . $field . " AS \"". $field . "\"";
			}
		}
	}

	if ($HTTP_POST_VARS['tables'] == "producteurs") {
		$tables = " producteurs";
	}
	elseif ($HTTP_POST_VARS['tables'] == "produits") {
		$tables = " produits, producteurs";
		$where = " WHERE produits.id_producteur = producteurs.id";
	}
	elseif ($HTTP_POST_VARS['tables'] == "exploitations") {
		$tables = " exploitations, producteurs";
		$where = " WHERE exploitations.id_producteur = producteurs.id";
	}
	elseif ($HTTP_POST_VARS['tables'] == "activites") {
		$tables = " activites, producteurs";
		$where = " WHERE activites.id_producteur = producteurs.id";
	}

	if ($HTTP_POST_VARS['condition_champ']) {

		$check = "non";
		
		for ($i=0; $i<count($HTTP_POST_VARS['condition_champ']); $i++) {
			if ($HTTP_POST_VARS['condition_champ'][$i] != "0") {

				if ($check == "non") {
					if ($where == "") {
						$where .= " WHERE ";
					}
					else {
						$where .= " AND ";
					}
				}
				else {
					$where .= " AND ";
				}
				
				if ($HTTP_POST_VARS['condition_champ'][$i] == "producteur_langue") {

					$where .= "producteur_langue.id_langue".$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
					$join .= " LEFT OUTER JOIN producteur_langue ON producteurs.id = producteur_langue.id_producteur";
				}
				elseif ($HTTP_POST_VARS['condition_champ'][$i] == "produit_label") {

					$where .= "produit_label.id_label".$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
					$join .= " LEFT OUTER JOIN produit_label ON produits.id = produit_label.id_produit";
				}
				elseif ($HTTP_POST_VARS['condition_champ'][$i] == "exploitation_animal") {

					$where .= "exploitation_animal.id_animal".$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
					$join .= " LEFT OUTER JOIN exploitation_animal ON exploitations.id = exploitation_animal.id_exploitation";
				}
				elseif ($HTTP_POST_VARS['condition_champ'][$i] == "activite_type") {

					$where .= "activite_type.id_type".$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
					$join .= " LEFT OUTER JOIN activite_type ON activites.id = activite_type.id_activite";
				}
				elseif ($HTTP_POST_VARS['condition_champ'][$i] == "activite_vente_producteur_pepa") {

					$where .= "activite_vente_producteur_pepa.id_producteur_pepa".$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
					$join .= " LEFT OUTER JOIN activite_vente_producteur_pepa ON activites.id = activite_vente_producteur_pepa.id_activite";
				}
				else {
					$where .= $HTTP_POST_VARS['condition_champ'][$i].$HTTP_POST_VARS['condition_operateur'][$i]."\"".$HTTP_POST_VARS['condition_liste'][$i]."\"";
				}

				$check = "oui";
			}
		}
	}


	$requete .= " FROM" . $tables . $join . $where;

	$l = 0;

	if ($HTTP_POST_VARS['orders']) {
		foreach($HTTP_POST_VARS['orders'] as $order) {
			if ($l == 0) {
				$requete .= " ORDER BY ".$order;
			}
			else {
				$requete .= ", ".$order;
			}
			$l++;
		}
	}
?>