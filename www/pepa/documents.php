<?php
	require "config.php";

	include 'champs_attributs.php';

	$champs_conditions = Array(

		'producteurs.nom',
		'producteurs.pepa',
		'producteurs.fonction',
		'producteurs.ambassadeur',
		'producteur_langue',
		'producteurs.ville',
		'producteurs.vente_correspondance',
		'produit_label',
		'produits.id_type',
		'produits.id_lait_type',
		'produits.id_lait_type_lait',
		'produits.id_lait_traitement_lait',
		'produits.id_viande_type',
		'produits.id_viande_conditionnement',
		'produits.id_marche',
		'produits.produit_phare',
		'exploitations.alpage',
		'exploitations.etivaz',
		'exploitations.altitude',
		'exploitation_animal',
		'exploitations.id_commune',
		'exploitations.handicapes',
		'activite_type',
		'activite_vente_producteur_pepa',
		'activites.vente_producteur_non_pepa'
	);


	foreach (array_keys($width_champs) as $width) {
		$width_champs[$width] = $width_champs[$width] * 5;
	}

	$arr_tables_values = "";
	$arr_tables_texts = "";
	$i = 0;

	foreach (array_keys($titres_tables) as $var) {
		if ($i == 0) {
			$arr_tables_texts .= "\"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= "\"" . $var . "\"";
		}
		else {
			$arr_tables_texts .= ", \"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= ", \"" . $var . "\"";
		}

		$i++;
	}


	$arr_producteurs_values = "";
	$arr_producteurs_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_producteurs) as $var) {
		if ($i == 0) {
			$arr_producteurs_texts .= "\"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= "\"" . $var . "\"";
		}
		else {
			$arr_producteurs_texts .= ", \"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_produits_values = "";
	$arr_produits_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_produits) as $var) {
		if ($i == 0) {
			$arr_produits_texts .= "\"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= "\"" . $var . "\"";
		}
		else {
			$arr_produits_texts .= ", \"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_exploitations_values = "";
	$arr_exploitations_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_exploitations) as $var) {
		if ($i == 0) {
			$arr_exploitations_texts .= "\"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= "\"" . $var . "\"";
		}
		else {
			$arr_exploitations_texts .= ", \"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_activites_values = "";
	$arr_activites_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_activites) as $var) {
		if ($i == 0) {
			$arr_activites_texts .= "\"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= "\"" . $var . "\"";
		}
		else {
			$arr_activites_texts .= ", \"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_champs_conditions = "";
	$i = 0;

	foreach ($champs_conditions as $var) {
		if ($i == 0) {
			$arr_champs_conditions .= "\"" . $var . "\"";
		}
		else {
			$arr_champs_conditions .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

////////
	$conditions_listes_producteurs_nom_values  = "";

	$sqlquery= "SELECT nom FROM producteurs ORDER BY pepa, nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_producteurs_nom_values .= ", ";
			}
			
			$conditions_listes_producteurs_nom_values .= "\"" . mysql_result($resultat_sql,$i,"nom") . "\"";
		}

////////
	$conditions_listes_producteurs_pepa_values  = "\"oui\", \"non\"";

////////
	$conditions_listes_producteurs_fonction_values  = "\"producteur\", \"commerce\", \"restaurant\"";

////////
	$conditions_listes_producteurs_ambassadeur_values  = "\"oui\", \"non\"";


////////
	$conditions_listes_producteur_langue_texts  = "";
	$conditions_listes_producteur_langue_values  = "";

	$sqlquery= "SELECT id, langue FROM producteurs_langues";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_producteur_langue_texts .= ", ";
				$conditions_listes_producteur_langue_values .= ", ";
			}

			$conditions_listes_producteur_langue_texts .= "\"" . mysql_result($resultat_sql,$i,"langue") . "\"";
			$conditions_listes_producteur_langue_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}

////////
	$conditions_listes_producteurs_ville_values  = "";

	$sqlquery= "SELECT DISTINCT ville FROM producteurs ORDER BY ville";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_producteurs_ville_values .= ", ";
			}
			
			$conditions_listes_producteurs_ville_values .= "\"" . mysql_result($resultat_sql,$i,"ville") . "\"";
		}

////////
	$conditions_listes_producteurs_vente_correspondance_values  = "\"oui\", \"non\"";


////////
	$conditions_listes_produit_label_texts  = "";
	$conditions_listes_produit_label_values  = "";

	$sqlquery= "SELECT id, label FROM produits_labels";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produit_label_texts .= ", ";
				$conditions_listes_produit_label_values .= ", ";
			}

			$conditions_listes_produit_label_texts .= "\"" . mysql_result($resultat_sql,$i,"label") . "\"";
			$conditions_listes_produit_label_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_type_texts  = "";
	$conditions_listes_produits_id_type_values  = "";

	$sqlquery= "SELECT id, type FROM produits_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_type_texts .= ", ";
				$conditions_listes_produits_id_type_values .= ", ";
			}

			$conditions_listes_produits_id_type_texts .= "\"" . mysql_result($resultat_sql,$i,"type") . "\"";
			$conditions_listes_produits_id_type_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_lait_type_texts  = "";
	$conditions_listes_produits_id_lait_type_values  = "";

	$sqlquery= "SELECT id, type FROM produits_lait_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_lait_type_texts .= ", ";
				$conditions_listes_produits_id_lait_type_values .= ", ";
			}

			$conditions_listes_produits_id_lait_type_texts .= "\"" . mysql_result($resultat_sql,$i,"type") . "\"";
			$conditions_listes_produits_id_lait_type_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_lait_type_lait_texts  = "";
	$conditions_listes_produits_id_lait_type_lait_values  = "";

	$sqlquery= "SELECT id, type_lait FROM produits_lait_types_laits";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_lait_type_lait_texts .= ", ";
				$conditions_listes_produits_id_lait_type_lait_values .= ", ";
			}

			$conditions_listes_produits_id_lait_type_lait_texts .= "\"" . mysql_result($resultat_sql,$i,"type_lait") . "\"";
			$conditions_listes_produits_id_lait_type_lait_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_lait_traitement_lait_texts  = "";
	$conditions_listes_produits_id_lait_traitement_lait_values  = "";

	$sqlquery= "SELECT id, traitement_lait FROM produits_lait_traitements_laits";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_lait_traitement_lait_texts .= ", ";
				$conditions_listes_produits_id_lait_traitement_lait_values .= ", ";
			}

			$conditions_listes_produits_id_lait_traitement_lait_texts .= "\"" . mysql_result($resultat_sql,$i,"traitement_lait") . "\"";
			$conditions_listes_produits_id_lait_traitement_lait_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_viande_type_texts  = "";
	$conditions_listes_produits_id_viande_type_values  = "";

	$sqlquery= "SELECT id, type FROM produits_viande_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_viande_type_texts .= ", ";
				$conditions_listes_produits_id_viande_type_values .= ", ";
			}

			$conditions_listes_produits_id_viande_type_texts .= "\"" . mysql_result($resultat_sql,$i,"type") . "\"";
			$conditions_listes_produits_id_viande_type_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_viande_conditionnement_texts  = "";
	$conditions_listes_produits_id_viande_conditionnement_values  = "";

	$sqlquery= "SELECT id, conditionnement FROM produits_viande_conditionnements";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_viande_conditionnement_texts .= ", ";
				$conditions_listes_produits_id_viande_conditionnement_values .= ", ";
			}

			$conditions_listes_produits_id_viande_conditionnement_texts .= "\"" . mysql_result($resultat_sql,$i,"conditionnement") . "\"";
			$conditions_listes_produits_id_viande_conditionnement_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_produits_id_marche_texts  = "";
	$conditions_listes_produits_id_marche_values  = "";

	$sqlquery= "SELECT id, marche FROM produits_marches";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_produits_id_marche_texts .= ", ";
				$conditions_listes_produits_id_marche_values .= ", ";
			}

			$conditions_listes_produits_id_marche_texts .= "\"" . mysql_result($resultat_sql,$i,"marche") . "\"";
			$conditions_listes_produits_id_marche_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}

////////
	$conditions_listes_produits_produit_phare_values  = "\"oui\", \"non\"";

////////
	$conditions_listes_exploitations_alpage_values  = "\"oui\", \"non\"";

////////
	$conditions_listes_exploitations_etivaz_values  = "\"oui\", \"non\"";

////////
	$conditions_listes_exploitations_altitude_values  = "\"1200\", \"1250\", \"1300\", \"1350\", \"1400\", \"1450\", \"1500\", \"1550\", \"1600\", \"1650\", \"1700\", \"1750\", \"1800\", \"1850\", \"1900\", \"1950\", \"2000\", \"2050\", \"2100\", \"2150\", \"2200\"";


////////
	$conditions_listes_exploitation_animal_texts  = "";
	$conditions_listes_exploitation_animal_values  = "";

	$sqlquery= "SELECT id, animal FROM exploitations_animaux ORDER BY animal";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_exploitation_animal_texts .= ", ";
				$conditions_listes_exploitation_animal_values .= ", ";
			}

			$conditions_listes_exploitation_animal_texts .= "\"" . mysql_result($resultat_sql,$i,"animal") . "\"";
			$conditions_listes_exploitation_animal_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_exploitations_id_commune_texts  = "";
	$conditions_listes_exploitations_id_commune_values  = "";

	$sqlquery= "SELECT id, commune FROM exploitations_communes";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_exploitations_id_commune_texts .= ", ";
				$conditions_listes_exploitations_id_commune_values .= ", ";
			}

			$conditions_listes_exploitations_id_commune_texts .= "\"" . mysql_result($resultat_sql,$i,"commune") . "\"";
			$conditions_listes_exploitations_id_commune_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}

////////
	$conditions_listes_exploitations_handicapes_values  = "\"oui\", \"non\"";


////////
	$conditions_listes_activite_type_texts  = "";
	$conditions_listes_activite_type_values  = "";

	$sqlquery= "SELECT id, type FROM activites_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_activite_type_texts .= ", ";
				$conditions_listes_activite_type_values .= ", ";
			}

			$conditions_listes_activite_type_texts .= "\"" . mysql_result($resultat_sql,$i,"type") . "\"";
			$conditions_listes_activite_type_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}


////////
	$conditions_listes_activite_vente_producteur_pepa_texts  = "";
	$conditions_listes_activite_vente_producteur_pepavalues  = "";

	$sqlquery= "SELECT id, nom FROM producteurs WHERE pepa = \"oui\" ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			if ($i != 0) {
				$conditions_listes_activite_vente_producteur_pepa_texts .= ", ";
				$conditions_listes_activite_vente_producteur_pepa_values .= ", ";
			}

			$conditions_listes_activite_vente_producteur_pepa_texts .= "\"" . mysql_result($resultat_sql,$i,"nom") . "\"";
			$conditions_listes_activite_vente_producteur_pepa_values .= "\"" . mysql_result($resultat_sql,$i,"id") . "\"";
		}

////////
	$conditions_listes_activites_vente_producteur_non_pepa_values  = "\"oui\", \"non\"";




	mysql_close($connexion);
?>

<html>
<head>
<script language="JavaScript">

	var arr_tables_texts = new Array(<?php echo $arr_tables_texts; ?>);
	var arr_tables_values = new Array(<?php echo $arr_tables_values; ?>);
	
	var arr_producteurs_texts = new Array(<?php echo $arr_producteurs_texts; ?>);
	var arr_producteurs_values = new Array(<?php echo $arr_producteurs_values; ?>);

	var arr_produits_texts = new Array(<?php echo $arr_produits_texts; ?>);
	var arr_produits_values = new Array(<?php echo $arr_produits_values; ?>);
	
	var arr_activites_texts = new Array(<?php echo $arr_activites_texts; ?>);
	var arr_activites_values = new Array(<?php echo $arr_activites_values; ?>);

	var arr_exploitations_texts = new Array(<?php echo $arr_exploitations_texts; ?>);
	var arr_exploitations_values = new Array(<?php echo $arr_exploitations_values; ?>);

	var arr_champs_conditions = new Array(<?php echo $arr_champs_conditions; ?>);

	var arr_conditions_operateurs_texts = new Array();
	var arr_conditions_operateurs_values = new Array();

		arr_conditions_operateurs_texts['producteurs.nom'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['producteurs.nom'] = new Array("=","<>");

		arr_conditions_operateurs_texts['producteurs.pepa'] = new Array("est �gal �");
		arr_conditions_operateurs_values['producteurs.pepa'] = new Array("=");

		arr_conditions_operateurs_texts['producteurs.fonction'] = new Array("est �gal �");
		arr_conditions_operateurs_values['producteurs.fonction'] = new Array("=");

		arr_conditions_operateurs_texts['producteurs.ambassadeur'] = new Array("est �gal �");
		arr_conditions_operateurs_values['producteurs.ambassadeur'] = new Array("=");

		arr_conditions_operateurs_texts['producteur_langue'] = new Array("est �gal �");
		arr_conditions_operateurs_values['producteur_langue'] = new Array("=");

		arr_conditions_operateurs_texts['producteurs.ville'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['producteurs.ville'] = new Array("=","<>");

		arr_conditions_operateurs_texts['producteurs.vente_correspondance'] = new Array("est �gal �");
		arr_conditions_operateurs_values['producteurs.vente_correspondance'] = new Array("=");

		arr_conditions_operateurs_texts['produit_label'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produit_label'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_type'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_type'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_lait_type'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_lait_type'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_lait_type_lait'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_lait_type_lait'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_lait_traitement_lait'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_lait_traitement_lait'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_viande_type'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_viande_type'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_viande_conditionnement'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['produits.id_viande_conditionnement'] = new Array("=","<>");

		arr_conditions_operateurs_texts['produits.id_marche'] = new Array("est inf�rieur ou �gal �","est �gal �","est supp�rieur ou �gal �");
		arr_conditions_operateurs_values['produits.id_marche'] = new Array("<=","=",">=");

		arr_conditions_operateurs_texts['produits.produit_phare'] = new Array("est �gal �");
		arr_conditions_operateurs_values['produits.produit_phare'] = new Array("=");

		arr_conditions_operateurs_texts['exploitations.alpage'] = new Array("est �gal �");
		arr_conditions_operateurs_values['exploitations.alpage'] = new Array("=");

		arr_conditions_operateurs_texts['exploitations.etivaz'] = new Array("est �gal �");
		arr_conditions_operateurs_values['exploitations.etivaz'] = new Array("=");

		arr_conditions_operateurs_texts['exploitations.altitude'] = new Array("est inf�rieur ou �gal �","est �gal �","est supp�rieur ou �gal �");
		arr_conditions_operateurs_values['exploitations.altitude'] = new Array("<=","=",">=");

		arr_conditions_operateurs_texts['exploitation_animal'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['exploitation_animal'] = new Array("=","<>");

		arr_conditions_operateurs_texts['exploitations.id_commune'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['exploitations.id_commune'] = new Array("=","<>");

		arr_conditions_operateurs_texts['exploitations.handicapes'] = new Array("est �gal �");
		arr_conditions_operateurs_values['exploitations.handicapes'] = new Array("=");

		arr_conditions_operateurs_texts['activite_type'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['activite_type'] = new Array("=","<>");

		arr_conditions_operateurs_texts['activite_vente_producteur_pepa'] = new Array("est �gal �","n'est pas �gal �");
		arr_conditions_operateurs_values['activite_vente_producteur_pepa'] = new Array("=","<>");

		arr_conditions_operateurs_texts['activites.vente_producteur_non_pepa'] = new Array("est �gal �");
		arr_conditions_operateurs_values['activites.vente_producteur_non_pepa'] = new Array("=");

	var arr_conditions_listes_texts = new Array();
	var arr_conditions_listes_values = new Array();

		arr_conditions_listes_texts['producteurs.nom'] = new Array(<?php echo $conditions_listes_producteurs_nom_values; ?>);
		arr_conditions_listes_values['producteurs.nom'] = new Array(<?php echo $conditions_listes_producteurs_nom_values; ?>);

		arr_conditions_listes_texts['producteurs.pepa'] = new Array(<?php echo $conditions_listes_producteurs_pepa_values; ?>);
		arr_conditions_listes_values['producteurs.pepa'] = new Array(<?php echo $conditions_listes_producteurs_pepa_values; ?>);

		arr_conditions_listes_texts['producteurs.fonction'] = new Array(<?php echo $conditions_listes_producteurs_fonction_values; ?>);
		arr_conditions_listes_values['producteurs.fonction'] = new Array(<?php echo $conditions_listes_producteurs_fonction_values; ?>);

		arr_conditions_listes_texts['producteurs.ambassadeur'] = new Array(<?php echo $conditions_listes_producteurs_ambassadeur_values; ?>);
		arr_conditions_listes_values['producteurs.ambassadeur'] = new Array(<?php echo $conditions_listes_producteurs_ambassadeur_values; ?>);

		arr_conditions_listes_texts['producteur_langue'] = new Array(<?php echo $conditions_listes_producteur_langue_texts; ?>);
		arr_conditions_listes_values['producteur_langue'] = new Array(<?php echo $conditions_listes_producteur_langue_values; ?>);

		arr_conditions_listes_texts['producteurs.ville'] = new Array(<?php echo $conditions_listes_producteurs_ville_values; ?>);
		arr_conditions_listes_values['producteurs.ville'] = new Array(<?php echo $conditions_listes_producteurs_ville_values; ?>);

		arr_conditions_listes_texts['producteurs.vente_correspondance'] = new Array(<?php echo $conditions_listes_producteurs_vente_correspondance_values; ?>);
		arr_conditions_listes_values['producteurs.vente_correspondance'] = new Array(<?php echo $conditions_listes_producteurs_vente_correspondance_values; ?>);

		arr_conditions_listes_texts['produit_label'] = new Array(<?php echo $conditions_listes_produit_label_texts; ?>);
		arr_conditions_listes_values['produit_label'] = new Array(<?php echo $conditions_listes_produit_label_values; ?>);

		arr_conditions_listes_texts['produits.id_type'] = new Array(<?php echo $conditions_listes_produits_id_type_texts; ?>);
		arr_conditions_listes_values['produits.id_type'] = new Array(<?php echo $conditions_listes_produits_id_type_values; ?>);

		arr_conditions_listes_texts['produits.id_lait_type'] = new Array(<?php echo $conditions_listes_produits_id_lait_type_texts; ?>);
		arr_conditions_listes_values['produits.id_lait_type'] = new Array(<?php echo $conditions_listes_produits_id_lait_type_values; ?>);

		arr_conditions_listes_texts['produits.id_lait_type_lait'] = new Array(<?php echo $conditions_listes_produits_id_lait_type_lait_texts; ?>);
		arr_conditions_listes_values['produits.id_lait_type_lait'] = new Array(<?php echo $conditions_listes_produits_id_lait_type_lait_values; ?>);

		arr_conditions_listes_texts['produits.id_lait_traitement_lait'] = new Array(<?php echo $conditions_listes_produits_id_lait_traitement_lait_texts; ?>);
		arr_conditions_listes_values['produits.id_lait_traitement_lait'] = new Array(<?php echo $conditions_listes_produits_id_lait_traitement_lait_values; ?>);

		arr_conditions_listes_texts['produits.id_viande_type'] = new Array(<?php echo $conditions_listes_produits_id_viande_type_texts; ?>);
		arr_conditions_listes_values['produits.id_viande_type'] = new Array(<?php echo $conditions_listes_produits_id_viande_type_values; ?>);

		arr_conditions_listes_texts['produits.id_viande_conditionnement'] = new Array(<?php echo $conditions_listes_produits_id_viande_conditionnement_texts; ?>);
		arr_conditions_listes_values['produits.id_viande_conditionnement'] = new Array(<?php echo $conditions_listes_produits_id_viande_conditionnement_values; ?>);

		arr_conditions_listes_texts['produits.id_marche'] = new Array(<?php echo $conditions_listes_produits_id_marche_texts; ?>);
		arr_conditions_listes_values['produits.id_marche'] = new Array(<?php echo $conditions_listes_produits_id_marche_values; ?>);

		arr_conditions_listes_texts['produits.produit_phare'] = new Array(<?php echo $conditions_listes_produits_produit_phare_values; ?>);
		arr_conditions_listes_values['produits.produit_phare'] = new Array(<?php echo $conditions_listes_produits_produit_phare_values; ?>);

		arr_conditions_listes_texts['exploitations.alpage'] = new Array(<?php echo $conditions_listes_exploitations_alpage_values; ?>);
		arr_conditions_listes_values['exploitations.alpage'] = new Array(<?php echo $conditions_listes_exploitations_alpage_values; ?>);

		arr_conditions_listes_texts['exploitations.etivaz'] = new Array(<?php echo $conditions_listes_exploitations_etivaz_values; ?>);
		arr_conditions_listes_values['exploitations.etivaz'] = new Array(<?php echo $conditions_listes_exploitations_etivaz_values; ?>);

		arr_conditions_listes_texts['exploitations.altitude'] = new Array(<?php echo $conditions_listes_exploitations_altitude_values; ?>);
		arr_conditions_listes_values['exploitations.altitude'] = new Array(<?php echo $conditions_listes_exploitations_altitude_values; ?>);

		arr_conditions_listes_texts['exploitation_animal'] = new Array(<?php echo $conditions_listes_exploitation_animal_texts; ?>);
		arr_conditions_listes_values['exploitation_animal'] = new Array(<?php echo $conditions_listes_exploitation_animal_values; ?>);

		arr_conditions_listes_texts['exploitations.id_commune'] = new Array(<?php echo $conditions_listes_exploitations_id_commune_texts; ?>);
		arr_conditions_listes_values['exploitations.id_commune'] = new Array(<?php echo $conditions_listes_exploitations_id_commune_values; ?>);

		arr_conditions_listes_texts['exploitations.handicapes'] = new Array(<?php echo $conditions_listes_exploitations_handicapes_values; ?>);
		arr_conditions_listes_values['exploitations.handicapes'] = new Array(<?php echo $conditions_listes_exploitations_handicapes_values; ?>);

		arr_conditions_listes_texts['activite_type'] = new Array(<?php echo $conditions_listes_activite_type_texts; ?>);
		arr_conditions_listes_values['activite_type'] = new Array(<?php echo $conditions_listes_activite_type_values; ?>);

		arr_conditions_listes_texts['activite_vente_producteur_pepa'] = new Array(<?php echo $conditions_listes_activite_vente_producteur_pepa_texts; ?>);
		arr_conditions_listes_values['activite_vente_producteur_pepa'] = new Array(<?php echo $conditions_listes_activite_vente_producteur_pepa_values; ?>);

		arr_conditions_listes_texts['activites.vente_producteur_non_pepa'] = new Array(<?php echo $conditions_listes_activites_vente_producteur_non_pepa_values; ?>);
		arr_conditions_listes_values['activites.vente_producteur_non_pepa'] = new Array(<?php echo $conditions_listes_activites_vente_producteur_non_pepa_values; ?>);

// cr�ation d'une propri�t� "chaine" de l'objet Array, qui contiendra le tableau en forme de cha�ne, chaque �l�ment s�par� par un "|"
	Array.prototype.chaine = "";

// m�thode "contient" qui fait la recherche
	Array.prototype.contient = function(valeur) {
		this.chaine = "|" + this.join("|") + "|";
		if (this.chaine.indexOf("|" + valeur + "|") == -1) return false;
		else return true;
	}

	function SetDiv(ID,Content) {

		var ns4 = (document.layers)? true:false;		//NS 4
		var ie4 = (document.all)? true:false;			//IE 4
		var dom = (document.getElementById)? true:false;	//NS 6 ou IE 5

		if (dom) {
			document.getElementById(ID).innerHTML = Content;
			return;
		}
		if (ie4) {
			document.all[ID].innerHTML = Content;
			return;
		}
		if (ns4) {
			with (eval('document.'+ID+'.document')) {
				open();
				write(Content);
				close();
			}
			return;
		}
	}

	var nbr_conditions = 0;
	
	function condition(action,id) {

		if (action == 'ajouter') {
			chaine = document.all['conditions'].innerHTML;
			chaine += "n�"+(nbr_conditions+1)+": <select id=\"condition_champ_"+nbr_conditions+"\" name=\"condition_champ["+nbr_conditions+"]\" size=\"1\" onchange=\"maj_condition("+nbr_conditions+")\" style=\"width:250px\">";

			chaine += "<option value=\"0\">(choisir dans la liste)</option>\n";

			var tables_ok = Array();
			var tables_ok_text = Array();

			if (document.formulaire.tables.selectedIndex == 1) {

				tables_ok[tables_ok.length] = arr_tables_values[0];

				tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
			}
			else if (document.formulaire.tables.selectedIndex == 2) {

				tables_ok[tables_ok.length] = arr_tables_values[1];
				tables_ok[tables_ok.length] = arr_tables_values[0];

				tables_ok_text[tables_ok_text.length] = arr_tables_texts[1];
				tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
			}
			else if (document.formulaire.tables.selectedIndex == 3) {

				tables_ok[tables_ok.length] = arr_tables_values[2];
				tables_ok[tables_ok.length] = arr_tables_values[0];

				tables_ok_text[tables_ok_text.length] = arr_tables_texts[2];
				tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
			}
			else if (document.formulaire.tables.selectedIndex == 4) {

				tables_ok[tables_ok.length] = arr_tables_values[3];
				tables_ok[tables_ok.length] = arr_tables_values[0];

				tables_ok_text[tables_ok_text.length] = arr_tables_texts[3];
				tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
			}

			var j

			for (j=0; j<tables_ok.length; j++) {

				var arrtxt = eval("arr_"+tables_ok[j]+"_texts");
				var arrval = eval("arr_"+tables_ok[j]+"_values");

				var i

				for (i=0; i<arrval.length; i++) {

					if (arrval[i] == "produits.id_lait_type_lait" || arrval[i] == "produits.id_lait_traitement_lait" || arrval[i] == "produits.produit_phare" || arrval[i] == "exploitations.etivaz" || arrval[i] == "exploitations.altitude" || arrval[i] == "activite_vente_producteur_pepa" || arrval[i] == "activites.vente_producteur_non_pepa") {
						var texte = "* ";
					}
					else {
						var texte = "";
					}

					if (document.formulaire.tables.selectedIndex == 1) {
						var arrtxt_i = texte+arrtxt[i];
						var arrval_i = arrval[i];
					}
					else {
						var arrtxt_i = texte+tables_ok_text[j]+" - "+arrtxt[i];
						var arrval_i = arrval[i];
					}
				
					if (arr_champs_conditions.contient(arrval_i)) {
						chaine += "<option value=\""+arrval_i+"\">"+arrtxt_i+"</option>\n";
					}
				}
			}

			chaine += "</select> <select id=\"condition_operateur_"+nbr_conditions+"\" name=\"condition_operateur["+nbr_conditions+"]\" size=\"1\" style=\"width:150px\"></select> <select id=\"condition_liste_"+nbr_conditions+"\" name=\"condition_liste["+nbr_conditions+"]\" size=\"1\" style=\"width:200px\"></select> <br>\n";

			nbr_conditions++;
		}
		else if (action == 'effacer_tout') {
			nbr_conditions = 0;
			chaine = "";
		}

		SetDiv('conditions',chaine);
	}

	function ini() {
		var liste1 = document.formulaire.tables;
		var arrtxt = arr_tables_texts;
		var arrval = arr_tables_values;
		
		liste1.options.length=0;

		var o=new Option("(choisir dans la liste)","");
		liste1.options[liste1.options.length]=o;

		var i

		for (i=0; i<arrval.length; i++) {

			var o=new Option(arrtxt[i],arrval[i]);
			liste1.options[liste1.options.length]=o;
		}
	}


	function push(liste1,liste2){

		for(yo=0;yo<liste1.length;yo++){

			if(liste1.options[yo].selected == true){

				var p= new Option(liste1.options[yo].text,liste1.options[yo].value);
				liste2.options[liste2.options.length]=p;
				liste1.options[yo] = null;

				yo=yo-1;
			}
		}
	}

	function maj_champs() {

		var tables_ok = Array();
		var tables_ok_text = Array();

		if (document.formulaire.tables.selectedIndex == 1) {

			tables_ok[tables_ok.length] = arr_tables_values[0];

			tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
		}
		else if (document.formulaire.tables.selectedIndex == 2) {

			tables_ok[tables_ok.length] = arr_tables_values[1];
			tables_ok[tables_ok.length] = arr_tables_values[0];

			tables_ok_text[tables_ok_text.length] = arr_tables_texts[1];
			tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
		}
		else if (document.formulaire.tables.selectedIndex == 3) {

			tables_ok[tables_ok.length] = arr_tables_values[2];
			tables_ok[tables_ok.length] = arr_tables_values[0];

			tables_ok_text[tables_ok_text.length] = arr_tables_texts[2];
			tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
		}
		else if (document.formulaire.tables.selectedIndex == 4) {

			tables_ok[tables_ok.length] = arr_tables_values[3];
			tables_ok[tables_ok.length] = arr_tables_values[0];

			tables_ok_text[tables_ok_text.length] = arr_tables_texts[3];
			tables_ok_text[tables_ok_text.length] = arr_tables_texts[0];
		}


		document.formulaire.champs.length = 0;
		document.formulaire.champs_ok.length = 0;
		document.formulaire.tris.length = 0;
		document.formulaire.tris_ok.length = 0;

		var j

		for (j=0; j<tables_ok.length; j++) {

			var arrtxt = eval("arr_"+tables_ok[j]+"_texts");
			var arrval = eval("arr_"+tables_ok[j]+"_values");

			var i

			for (i=0; i<arrval.length; i++) {

				if (document.formulaire.tables.selectedIndex == 1) {
					var o=new Option(arrtxt[i],arrval[i]);
					var oo=new Option(arrtxt[i],arrval[i]);
				}
				else {
					var o=new Option(tables_ok_text[j]+" - "+arrtxt[i],arrval[i]);
					var oo=new Option(tables_ok_text[j]+" - "+arrtxt[i],arrval[i]);
				}
				
				document.formulaire.champs.options[document.formulaire.champs.options.length]=o;
				
				if ((arrval[i].substring(0,arrval[i].indexOf("_",0)) != "producteur")  && (arrval[i].substring(0,arrval[i].indexOf("_",0)) != "produit")  && (arrval[i].substring(0,arrval[i].indexOf("_",0)) != "exploitation")  && (arrval[i].substring(0,arrval[i].indexOf("_",0)) != "activite")) {
					document.formulaire.tris.options[document.formulaire.tris.options.length]=oo;
				}
			}
		}
		condition('effacer_tout');
	}

	function maj_condition(id) {

		var champ = eval("document.formulaire.condition_champ_"+id);
		var operateur = eval("document.formulaire.condition_operateur_"+id);
		var liste = eval("document.formulaire.condition_liste_"+id);

		if (champ.value != 0) {

			var arrtxt = arr_conditions_operateurs_texts[champ.value];
			var arrval = arr_conditions_operateurs_values[champ.value];

			var i
			operateur.options.length = 0;

			for (i=0; i<arrval.length; i++) {

				var o=new Option(arrtxt[i],arrval[i]);
				
				operateur.options[operateur.options.length]=o;
			}

			var arrtxt = arr_conditions_listes_texts[champ.value];
			var arrval = arr_conditions_listes_values[champ.value];

			liste.options.length = 0;

			for (i=0; i<arrval.length; i++) {

				var o=new Option(arrtxt[i],arrval[i]);
				
				liste.options[liste.options.length]=o;
			}
		}
		else {
			operateur.options.length = 0;
			liste.options.length = 0;
		}
	}

	function check_submit() {
		for (i=0; i<document.formulaire.champs_ok.length; i++) {
			document.formulaire.champs_ok.options[i].selected=true;
		}
		for (i=0; i<document.formulaire.tris_ok.length; i++) {
			document.formulaire.tris_ok.options[i].selected=true;
		}
		
		document.formulaire.action = document.formulaire.output.value; 
	}

</script>
<style type="text/css">
<!-- 
p {font-family:Arial Narrow, font-size:2; color:black;}
a {font-family:Arial Narrow, font-size:2; color:black; text-decoration:underline; }
td {font-family:Arial Narrow, font-size:2; color:black;}
 -->
</style>
</head>

<body onload="ini();maj_champs();">

<form name="formulaire" method="post" action="" onsubmit="check_submit();" TARGET="_blank">
<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; border: 1px dotted #0000FF" bordercolor="#111111" width="957">
  <tr>
    <td width="250" style="border-collapse: collapse; border: 1px dotted #0000FF" align="left" valign="top"><?php

include "menu.php";

?></td>
    <td width="700" style="border-collapse: collapse; border: 1px dotted #0000FF" valign="top"><b><font face="Arial Narrow">

       Choix de la table principale :
       <br>

       <select name="tables" size="1" onChange="maj_champs();">
       </select>


       <BR><br>Choix des champs (ils apparaiteront dans l'ordre propos�) : 
       <br>

       <select name="champs" align="top" size="15" multiple="multiple" ondblclick="push(document.formulaire.champs,document.formulaire.champs_ok);" style="width:300px">
       </select>

       <input type="button" name="boutvG" value="<" onclick="push(document.formulaire.champs_ok,document.formulaire.champs);">
       <input type="button" name="boutvD" value=">" onclick="push(document.formulaire.champs,document.formulaire.champs_ok);">


       <select id="champs_ok" name="fields[]" align="top" size="15" multiple="multiple" ondblclick="push(document.formulaire.champs_ok,document.formulaire.champs);" style="width:300px">
       </select>
       
       <BR><br>Ordre de tri : 
       <br>

       <select name="tris" align="top" size="7" multiple="multiple" ondblclick="push(document.formulaire.tris,document.formulaire.tris_ok);" style="width:300px">
       </select>

       <input type="button" name="boutvG" value="<" onclick="push(document.formulaire.tris_ok,document.formulaire.tris);">
       <input type="button" name="boutvD" value=">" onclick="push(document.formulaire.tris,document.formulaire.tris_ok);">


       <select id="tris_ok" name="orders[]" align="top" size="7" multiple="multiple" ondblclick="push(document.formulaire.tris_ok,document.formulaire.tris);" style="width:300px">
       </select>

       <BR><br>Conditions : <input type="button" value="Ajouter une condition" onclick="condition('ajouter');"> <input type="button" value="Effacer les conditions" onclick="condition('effacer_tout');"><br>
       <div id="conditions">
       </div>

	<br><font size="2"></b>* Ces champs sont d�pendants d'autres champs (de PEPA pour les produits, d'ALPAGE pour les exploitations et de VENTE DIRECTE pour les activit�s). Lors de l'utilisation des conditions, on indique implicitement les deux conditions. Ainsi, en choisissant l'altitude, on part de l'id�e que c'est un alpage ! Ceci est aussi vrai pour le traitement du lait qui ne s'aplique qu'aux produits laitiers, mais c'est plus intuitif.</font><br>

       <BR>Option de sortie :<br>
       <select name="output" size="1">
        <option value ="documents_html.php">tableau HTML</option>
        <option value ="documents_xls.php">tableau Excel</option>
        <option value ="documents_html_fiche.php">fiche HTML</option>
        <option value ="documents_doc.php">fiche Word (RTF)</option>
        <option value ="documents_pdf.php">fiche PDF</option>
       </select>

       <BR><br>
       <input type="submit" name="name" value="Envoyer">

    </td>
  </tr>
</table>
</form>

</body>

</html>

