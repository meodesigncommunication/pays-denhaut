<?php
	require "config.php";

	include 'champs_attributs.php';

	$arr_tables_values = "";
	$arr_tables_texts = "";
	$i = 0;

	foreach (array_keys($titres_tables) as $var) {
		if ($i == 0) {
			$arr_tables_texts .= "\"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= "\"" . $var . "\"";
		}
		else {
			$arr_tables_texts .= ", \"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_producteurs_values = "";
	$arr_producteurs_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_producteurs) as $var) {
		if ($i == 0) {
			$arr_producteurs_texts .= "\"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= "\"" . $var . "\"";
		}
		else {
			$arr_producteurs_texts .= ", \"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_produits_values = "";
	$arr_produits_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_produits) as $var) {
		if ($i == 0) {
			$arr_produits_texts .= "\"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= "\"" . $var . "\"";
		}
		else {
			$arr_produits_texts .= ", \"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_exploitations_values = "";
	$arr_exploitations_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_exploitations) as $var) {
		if ($i == 0) {
			$arr_exploitations_texts .= "\"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= "\"" . $var . "\"";
		}
		else {
			$arr_exploitations_texts .= ", \"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_activites_values = "";
	$arr_activites_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_activites) as $var) {
		if ($i == 0) {
			$arr_activites_texts .= "\"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= "\"" . $var . "\"";
		}
		else {
			$arr_activites_texts .= ", \"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	include 'requete.php';

	$sqlquery= $requete;
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbre_lignes = mysql_num_rows($resultat_sql);

		while($row = mysql_fetch_assoc($resultat_sql)){ 
			$array[] = $row;
		}

		for ($i=0;$i<$nbre_lignes;$i++) {
			foreach (array_keys($array[0]) as $heading) {

				if ($heading == "exploitation_activite") {
					$requete = "SELECT activites.nom FROM activites LEFT OUTER JOIN activite_exploitation ON activites.id = activite_exploitation.id_activite WHERE activite_exploitation.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"activites.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_exploitation") {
					$requete = "SELECT exploitations.nom FROM exploitations LEFT OUTER JOIN activite_exploitation ON exploitations.id = activite_exploitation.id_exploitation WHERE activite_exploitation.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"exploitations.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_type") {
					$requete = "SELECT type FROM activites_types LEFT OUTER JOIN activite_type ON activites_types.id = activite_type.id_type WHERE activite_type.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"type")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_producteur_pepa") {
					$requete = "SELECT producteurs.nom FROM producteurs LEFT OUTER JOIN activite_vente_producteur_pepa ON producteurs.id = activite_vente_producteur_pepa.id_producteur_pepa WHERE activite_vente_producteur_pepa.id_activite = " . $array[$i][$heading]. " ORDER BY producteurs.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_produit") {
					$requete = "SELECT produits.nom FROM produits LEFT OUTER JOIN activite_vente_produit ON activite_vente_produit.id_produit = produits.id WHERE activite_vente_produit.id_activite = " . $array[$i][$heading]. " ORDER BY produits.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"produits.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "exploitation_animal") {
					$requete = "SELECT animal FROM exploitations_animaux LEFT OUTER JOIN exploitation_animal ON exploitations_animaux.id = exploitation_animal.id_animal WHERE exploitation_animal.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"animal")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteur_langue") {
					$requete = "SELECT langue FROM producteurs_langues LEFT OUTER JOIN producteur_langue ON producteurs_langues.id = producteur_langue.id_langue WHERE producteur_langue.id_producteur = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"langue")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "produit_label") {
					$requete = "SELECT label FROM produits_labels LEFT OUTER JOIN produit_label ON produits_labels.id = produit_label.id_label WHERE produit_label.id_produit = " . $array[$i][$heading] . " ORDER BY produit_label.id_label";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"label")."\n";
						}

					$tableau[$i][$heading] = $tmp.$array[$i]['produits.autre_label'];
				}
				elseif ($heading == "produits.autre_label") {
				}
				elseif ($heading == "producteurs.site_web") {
					$tableau[$i][$heading] = " ".$array[$i][$heading];
				}
				else {
					$tableau[$i][$heading] = $array[$i][$heading];
				}
			}
		}

	mysql_close($connexion);


function array2fiche($array, $array_titres, $array_tailles, $titre) {

	// compte les lignes et colonnes :
	$nbr_lignes = count ($array);
	$nbr_colonnes = 0;
	$titres_colonne = Array();
	$headings = Array();

	foreach (array_keys($array[0]) as $heading) {
		$headings[$nbr_colonnes] = $heading;
		$titres_colonne[$nbr_colonnes] = $array_titres[$heading];
		$taille_colonne[$nbr_colonnes] = $array_tailles[$heading]*3;
		$nbr_colonnes++;
	}

	$k = 0;

	echo "<html>\n  <body style=\"font-family: Arial Narrow; font-size: 10 pt\">\n";

	foreach ($array as $row) {

		if ($k == 0) {
			echo "    <div style=\"page-break-before: auto;\">\n";
		}
		else {
			echo "    <div style=\"page-break-before: always;\">\n";
		}
		echo "    <font size=\"3\"><b>Fiche ".$titre."<b></font>\n<br>\n\n";

		echo "    <table border=\"1\" width=\"630\" cellspacing=\"0\" cellpadding=\"3\" style=\"border-collapse: collapse\" bordercolor=\"#111111\">\n";

		for($i=0;$i<$nbr_colonnes;$i++) {

			$row[$headings[$i]] = str_replace("\n", "<br>", $row[$headings[$i]]);
			if ($row[$headings[$i]] == "" or $row[$headings[$i]] == " ") {
				$row[$headings[$i]] = "&nbsp;";
			}
			echo "      <tr>\n        <td width=\"100\">".$titres_colonne[$i]."</td>\n        <td width=\"530\">".$row[$headings[$i]]."</td>\n      </tr>\n";

		}

		echo "    </table><br>\n";
		echo "    </div>\n\n";

		$k++;
	}

	echo "  </body>\n</html>\n";
}


array2fiche($tableau,$titres_champs,$width_champs,$titres_tables[$HTTP_POST_VARS['tables']]);


?>
