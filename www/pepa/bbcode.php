<?php

// remplace les balises BBCode par des balises HTML

	function bbCode($t) {
		$t = htmlspecialchars($t);
		
		// retour de chariot
		$t=str_replace("\r\n", "<br>\n", $t);
		
		// barre horizontale
		$t=str_replace("[/]", "<hr width=\"100%\" size=\"1\" />", $t);
		$t=str_replace("[hr]", "<hr width=\"100%\" size=\"1\" />", $t);
		$t=str_replace("[ligne]", "<hr width=\"100%\" size=\"1\" />", $t);
		$t=str_replace("[HR]", "<hr width=\"100%\" size=\"1\" />", $t);
		$t=str_replace("[LIGNE]", "<hr width=\"100%\" size=\"1\" />", $t);
		
		// gras
		$t=str_replace("[b]", "<b>", $t);
		$t=str_replace("[/b]", "</b>", $t);
		$t=str_replace("[g]", "<b>", $t);
		$t=str_replace("[/g]", "</b>", $t);
		$t=str_replace("[B]", "<b>", $t);
		$t=str_replace("[/B]", "</b>", $t);
		$t=str_replace("[G]", "<b>", $t);
		$t=str_replace("[/G]", "</b>", $t);
		
		// italique
		$t=str_replace("[i]", "<i>", $t);
		$t=str_replace("[/i]", "</i>", $t);
		$t=str_replace("[I]", "<i>", $t);
		$t=str_replace("[/I]", "</i>", $t);
		
		// soulignement
		$t=str_replace("[u]", "<u>", $t);
		$t=str_replace("[/u]", "</u>", $t);
		$t=str_replace("[s]", "<u>", $t);
		$t=str_replace("[/s]", "</u>", $t);
		$t=str_replace("[U]", "<u>", $t);
		$t=str_replace("[/U]", "</u>", $t);
		$t=str_replace("[S]", "<u>", $t);
		$t=str_replace("[/S]", "</u>", $t);
		
		// alignement centr�
		$t=str_replace("[center]", "<div style=\"text-align: center\">", $t);
		$t=str_replace("[/center]", "</div>", $t);
		$t=str_replace("[centre]", "<div style=\"text-align: center\">", $t);
		$t=str_replace("[/centre]", "</div>", $t);
		$t=str_replace("[centr�]", "<div style=\"text-align: center\">", $t);
		$t=str_replace("[/centr�]", "</div>", $t);
		$t=str_replace("[CENTER]", "<div style=\"text-align: center\">", $t);
		$t=str_replace("[/CENTER]", "</div>", $t);
		$t=str_replace("[CENTRE]", "<div style=\"text-align: center\">", $t);
		$t=str_replace("[/CENTRE]", "</div>", $t);
		
		// alignement � droite
		$t=str_replace("[right]", "<div style=\"text-align: right\">", $t);
		$t=str_replace("[/right]", "</div>", $t);
		$t=str_replace("[droite]", "<div style=\"text-align: right\">", $t);
		$t=str_replace("[/droite]", "</div>", $t);
		$t=str_replace("[RIGHT]", "<div style=\"text-align: right\">", $t);
		$t=str_replace("[/RIGHT]", "</div>", $t);
		$t=str_replace("[DROITE]", "<div style=\"text-align: right\">", $t);
		$t=str_replace("[/DROITE]", "</div>", $t);
		
		// alignement � gauche
		$t=str_replace("[left]", "<div style=\"text-align: left\">", $t);
		$t=str_replace("[/left]", "</div>", $t);
		$t=str_replace("[gauche]", "<div style=\"text-align: left\">", $t);
		$t=str_replace("[/gauche]", "</div>", $t);
		$t=str_replace("[LEFT]", "<div style=\"text-align: left\">", $t);
		$t=str_replace("[/LEFT]", "</div>", $t);
		$t=str_replace("[GAUCHE]", "<div style=\"text-align: left\">", $t);
		$t=str_replace("[/GAUCHE]", "</div>", $t);
		
		// alignement justifi�
		$t=str_replace("[justify]", "<div style=\"text-align: justify\">", $t);
		$t=str_replace("[/justify]", "</div>", $t);
		$t=str_replace("[justifie]", "<div style=\"text-align: justify\">", $t);
		$t=str_replace("[/justifie]", "</div>", $t);
		$t=str_replace("[justifi�]", "<div style=\"text-align: justify\">", $t);
		$t=str_replace("[/justifi�]", "</div>", $t);
		$t=str_replace("[JUSTIFY]", "<div style=\"text-align: justify\">", $t);
		$t=str_replace("[/JUSTIFY]", "</div>", $t);
		$t=str_replace("[JUSTIFIE]", "<div style=\"text-align: justify\">", $t);
		$t=str_replace("[/JUSTIFIE]", "</div>", $t);
		
		// couleur
		$t=str_replace("[/color]", "</font>", $t);
		$regCouleur="\[color= ?(([[:alpha:]]+)|(#[[:digit:][:alpha:]]{6})) ?\]";
		$t=ereg_replace($regCouleur, "<font color=\"\\1\">", $t);
		$t=str_replace("[/couleur]", "</font>", $t);
		$regCouleur="\[couleur= ?(([[:alpha:]]+)|(#[[:digit:][:alpha:]]{6})) ?\]";
		$t=ereg_replace($regCouleur, "<font color=\"\\1\">", $t);
		$t=str_replace("[/COLOR]", "</font>", $t);
		$regCouleur="\[COLOR= ?(([[:alpha:]]+)|(#[[:digit:][:alpha:]]{6})) ?\]";
		$t=ereg_replace($regCouleur, "<font color=\"\\1\">", $t);
		$t=str_replace("[/COULEUR]", "</font>", $t);
		$regCouleur="\[COULEUR= ?(([[:alpha:]]+)|(#[[:digit:][:alpha:]]{6})) ?\]";
		$t=ereg_replace($regCouleur, "<font color=\"\\1\">", $t);
		
		// taille des caract�res
		$t=str_replace("[/size]", "</font>", $t);
		$regCouleur="\[size= ?([[:digit:]]+) ?\]";
		$t=ereg_replace($regCouleur, "<font size=\"\\1\">", $t);
		$t=str_replace("[/taille]", "</font>", $t);
		$regCouleur="\[taille= ?([[:digit:]]+) ?\]";
		$t=ereg_replace($regCouleur, "<font size=\"\\1\">", $t);
		$t=str_replace("[/SIZE]", "</font>", $t);
		$regCouleur="\[SIZE= ?([[:digit:]]+) ?\]";
		$t=ereg_replace($regCouleur, "<font size=\"\\1\">", $t);
		$t=str_replace("[/TAILLE]", "</font>", $t);
		$regCouleur="\[TAILLE= ?([[:digit:]]+) ?\]";
		$t=ereg_replace($regCouleur, "<font size=\"\\1\">", $t);
		
		// lien
		$t = preg_replace("#\[url\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/url\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[url=(.+?)\](.+?)\[\/url\]/", "<a href=$1 target=blank>$2</a>", $t); 
		$t = preg_replace("#\[link\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/link\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[link=(.+?)\](.+?)\[\/link\]/", "<a href=$1 target=blank>$2</a>", $t); 
		$t = preg_replace("#\[lien\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/lien\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[lien=(.+?)\](.+?)\[\/lien\]/", "<a href=$1 target=blank>$2</a>", $t); 
		$t = preg_replace("#\[URL\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/URL\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[URL=(.+?)\](.+?)\[\/URL\]/", "<a href=$1 target=blank>$2</a>", $t); 
		$t = preg_replace("#\[LINK\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/LINK\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[LINK=(.+?)\](.+?)\[\/LINK\]/", "<a href=$1 target=blank>$2</a>", $t); 
		$t = preg_replace("#\[LIEN\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/LIEN\]#sie", "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\" target=blank>\\1\\3</a>'", $t); 
		$t = preg_replace("/\[LIEN=(.+?)\](.+?)\[\/LIEN\]/", "<a href=$1 target=blank>$2</a>", $t); 
		
		// �v�nement
		$t = preg_replace("/\[evenement=(.+?)\](.+?)\[\/evenement\]/", "<a href=evenement.php?id=$1>$2</a>", $t); 
		$t = preg_replace("/\[�v�nement=(.+?)\](.+?)\[\/�v�nement\]/", "<a href=evenement.php?id=$1>$2</a>", $t); 
		$t = preg_replace("/\[EVENEMENT=(.+?)\](.+?)\[\/EVENEMENT\]/", "<a href=evenement.php?id=$1>$2</a>", $t); 
		
		// mail
		$regMailSimple="\[email\] ?([^\[]*) ?\[/email\]";
		$regMailEtendu="\[email ?=([^\[]*) ?] ?([^]]*) ?\[/email\]";
		if (ereg($regMailSimple, $t)) $t=ereg_replace($regMailSimple, "<a href=\"mailto:\\1\">\\1</a>", $t);
		else $t=ereg_replace($regMailEtendu, "<a href=\"mailto:\\1\">\\2</a>", $t);
		$regMailSimple="\[mail\] ?([^\[]*) ?\[/mail\]";
		$regMailEtendu="\[mail ?=([^\[]*) ?] ?([^]]*) ?\[/mail\]";
		if (ereg($regMailSimple, $t)) $t=ereg_replace($regMailSimple, "<a href=\"mailto:\\1\">\\1</a>", $t);
		else $t=ereg_replace($regMailEtendu, "<a href=\"mailto:\\1\">\\2</a>", $t);
		$regMailSimple="\[EMAIL\] ?([^\[]*) ?\[/EMAIL\]";
		$regMailEtendu="\[EMAIL ?=([^\[]*) ?] ?([^]]*) ?\[/EMAIL\]";
		if (ereg($regMailSimple, $t)) $t=ereg_replace($regMailSimple, "<a href=\"mailto:\\1\">\\1</a>", $t);
		else $t=ereg_replace($regMailEtendu, "<a href=\"mailto:\\1\">\\2</a>", $t);
		$regMailSimple="\[MAIL\] ?([^\[]*) ?\[/MAIL\]";
		$regMailEtendu="\[MAIL ?=([^\[]*) ?] ?([^]]*) ?\[/MAIL\]";
		if (ereg($regMailSimple, $t)) $t=ereg_replace($regMailSimple, "<a href=\"mailto:\\1\">\\1</a>", $t);
		else $t=ereg_replace($regMailEtendu, "<a href=\"mailto:\\1\">\\2</a>", $t);
		
		// image
		$regImage="\[img\] ?([^\[]*) ?\[/img\]";
		$regImageAlternatif="\[img ?= ?([^\[]*) ?\]";
		if (ereg($regImage, $t)) $t=ereg_replace($regImage, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		else $t=ereg_replace($regImageAlternatif, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		$regImage="\[image\] ?([^\[]*) ?\[/image\]";
		$regImageAlternatif="\[image ?= ?([^\[]*) ?\]";
		if (ereg($regImage, $t)) $t=ereg_replace($regImage, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		else $t=ereg_replace($regImageAlternatif, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		$regImage="\[IMG\] ?([^\[]*) ?\[/IMG\]";
		$regImageAlternatif="\[IMG ?= ?([^\[]*) ?\]";
		if (ereg($regImage, $t)) $t=ereg_replace($regImage, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		else $t=ereg_replace($regImageAlternatif, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		$regImage="\[IMAGE\] ?([^\[]*) ?\[/IMAGE\]";
		$regImageAlternatif="\[IMAGE ?= ?([^\[]*) ?\]";
		if (ereg($regImage, $t)) $t=ereg_replace($regImage, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		else $t=ereg_replace($regImageAlternatif, "<img src=\"\\1\" alt=\"\" border=\"0\" />", $t);
		
		return $t;
	}

?>