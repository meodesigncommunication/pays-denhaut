<ul>
<b>Producteurs</b>
  <li><a href="producteurs.php">ajouter un producteur</a></li>
  <li><a href="listes.php?liste=producteurs">liste des producteurs actuels</a></li>
</ul>

<ul>
<b>Produits</b>
  <li><a href="produits.php">ajouter un produit</a></li>
  <li><a href="listes.php?liste=produits">liste des produits actuels</a></li>
</ul>

<ul>
<b>Exploitations/lieux</b>
  <li><a href="exploitations.php">ajouter une exploitation</a></li>
  <li><a href="listes.php?liste=exploitations">liste des exploitations actuelles</a></li>
</ul>

<ul>
<b>Activit�s</b>
  <li><a href="activites.php">ajouter une activit�</a></li>
  <li><a href="listes.php?liste=activites">liste des activit�s actuelles</a></li>
</ul>

<ul>
<b>Photos</b>
  <li><a href="photos.php">ajouter une photo</a></li>
</ul>

<ul>
<b>Documents</b>
  <li><a href="documents.php">Document � la carte</a><BR><BR></li>
  <li><a href="documents_xls_autres.php?document=tout" target="_blank">toutes les donn�es (Excel)</a></li>
  <li><a href="documents_xls_autres.php?document=suivi" target="_blank">tableau de suivi (Excel)</a></li>
  <br><br><font color="red" size="1"><i>Attention, dans tous les fichiers Excel, le nombre de caract�res dans une cellule est limit� � 255. Les descriptions et autres longs champs seront donc coup�s !<br></i></font>
</ul>