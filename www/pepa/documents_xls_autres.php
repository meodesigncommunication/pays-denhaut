<?php
	require "config.php";

	require_once "class.writeexcel_workbook.inc.php";
	require_once "class.writeexcel_worksheet.inc.php";

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}


// Toutes les donn�es //

if ($_GET{'document'} == "tout") {

   function array2xls($arrays, $array_titres, $array_tailles, $titre) {


	// ouvre un fichier, cr�e une feuille et d�finit les formats
	$fname = tempnam("tmp", "file.xls");
	$workbook =& new writeexcel_workbook($fname);

	foreach (array_keys($arrays) as $heading_feuille) {
		$array = $arrays[$heading_feuille];

		// variables
		$ligne = 2;
		$colonne = 0;	
		$null = '';

		// Sanity check
		if (empty($array) || !is_array($array)) {
			return false;
		}

		if (!isset($array[0]) || !is_array($array[0])) {
			$array = array($array);
		}

		$worksheet =& $workbook->addworksheet($titre[$heading_feuille]);


		// compte les lignes et colonnes :
		$nbr_lignes = count ($array);
		$nbr_colonnes = 0;
		$titres_colonne = Array();

		foreach (array_keys($array[0]) as $heading) {
			$titres_colonne[$nbr_colonnes] = $array_titres[$heading];

			$worksheet->set_column($nbr_colonnes+$colonne, $nbr_colonnes+$colonne, $array_tailles[$heading]);

			$nbr_colonnes++;
		}

		// d�finit les formats
		$titre_format =& $workbook->addformat(array(
                                            bold    => 1,
                                            size    => 12,
                                            font    => 'Arial Narrow'
                                        ));
		$titre_format->set_align('top');

		$soustitre_format =& $workbook->addformat(array(
                                            bold    => 1,
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 1
                                        ));
		$soustitre_format->set_text_wrap();
		$soustitre_format->set_align('top');

		$texte_format =& $workbook->addformat(array(
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 1
                                        ));
		$texte_format->set_text_wrap();
		$texte_format->set_align('top');
	
		$i = 1;

		// �crit le titre
		$worksheet->write($ligne-2, $colonne, "Tableau des ".$titre[$heading_feuille], $titre_format);

		// �crit la ligne des soustitres
		$worksheet->write_row($ligne, $colonne, $titres_colonne, $soustitre_format);

		// �crit les lignes suivantes
		foreach ($array as $row) {
			$worksheet->write_row($ligne+$i, $colonne, $row, $texte_format);
			$i++;
		}

		$worksheet->write($ligne+$i+1, $colonne, "Situation au ".date("d.m.Y"), $format);     
	}

	$workbook->close();

	header("Content-Type: application/x-msexcel; name=\"file.xls\"");
	header("Content-Disposition: inline; filename=\"file.xls\"");
	$fh=fopen($fname, "rb");
	fpassthru($fh);
	unlink($fname);

  }

	include 'champs_attributs.php';

	$titres_champs = array_merge ($titres_champs, Array('producteurs.nom_prenom' => 'Producteur'));
	$width_champs = array_merge ($width_champs, Array('producteurs.nom_prenom' => 25));


	$arr_tables_values = "";
	$arr_tables_texts = "";
	$i = 0;

	foreach (array_keys($titres_tables) as $var) {
		if ($i == 0) {
			$arr_tables_texts .= "\"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= "\"" . $var . "\"";
		}
		else {
			$arr_tables_texts .= ", \"" . $titres_tables[$var] . "\"";
			$arr_tables_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_producteurs_values = "";
	$arr_producteurs_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_producteurs) as $var) {
		if ($i == 0) {
			$arr_producteurs_texts .= "\"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= "\"" . $var . "\"";
		}
		else {
			$arr_producteurs_texts .= ", \"" . $titres_champs_producteurs[$var] . "\"";
			$arr_producteurs_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_produits_values = "";
	$arr_produits_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_produits) as $var) {
		if ($i == 0) {
			$arr_produits_texts .= "\"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= "\"" . $var . "\"";
		}
		else {
			$arr_produits_texts .= ", \"" . $titres_champs_produits[$var] . "\"";
			$arr_produits_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_exploitations_values = "";
	$arr_exploitations_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_exploitations) as $var) {
		if ($i == 0) {
			$arr_exploitations_texts .= "\"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= "\"" . $var . "\"";
		}
		else {
			$arr_exploitations_texts .= ", \"" . $titres_champs_exploitations[$var] . "\"";
			$arr_exploitations_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$arr_activites_values = "";
	$arr_activites_texts = "";
	$i = 0;

	foreach (array_keys($titres_champs_activites) as $var) {
		if ($i == 0) {
			$arr_activites_texts .= "\"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= "\"" . $var . "\"";
		}
		else {
			$arr_activites_texts .= ", \"" . $titres_champs_activites[$var] . "\"";
			$arr_activites_values .= ", \"" . $var . "\"";
		}
		$i++;
	}

	$requete = 'SELECT producteurs.id, producteurs.nom AS "producteurs.nom", producteurs.prenom AS "producteurs.prenom", producteurs.fonction AS "producteurs.fonction", producteurs.ambassadeur AS "producteurs.ambassadeur", producteurs.pepa AS "producteurs.pepa", producteurs.description_courte AS "producteurs.description_courte", producteurs.description AS "producteurs.description", producteurs.id AS "producteur_langue", producteurs.responsable AS "producteurs.responsable", producteurs.rue AS "producteurs.rue", producteurs.no_postal AS "producteurs.no_postal", producteurs.ville AS "producteurs.ville", producteurs.tel AS "producteurs.tel", producteurs.fax AS "producteurs.fax", producteurs.mobile AS "producteurs.mobile", producteurs.email AS "producteurs.email", producteurs.site_web AS "producteurs.site_web", producteurs.vente_correspondance AS "producteurs.vente_correspondance" FROM producteurs ORDER BY producteurs.fonction, producteurs.pepa, producteurs.nom';

	$sqlquery= $requete;
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbre_lignes = mysql_num_rows($resultat_sql);

		$array= Array();
		$tableau= Array();

		while($row = mysql_fetch_assoc($resultat_sql)){ 
			$array[] = $row;
		}

		for ($i=0;$i<$nbre_lignes;$i++) {
			foreach (array_keys($array[0]) as $heading) {

				if ($heading == "exploitation_activite") {
					$requete = "SELECT activites.nom FROM activites LEFT OUTER JOIN activite_exploitation ON activites.id = activite_exploitation.id_activite WHERE activite_exploitation.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"activites.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_exploitation") {
					$requete = "SELECT exploitations.nom FROM exploitations LEFT OUTER JOIN activite_exploitation ON exploitations.id = activite_exploitation.id_exploitation WHERE activite_exploitation.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"exploitations.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_type") {
					$requete = "SELECT type FROM activites_types LEFT OUTER JOIN activite_type ON activites_types.id = activite_type.id_type WHERE activite_type.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"type")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_producteur_pepa") {
					$requete = "SELECT producteurs.nom FROM producteurs LEFT OUTER JOIN activite_vente_producteur_pepa ON producteurs.id = activite_vente_producteur_pepa.id_producteur_pepa WHERE activite_vente_producteur_pepa.id_activite = " . $array[$i][$heading]. " ORDER BY producteurs.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_produit") {
					$requete = "SELECT produits.nom FROM produits LEFT OUTER JOIN activite_vente_produit ON activite_vente_produit.id_produit = produits.id WHERE activite_vente_produit.id_activite = " . $array[$i][$heading]. " ORDER BY produits.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"produits.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "exploitation_animal") {
					$requete = "SELECT animal FROM exploitations_animaux LEFT OUTER JOIN exploitation_animal ON exploitations_animaux.id = exploitation_animal.id_animal WHERE exploitation_animal.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"animal")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteur_langue") {
					$requete = "SELECT langue FROM producteurs_langues LEFT OUTER JOIN producteur_langue ON producteurs_langues.id = producteur_langue.id_langue WHERE producteur_langue.id_producteur = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"langue")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "produit_label") {
					$requete = "SELECT label FROM produits_labels LEFT OUTER JOIN produit_label ON produits_labels.id = produit_label.id_label WHERE produit_label.id_produit = " . $array[$i][$heading] . " ORDER BY produit_label.id_label";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"label")."\n";
						}

					$tableau[$i][$heading] = $tmp.$array[$i]['produits.autre_label'];
				}
				elseif ($heading == "produits.autre_label") {
				}
				elseif ($heading == "producteurs.site_web") {
					$tableau[$i][$heading] = " ".$array[$i][$heading];
				}
				else {
					$tableau[$i][$heading] = $array[$i][$heading];
				}
			}
		}

	$tableaux['producteurs'] = $tableau;

	$requete = 'SELECT produits.id, produits.nom AS "produits.nom", producteurs.nom AS "producteurs.nom", producteurs.prenom AS "producteurs.prenom", produits.id AS "produit_label", produits.autre_label AS "produits.autre_label", produits_types.type AS "produits.id_type", produits_lait_types.type AS "produits.id_lait_type", produits_lait_types_laits.type_lait AS "produits.id_lait_type_lait", produits_lait_traitements_laits.traitement_lait AS "produits.id_lait_traitement_lait", produits.lait_specificite AS "produits.lait_specificite", produits.lait_mg_es AS "produits.lait_mg_es", produits.lait_affinage AS "produits.lait_affinage", produits_viande_types.type AS "produits.id_viande_type", produits_viande_conditionnements.conditionnement AS "produits.id_viande_conditionnement", produits.description AS "produits.description", produits.utilisation AS "produits.utilisation", produits.presentation AS "produits.presentation", produits.saison AS "produits.saison", produits_marches.marche AS "produits.id_marche", produits.commentaires AS "produits.commentaires", produits.admission_pepa AS "produits.admission_pepa", produits.decision_cit AS "produits.decision_cit", produits.decision_degust AS "produits.decision_degust", produits.produit_phare AS "produits.produit_phare" FROM produits, producteurs LEFT OUTER JOIN produits_types ON produits.id_type = produits_types.id LEFT OUTER JOIN produits_lait_types ON produits.id_lait_type = produits_lait_types.id LEFT OUTER JOIN produits_lait_types_laits ON produits.id_lait_type_lait = produits_lait_types_laits.id LEFT OUTER JOIN produits_lait_traitements_laits ON produits.id_lait_traitement_lait = produits_lait_traitements_laits.id LEFT OUTER JOIN produits_viande_types ON produits.id_viande_type = produits_viande_types.id LEFT OUTER JOIN produits_viande_conditionnements ON produits.id_viande_conditionnement = produits_viande_conditionnements.id LEFT OUTER JOIN produits_marches ON produits.id_marche = produits_marches.id WHERE produits.id_producteur = producteurs.id ORDER BY producteurs.nom, produits.nom ';

	$sqlquery= $requete;
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbre_lignes = mysql_num_rows($resultat_sql);

		$array= Array();
		$tableau= Array();

		while($row = mysql_fetch_assoc($resultat_sql)){ 
			$array[] = $row;
		}

		for ($i=0;$i<$nbre_lignes;$i++) {
			foreach (array_keys($array[0]) as $heading) {

				if ($heading == "exploitation_activite") {
					$requete = "SELECT activites.nom FROM activites LEFT OUTER JOIN activite_exploitation ON activites.id = activite_exploitation.id_activite WHERE activite_exploitation.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"activites.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_exploitation") {
					$requete = "SELECT exploitations.nom FROM exploitations LEFT OUTER JOIN activite_exploitation ON exploitations.id = activite_exploitation.id_exploitation WHERE activite_exploitation.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"exploitations.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_type") {
					$requete = "SELECT type FROM activites_types LEFT OUTER JOIN activite_type ON activites_types.id = activite_type.id_type WHERE activite_type.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"type")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_producteur_pepa") {
					$requete = "SELECT producteurs.nom FROM producteurs LEFT OUTER JOIN activite_vente_producteur_pepa ON producteurs.id = activite_vente_producteur_pepa.id_producteur_pepa WHERE activite_vente_producteur_pepa.id_activite = " . $array[$i][$heading]. " ORDER BY producteurs.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_produit") {
					$requete = "SELECT produits.nom FROM produits LEFT OUTER JOIN activite_vente_produit ON activite_vente_produit.id_produit = produits.id WHERE activite_vente_produit.id_activite = " . $array[$i][$heading]. " ORDER BY produits.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"produits.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "exploitation_animal") {
					$requete = "SELECT animal FROM exploitations_animaux LEFT OUTER JOIN exploitation_animal ON exploitations_animaux.id = exploitation_animal.id_animal WHERE exploitation_animal.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"animal")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteur_langue") {
					$requete = "SELECT langue FROM producteurs_langues LEFT OUTER JOIN producteur_langue ON producteurs_langues.id = producteur_langue.id_langue WHERE producteur_langue.id_producteur = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"langue")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteurs.nom") {

					$tableau[$i]['producteurs.nom_prenom'] = $array[$i]['producteurs.nom']." ".$array[$i]['producteurs.prenom'];
				}
				elseif ($heading == "producteurs.prenom") {
					// on laisse vide...
				}
				elseif ($heading == "produit_label") {
					$requete = "SELECT label FROM produits_labels LEFT OUTER JOIN produit_label ON produits_labels.id = produit_label.id_label WHERE produit_label.id_produit = " . $array[$i][$heading] . " ORDER BY produit_label.id_label";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"label")."\n";
						}

					$tableau[$i][$heading] = $tmp.$array[$i]['produits.autre_label'];
				}
				elseif ($heading == "produits.autre_label") {
				}
				elseif ($heading == "producteurs.site_web") {
					$tableau[$i][$heading] = " ".$array[$i][$heading];
				}
				else {
					$tableau[$i][$heading] = $array[$i][$heading];
				}
			}
		}

	$tableaux['produits'] = $tableau;

	$requete = 'SELECT exploitations.id, exploitations.nom AS "exploitations.nom", producteurs.nom AS "producteurs.nom", producteurs.prenom AS "producteurs.prenom", exploitations.alpage AS "exploitations.alpage", exploitations.etivaz AS "exploitations.etivaz", exploitations.altitude AS "exploitations.altitude", exploitations.tel AS "exploitations.tel", exploitations.saison AS "exploitations.saison", exploitations.id AS "exploitation_animal", exploitations.commentaires AS "exploitations.commentaires", exploitations_communes.commune AS "exploitations.id_commune", exploitations.handicapes AS "exploitations.handicapes", exploitations.id AS "exploitation_activite" FROM exploitations, producteurs LEFT OUTER JOIN exploitations_communes ON exploitations.id_commune = exploitations_communes.id WHERE exploitations.id_producteur = producteurs.id ORDER BY producteurs.nom, producteurs.prenom, exploitations.nom ';

	$sqlquery = $requete;
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbre_lignes = mysql_num_rows($resultat_sql);

		$array = Array();
		$tableau = Array();

		while($row = mysql_fetch_assoc($resultat_sql)){ 
			$array[] = $row;
		}

		for ($i=0;$i<$nbre_lignes;$i++) {
			foreach (array_keys($array[0]) as $heading) {

				if ($heading == "exploitation_activite") {
					$requete = "SELECT activites.nom FROM activites LEFT OUTER JOIN activite_exploitation ON activites.id = activite_exploitation.id_activite WHERE activite_exploitation.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"activites.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_exploitation") {
					$requete = "SELECT exploitations.nom FROM exploitations LEFT OUTER JOIN activite_exploitation ON exploitations.id = activite_exploitation.id_exploitation WHERE activite_exploitation.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"exploitations.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_type") {
					$requete = "SELECT type FROM activites_types LEFT OUTER JOIN activite_type ON activites_types.id = activite_type.id_type WHERE activite_type.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"type")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_producteur_pepa") {
					$requete = "SELECT producteurs.nom FROM producteurs LEFT OUTER JOIN activite_vente_producteur_pepa ON producteurs.id = activite_vente_producteur_pepa.id_producteur_pepa WHERE activite_vente_producteur_pepa.id_activite = " . $array[$i][$heading]. " ORDER BY producteurs.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_produit") {
					$requete = "SELECT produits.nom FROM produits LEFT OUTER JOIN activite_vente_produit ON activite_vente_produit.id_produit = produits.id WHERE activite_vente_produit.id_activite = " . $array[$i][$heading]. " ORDER BY produits.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"produits.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "exploitation_animal") {
					$requete = "SELECT animal FROM exploitations_animaux LEFT OUTER JOIN exploitation_animal ON exploitations_animaux.id = exploitation_animal.id_animal WHERE exploitation_animal.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"animal")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteur_langue") {
					$requete = "SELECT langue FROM producteurs_langues LEFT OUTER JOIN producteur_langue ON producteurs_langues.id = producteur_langue.id_langue WHERE producteur_langue.id_producteur = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"langue")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteurs.nom") {

					$tableau[$i]['producteurs.nom_prenom'] = $array[$i]['producteurs.nom']." ".$array[$i]['producteurs.prenom'];
				}
				elseif ($heading == "producteurs.prenom") {
					// on laisse vide...
				}
				elseif ($heading == "produit_label") {
					$requete = "SELECT label FROM produits_labels LEFT OUTER JOIN produit_label ON produits_labels.id = produit_label.id_label WHERE produit_label.id_produit = " . $array[$i][$heading] . " ORDER BY produit_label.id_label";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"label")."\n";
						}

					$tableau[$i][$heading] = $tmp.$array[$i]['produits.autre_label'];
				}
				elseif ($heading == "produits.autre_label") {
				}
				elseif ($heading == "producteurs.site_web") {
					$tableau[$i][$heading] = " ".$array[$i][$heading];
				}
				else {
					$tableau[$i][$heading] = $array[$i][$heading];
				}
			}
		}

	$tableaux['exploitations'] = $tableau;

	$requete = 'SELECT activites.id, activites.nom AS "activites.nom", producteurs.nom AS "producteurs.nom", producteurs.prenom AS "producteurs.prenom", activites.id AS "activite_type", activites.description AS "activites.description", activites.id AS "activite_exploitation", activites.id AS "activite_vente_produit", activites.id AS "activite_vente_producteur_pepa", activites.vente_producteur_non_pepa AS "activites.vente_producteur_non_pepa" FROM activites, producteurs WHERE activites.id_producteur = producteurs.id ORDER BY producteurs.nom, activites.nom ';

	$sqlquery= $requete;
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbre_lignes = mysql_num_rows($resultat_sql);

		$array= Array();
		$tableau= Array();

		while($row = mysql_fetch_assoc($resultat_sql)){ 
			$array[] = $row;
		}

		for ($i=0;$i<$nbre_lignes;$i++) {
			foreach (array_keys($array[0]) as $heading) {

				if ($heading == "exploitation_activite") {
					$requete = "SELECT activites.nom FROM activites LEFT OUTER JOIN activite_exploitation ON activites.id = activite_exploitation.id_activite WHERE activite_exploitation.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"activites.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_exploitation") {
					$requete = "SELECT exploitations.nom FROM exploitations LEFT OUTER JOIN activite_exploitation ON exploitations.id = activite_exploitation.id_exploitation WHERE activite_exploitation.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"exploitations.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_type") {
					$requete = "SELECT type FROM activites_types LEFT OUTER JOIN activite_type ON activites_types.id = activite_type.id_type WHERE activite_type.id_activite = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"type")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_producteur_pepa") {
					$requete = "SELECT producteurs.nom FROM producteurs LEFT OUTER JOIN activite_vente_producteur_pepa ON producteurs.id = activite_vente_producteur_pepa.id_producteur_pepa WHERE activite_vente_producteur_pepa.id_activite = " . $array[$i][$heading]. " ORDER BY producteurs.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "activite_vente_produit") {
					$requete = "SELECT produits.nom FROM produits LEFT OUTER JOIN activite_vente_produit ON activite_vente_produit.id_produit = produits.id WHERE activite_vente_produit.id_activite = " . $array[$i][$heading]. " ORDER BY produits.nom";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"produits.nom")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "exploitation_animal") {
					$requete = "SELECT animal FROM exploitations_animaux LEFT OUTER JOIN exploitation_animal ON exploitations_animaux.id = exploitation_animal.id_animal WHERE exploitation_animal.id_exploitation = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"animal")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteur_langue") {
					$requete = "SELECT langue FROM producteurs_langues LEFT OUTER JOIN producteur_langue ON producteurs_langues.id = producteur_langue.id_langue WHERE producteur_langue.id_producteur = " . $array[$i][$heading];
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"langue")."\n";
						}

					$tableau[$i][$heading] = $tmp;
				}
				elseif ($heading == "producteurs.nom") {

					$tableau[$i]['producteurs.nom_prenom'] = $array[$i]['producteurs.nom']." ".$array[$i]['producteurs.prenom'];
				}
				elseif ($heading == "producteurs.prenom") {
					// on laisse vide...
				}
				elseif ($heading == "produit_label") {
					$requete = "SELECT label FROM produits_labels LEFT OUTER JOIN produit_label ON produits_labels.id = produit_label.id_label WHERE produit_label.id_produit = " . $array[$i][$heading] . " ORDER BY produit_label.id_label";
						$resultat_sql = mysql_query($requete,$connexion);
						$nbre_lignes_sousrequete = mysql_num_rows($resultat_sql);

						$tmp = "";

						for ($k=0; $k<$nbre_lignes_sousrequete; $k++) {
							$tmp .= mysql_result($resultat_sql,$k,"label")."\n";
						}

					$tableau[$i][$heading] = $tmp.$array[$i]['produits.autre_label'];
				}
				elseif ($heading == "produits.autre_label") {
				}
				elseif ($heading == "producteurs.site_web") {
					$tableau[$i][$heading] = " ".$array[$i][$heading];
				}
				else {
					$tableau[$i][$heading] = $array[$i][$heading];
				}
			}
		}

	$tableaux['activites'] = $tableau;

	array2xls($tableaux,$titres_champs,$width_champs,$titres_tables);
}

// tableau de suivi //

elseif ($_GET{'document'} == "suivi") {

	$sqlquery= "SELECT id, type FROM produits_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			$produits_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_types[$produits_types_id[$i]] = mysql_result($resultat_sql,$i,"type");

		}

	$sqlquery= "SELECT id, type FROM produits_lait_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			$produits_lait_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_lait_types[$produits_lait_types_id[$i]] = mysql_result($resultat_sql,$i,"type");

		}

	$sqlquery= "SELECT id, type FROM produits_viande_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {

			$produits_viande_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_viande_types[$produits_viande_types_id[$i]] = mysql_result($resultat_sql,$i,"type");

		}

	$sqlquery= "SELECT id, nom, prenom, responsable, rue, no_postal, ville FROM producteurs WHERE pepa = 'oui'";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_producteurs; $i++) {

			$producteur_id[$i] = mysql_result($resultat_sql,$i,"id");
			$producteur_nom[$i] = mysql_result($resultat_sql,$i,"nom");
			$producteur_prenom[$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteur_responsable[$i] = mysql_result($resultat_sql,$i,"responsable");
			$producteur_rue[$i] = mysql_result($resultat_sql,$i,"rue");
			$producteur_no_postal[$i] = mysql_result($resultat_sql,$i,"no_postal");
			$producteur_ville[$i] = mysql_result($resultat_sql,$i,"ville");

		}

	$sqlquery= "SELECT id, nom, id_producteur, id_type, id_lait_type, id_viande_type, admission_pepa, decision_cit, decision_degust, produit_phare FROM produits LEFT OUTER JOIN produit_label on produits.id = produit_label.id_produit WHERE produit_label.id_label = 1";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits; $i++) {

			$produit_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produit_nom[$i] = mysql_result($resultat_sql,$i,"nom");
			$produit_id_type[$i] = mysql_result($resultat_sql,$i,"id_type");
			if ($produit_id_type[$i] == "1") {
				$temp = mysql_result($resultat_sql,$i,"id_lait_type");
				$produit_soustype[$i] = $produits_lait_types[$temp];
			}
			elseif ($produit_id_type[$i] == "2") {
				$temp = mysql_result($resultat_sql,$i,"id_viande_type");
				$produit_soustype[$i] = $produits_viande_types[$temp];
			}
			else {
				$produit_soustype[$i] = "";
			}	
			$produit_admission_pepa[$i] = mysql_result($resultat_sql,$i,"admission_pepa");
			$produit_decision_cit[$i] = mysql_result($resultat_sql,$i,"decision_cit");
			$produit_decision_degust[$i] = mysql_result($resultat_sql,$i,"decision_degust");
			$produit_id_producteur[$i] = mysql_result($resultat_sql,$i,"id_producteur");
			$produit_produit_phare[$i] = mysql_result($resultat_sql,$i,"produit_phare");

			$produit_check[$i]  = "";
		}


	$fname = tempnam("tmp", "tableau_suivi.xls");
	$workbook =& new writeexcel_workbook($fname);
	$worksheet =& $workbook->addworksheet('Tableau de suivi PEPA');

	$worksheet->set_column('A:A', 30);
	$worksheet->set_column('B:B', 20);
	$worksheet->set_column('C:C', 15);
	$worksheet->set_column('D:D', 5);
	$worksheet->set_column('E:E', 15);
	$worksheet->set_column('F:F', 35);
	$worksheet->set_column('G:H', 30);
	$worksheet->set_column('I:K', 12);

	$worksheet->set_row('0', 65);
	$worksheet->set_row('1', 40);

	$titre_format =& $workbook->addformat(array(
                                            bold    => 1,
                                            size    => 12,
                                            font    => 'Arial Narrow',
                                            merge   => 1
                                        ));
	$titre_format->set_align('left');

	$soustitre_format =& $workbook->addformat(array(
                                            bold    => 1,
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 1
                                        ));
	$soustitre_format->set_align('left');
	$soustitre_format->set_text_wrap();

	$texte_format =& $workbook->addformat(array(
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 1
                                        ));

	$texte_format->set_text_wrap();
	$texte_format->set_align('top');

	$format =& $workbook->addformat(array(
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 0
                                        ));

	$format->set_text_wrap();
	$format->set_align('top');

	$texte_format_phare =& $workbook->addformat(array(
                                            size    => 10,
                                            font    => 'Arial Narrow',
                                            border    => 1,
                                            fg_color => 'yellow'
                                        ));

	$texte_format_phare->set_text_wrap();
	$texte_format_phare->set_align('top');

	$worksheet->write('A1', "Tableau de suivi, Pays-d'Enhaut Produits Authentiques", $titre_format);
	$worksheet->insert_bitmap('F1', 'pepa.bmp', 10, 2, 0.4, 0.4);

	$worksheet->write_row('A2', array("Producteur","","","","","Produits","Type de produit","Sous-type de produit","Admission PEPA","D�cision CIT","D�cision commission de d�gustation"), $soustitre_format);

	$worksheet->merge_cells('1','0', '1','4');
	$worksheet->merge_cells('0','0', '0','10');

	$k  = 2;

	for ($i=0; $i<$nbrlignes_producteurs; $i++) {

		$k++;
		$l = 0;

		$case = "A".$k;

		$worksheet->write_row($case, Array($producteur_nom[$i]." ".$producteur_prenom[$i],$producteur_responsable[$i],$producteur_rue[$i],$producteur_no_postal[$i],$producteur_ville[$i]), $texte_format);

		for ($j=0; $j<$nbrlignes_produits; $j++) {

			if ($produit_id_producteur[$j] == $producteur_id[$i]) {

				$produit_check[$j]  = "ok";

				$case = "F".$k;

				if ($produit_produit_phare[$j] == "oui") {
					$worksheet->write_row($case, Array($produit_nom[$j],$produits_types[$produit_id_type[$j]],$produit_soustype[$j]), $texte_format_phare);	
					$case = "I".$k;
					$worksheet->write_row($case, Array($produit_admission_pepa[$j],$produit_decision_cit[$j],$produit_decision_degust[$j]), $texte_format);
				}

				else {
					$worksheet->write_row($case, Array($produit_nom[$j],$produits_types[$produit_id_type[$j]],$produit_soustype[$j],$produit_admission_pepa[$j],$produit_decision_cit[$j],$produit_decision_degust[$j]), $texte_format);
				}
			
				if ($l != 0) {
					$case = "A".$k;
					$worksheet->write_row($case, Array("","","","",""), $texte_format);
				}

				$k++;

				$l++;
			}

		}

		if ($l == 0) {
			$case = "F".$k;
			$worksheet->write_row($case, Array("","","","","",""), $texte_format);

			$k++;
		}
	}

		$case = "A".($k+2); 
		$worksheet->write($case, "L�gende :", $format);  

		$case = "B".($k+2);
		$worksheet->write($case, "Produits phares", $texte_format_phare);  

		$case = "B".($k+3); 
		$worksheet->write($case, "Situation du ".date("d.m.Y"), $format);                 


	$workbook->close();

	header("Content-Type: application/x-msexcel; name=\"tableau_suivi.xls\"");
	header("Content-Disposition: inline; filename=\"tableau_suivi.xls\"");
	$fh=fopen($fname, "rb");
	fpassthru($fh);
	unlink($fname);
}


mysql_close($connexion);

?>