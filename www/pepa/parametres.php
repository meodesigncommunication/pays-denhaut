<?php
	require "config.php";

	$parametre = Array(
		'produits_labels'				=> 'label',
		'produits_lait_traitements_laits'		=> 'traitement_lait',
		'produits_lait_types'				=> 'type',
		'produits_lait_types_laits'			=> 'type_lait',
		'produits_marches'				=> 'marche',
		'produits_types'				=> 'type',
		'produits_viande_conditionnements'		=> 'conditionnement',
		'produits_viande_types'				=> 'type',
		'activites_types'				=> 'type',
		'activites_restauration_hebergement_types'	=> 'type',
		'producteurs_langues'				=> 'langue',
		'exploitations_communes'			=> 'commune',
		'exploitations_animaux'				=> 'animal',
	);
	
	$champ_mere = Array(
		'produits_labels'				=> 'id_label',
		'produits_lait_traitements_laits'		=> 'id_lait_traitement_lait',
		'produits_lait_types'				=> 'id_lait_type',
		'produits_lait_types_laits'			=> 'id_lait_type_lait',
		'produits_marches'				=> 'id_marche',
		'produits_types'				=> 'id_type',
		'produits_viande_conditionnements'		=> 'id_viande_conditionnement',
		'produits_viande_types'				=> 'id_viande_type',
		'activites_types'				=> 'id_type',
		'activites_restauration_hebergement_types'	=> 'id_restauration_hebergement_type',
		'producteurs_langues'				=> 'id_lang',
		'exploitations_communes'			=> 'id_commune',
		'exploitations_animaux'				=> 'id_anim',
	);
	
	$titre = Array(
		'produits_labels'				=> 'Labels',
		'produits_lait_traitements_laits'		=> 'Diff�rents traitements du lait',
		'produits_lait_types'				=> 'Types de produits laitiers',
		'produits_lait_types_laits'			=> 'Types de laits',
		'produits_marches'				=> 'March�s et rayon de diffusion',
		'produits_types'				=> 'Types de produits',
		'produits_viande_conditionnements'		=> 'Types de conditionnement de la viande',
		'produits_viande_types'				=> 'Types de viande',
		'activites_types'				=> 'Types d\'activit�s',
		'activites_restauration_hebergement_types'	=> 'Type de restauration ou d\'h�bergement',
		'producteurs_langues'				=> 'Choix des langues',
		'exploitations_communes'			=> 'Choix des communes',
		'exploitations_animaux'				=> 'Liste des animaux',
	);

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}


	if ($_GET{'action'} == "save") {
		if ($HTTP_POST_VARS['id'] == "") {
			$sqlquery= "INSERT INTO ".$_GET{'parametre'}." (".$parametre[$_GET{'parametre'}].") VALUES ('".addslashes($HTTP_POST_VARS['param'])."')";
				$resultat_sql = mysql_query($sqlquery,$connexion);

			$msg = "Le param�tre a �t� ajout�. En ajouter un nouveau";
		}
		else {
			$sqlquery= "UPDATE ".$_GET{'parametre'}." SET ".$parametre[$_GET{'parametre'}]."='".addslashes($HTTP_POST_VARS['param'])." ' WHERE id=".$HTTP_POST_VARS['id'];
				$resultat_sql = mysql_query($sqlquery,$connexion);
			
			$msg = "Le param�tre n�" . $HTTP_POST_VARS['id'] ." a �t� mis � jour. En ajouter un nouveau";
		}

		$id = "";
		$param = "";
	}
	elseif ($_GET{'action'} == "edit") {
		$id = $_GET{'id'};

		$msg = "�diter le param�tre n�" . $_GET{'id'};
	}
	else {
		$id = "";
		$param = "";

		$msg = "Ajouter un param�tre";
	}

	if ($_GET{'parametre'} == "exploitations_animaux" || $_GET{'parametre'} == "exploitations_communes") {
		$texte = " ORDER BY ".$parametre[$_GET{'parametre'}];
	}
	else {
		$texte = "";
	}

	$sqlquery= "SELECT * FROM ".$_GET{'parametre'}.$texte;
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_parametre = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_parametre; $i++) {
			$parametres_id[$i] = mysql_result($resultat_sql,$i,"id");
			$parametres[$i] = mysql_result($resultat_sql,$i,$parametre[$_GET{'parametre'}]);

			if ($_GET{'action'} == "edit" AND $parametres_id[$i]==$_GET{'id'}) {
				$param = $parametres[$i];
			}
		}


	mysql_close($connexion);

?>

<html>

<head>
<meta http-equiv="Content-Language" content="fr-ch">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Gestion des param�tres</title>

<script language="JavaScript">
<!-- 
	function update() {
		var param_id = new Array(<?php

		if ($_GET{'parametre'} != "producteurs_langues" && $_GET{'parametre'} != "exploitations_animaux") {
			echo "\"0\",";
		}

		for ($i=0; $i<$nbrlignes_parametre; $i++) {
			echo "\"". $parametres_id[$i]."\"";
			if ($i<$nbrlignes_parametre-1) {
				echo ", ";
			}
		}
?>) 
		var param_text = new Array(<?php

		if ($_GET{'parametre'} != "producteurs_langues" && $_GET{'parametre'} != "exploitations_animaux") {
			echo "\"(choisir dans la liste)\",";
		}

		for ($i=0; $i<$nbrlignes_parametre; $i++) {
			echo "\"". $parametres[$i]."\"";
			if ($i<$nbrlignes_parametre-1) {
				echo ", ";
			}
		}
?>) 
		var formulaire = window.opener.document.formulaire.<?php echo $champ_mere[$_GET{'parametre'}]; ?>; 

		formulaire.options.length = param_id.length; 
		for (i=0; i<param_id.length; i++) { 
			formulaire.options[i].value = param_id[i]; 
			formulaire.options[i].text = param_text[i]; 
		} 
	} 
 -->
</script>
<style type="text/css">
<!-- 
p {font-family:Arial Narrow, font-size:2; color:black;}
a {font-family:Arial Narrow, font-size:2; color:black; text-decoration:underline; }
td {font-family:Arial Narrow, font-size:2; color:black;}
 -->
</style>
</head>

<body onload="update()">

<form method="POST" enctype="multipart/form-data" action="parametres.php?parametre=<?php echo $_GET{'parametre'}; ?>&action=save" name="formulaire">
<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; border: 1px dotted #0000FF" bordercolor="#111111" width="100%" id="cadre" height="73">
  <tr>
    <td width="100%"><b><font face="Arial Narrow"><?php echo $titre[$_GET{'parametre'}]; ?></font></b><p>
    <font face="Arial Narrow" size="2"><b>Les �l�ments actuels</b></font></p>
    <ul>
<?php
	for ($i=0; $i<$nbrlignes_parametre; $i++) {
		echo "<li><font face=\"Arial Narrow\" size=\"2\">". $parametres_id[$i]." - " . $parametres[$i] . " (<a href=\"parametres.php?parametre=". $_GET{'parametre'} . "&action=edit&id=". $parametres_id[$i] . "\">�diter</a>)</font></li>\n";
	}
?>
    </ul>
    <p><i><font face="Arial Narrow" size="2" color="red">Pour �viter de perdre des informations dans les fiches existantes, il n'est pas possible de supprimer un param�tre. Merci de faire attention et de n'ajouter que ceux qui sont vraiment n�cessaires.</i></font></p>
    <p><b><font face="Arial Narrow" size="2"><?php echo $msg; ?></font></b><br>
    <input type="text" name="param" value="<?php echo $param; ?>" size="20"><br>
    <input type="hidden" value="<?php echo $id; ?>" name="id"><input type="submit" value="Enregistrer" name="enregistrer"> <input type="button" value="Fermer" onclick="javascript:window.close();"></p></td>
  </tr>
</table>
</form>

</body>

</html>