<?php
	require "config.php";

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	// r�cup�ration des donn�es sur les producteurs :
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE pepa = 'oui' AND fonction = 'producteur' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs1 = mysql_num_rows($resultat_sql);

			$producteurs_id[0] = "0";
			$producteurs[0] = "-PEPA-";
			
		for ($i=0; $i<$nbrlignes_producteurs1; $i++) {
			$producteurs_id[$i+1] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$i+1] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$i+1] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$i+1] = $producteurs_nom[$i+1]." ".$producteurs_prenom[$i+1];
		}
		
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE pepa != 'oui' AND fonction = 'producteur' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs2 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+1] = "0";
			$producteurs[$nbrlignes_producteurs1+1] = "-nonPEPA-";
			
		for ($i=0; $i<$nbrlignes_producteurs2; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+2+$i] = $producteurs_nom[$nbrlignes_producteurs1+2+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+2+$i];
		}	
	
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE fonction = 'commerce' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs3 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+2] = "0";
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+2] = "-commerce-";
			
		for ($i=0; $i<$nbrlignes_producteurs3; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = $producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i];
		}	
	
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE fonction = 'restaurant' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs4 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+3] = "0";
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+3] = "-resto-";
			
		for ($i=0; $i<$nbrlignes_producteurs4; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = $producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i];
		}
	$msg = "Nouvelle photo :";

	if ($_GET{'action'} == "save") {
		if ($HTTP_POST_VARS['id'] == "") {
			if ((isset($_FILES['photo']['tmp_name'])) AND ($_FILES['photo']['error'] == UPLOAD_ERR_OK)) {
				$nom_final = rand() . $_FILES['photo']['name'];
				$chemin = $chemin_photos_pepa . $nom_final ;
				$chemin_original = $chemin_photos_pepa . "originals/" . $nom_final;
				$chemin_thumb = $chemin_photos_pepa . "thumbs/" . $nom_final;

				move_uploaded_file($_FILES['photo']['tmp_name'], $chemin_original);

				// cr�ation de l'image r�duite :

				$info=getimagesize($chemin_original);

				$w_max="500"; 
				$h_max="500"; 

				$coef=min($w_max/$info[0],$h_max/$info[1]);
				$width=round($info[0]*$coef);
				$height=round($info[1]*$coef);

				if ($info[1]>$info[0]) {
					$orientation = "portrait";
				}
				else {
					$orientation = "paysage";
				}

				if ($info[2]=="1") {
					$img=imagecreatefromgif($chemin_original);
				}
				else if ($info[2]=="2") {
					$img=imagecreatefromjpeg($chemin_original);
				}
				else if ($info[2]=="3") {
					$img=imagecreatefrompng($chemin_original);
				}

				$photo_def=imagecreatetruecolor($width,$height);

				ImageCopyResampled($photo_def,$img,0,0,0,0,$width,$height,$info[0],$info[1]);

				imagejpeg($photo_def,$chemin);

				$width1=$width;
				$height1=$height;

				// Cr�ation de la minature

				$w_max="200"; 
				$h_max="200"; 

				$coef=min($w_max/$info[0],$h_max/$info[1]);
				$width=round($info[0]*$coef); 
				$height=round($info[1]*$coef);

				if ($info[2]=="1") {
					$img=imagecreatefromgif($chemin_original);
				}
				else if ($info[2]=="2") {
					$img=imagecreatefromjpeg($chemin_original);
				}
				else if ($info[2]=="3") {
					$img=imagecreatefrompng($chemin_original);
				}

				$photo_def=imagecreatetruecolor($width,$height);

				ImageCopyResampled($photo_def,$img,0,0,0,0,$width,$height,$info[0],$info[1]);

				imagejpeg($photo_def,$chemin_thumb);

				$sqlquery = "INSERT INTO photos (titre, auteur, date_prise_de_vue, mention, description, utilisation_adpe, fichier, width, height, orientation, width_original, height_original, width_miniature, height_miniature, date_creation, date_modification) VALUES ('".addslashes($HTTP_POST_VARS['titre'])."', '".addslashes($HTTP_POST_VARS['auteur'])."', '".addslashes($HTTP_POST_VARS['date_prise_de_vue'])."', '".addslashes($HTTP_POST_VARS['mention'])."', '".addslashes($HTTP_POST_VARS['description'])."', '".addslashes($HTTP_POST_VARS['utilisation_adpe'])."', '".$nom_final."', '".$width1."', '".$height1."', '".$orientation."', '".$info[0]."', '".$info[1]."', '".$width."', '".$height."', CURDATE(), CURDATE())";
					$resultat_sql = mysql_query($sqlquery,$connexion);
					$last_id = mysql_insert_id();

				if ($HTTP_POST_VARS['table'] == "producteur") {
					$id_mere = $HTTP_POST_VARS['id_producteur'];
				}
				elseif ($HTTP_POST_VARS['table'] == "produit") {
					$id_mere = $HTTP_POST_VARS['id_produit'];
				}
				elseif ($HTTP_POST_VARS['table'] == "exploitation") {
					$id_mere = $HTTP_POST_VARS['id_exploitation'];
				}
				elseif ($HTTP_POST_VARS['table'] == "activite") {
					$id_mere = $HTTP_POST_VARS['id_activite'];
				}

				$sqlquery = "INSERT INTO ".$HTTP_POST_VARS['table']."_photo (id_".$HTTP_POST_VARS['table'].", id_photo) values (".$id_mere.", ".$last_id.")";
					$resultat_sql = mysql_query($sqlquery,$connexion);
			}

			$msg = "Nouvelle photo enregistr�e. Utilisez le formulaire ci-dessous pour en ajouter une suppl�mentaire.";
		}
	}	

	mysql_close($connexion);

?>

<html>

<head>
<meta http-equiv="Content-Language" content="fr-ch">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Gestion des photos</title>
<script language="JavaScript">
<!--
	function afficheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='visible';
			document.getElementById(baliseId).style.display='block';
		}
	}

	function cacheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='hidden';
			document.getElementById(baliseId).style.display='none';
		}
	}

	function check_table() {
		if (document.formulaire.table.value == "produit") {
			document.formulaire.id_produit.disabled=false;
			document.formulaire.id_exploitation.disabled=true;
			document.formulaire.id_activite.disabled=true;

			afficheId('produit');
			cacheId('exploitation');
			cacheId('activite');
		}
		else if (document.formulaire.table.value == "exploitation") {
			document.formulaire.id_produit.disabled=true;
			document.formulaire.id_exploitation.disabled=false;
			document.formulaire.id_activite.disabled=true;

			cacheId('produit');
			afficheId('exploitation');
			cacheId('activite');
		}
		else if (document.formulaire.table.value == "activite") {
			document.formulaire.id_produit.disabled=true;
			document.formulaire.id_exploitation.disabled=true;
			document.formulaire.id_activite.disabled=false;

			cacheId('produit');
			cacheId('exploitation');
			afficheId('activite');
		}
		else {
			document.formulaire.id_produit.disabled=true;
			document.formulaire.id_exploitation.disabled=true;
			document.formulaire.id_activite.disabled=true;

			cacheId('produit');
			cacheId('exploitation');
			cacheId('activite');
		}
	}

	function select_producteur() {

		var top=(screen.height-400)/2;
		var left=(screen.width-280)/2;

		window.open("select_producteur_photo.php?producteur="+document.formulaire.id_producteur.value, "select_producteur", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=yes, resizable=yes, width=200, height=20");
	}

	function displayLocalFile (fileName, veldje) {
		if (fileName == "") {
			var url = "images/blanc.gif";
		}
		else {
			var url = 'file:///' + fileName;
		}

		if (document.layers && location.protocol.toLowerCase() != 'file:' && navigator.javaEnabled())
			netscape.security.PrivilegeManager.enablePrivilege
		('UniversalFileRead');
		document[veldje].src= url;

	}

	function test_submit() {
		var ok = "oui";

		if (document.formulaire.table.value == 0) {
			ok = "non";
		}
		if (document.formulaire.id_producteur.value == 0) {
			ok = "non";
		}
		if (document.formulaire.table.value == "produit" && document.formulaire.id_produit.value == 0) {
			ok = "non";
		}
		if (document.formulaire.table.value == "exploitation" && document.formulaire.id_exploitation.value == 0) {
			ok = "non";
		}
		if (document.formulaire.table.value == "activite" && document.formulaire.id_activite.value == 0) {
			ok = "non";
		}
		if (document.formulaire.photo.value == "") {
			ok = "non";
		}
		if (document.formulaire.titre.value == "") {
			ok = "non";
		}
		if (document.formulaire.value == "") {
			ok = "non";
		}
	
		if (ok != "oui") {
			alert("Tous les champs obligatoirs n'ont pas �t� compl�t�s !");
			return false;
		}
		else {
			document.formulaire.envoyer.disabled=true;
			document.formulaire.envoyer.value="Envois en cours ...";
			return true;
		}
	}

 -->
</script>
<style type="text/css">
<!-- 
p {font-family:Arial Narrow, font-size:2; color:black;}
a {font-family:Arial Narrow, font-size:2; color:black; text-decoration:underline; }
td {font-family:Arial Narrow, font-size:2; color:black;}
 -->
</style>
</head>

<body onload="check_table()">

<form method="POST" enctype="multipart/form-data" action="photos.php?action=save" name="formulaire" onsubmit="return test_submit();">
<input type="hidden" name="nombre_photos" value="1" size="2">
<input type="hidden" name="MAX_FILE_SIZE" value="3000000">
<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; border: 1px dotted #0000FF" bordercolor="#111111" width="957">
  <tr>
    <td width="250" style="border-collapse: collapse; border: 1px dotted #0000FF" align="left" valign="top"><?php

include "menu.php";

?></td>
    <td valign="top">
      <table width="700">
    <tr>
      <td width="700" colspan="2" align="left" valign="top"><b><?php echo $msg; ?></b></td>
    </tr>
    <tr>
      <td width="700" colspan="2" align="left" valign="top"><br><b>1. Choix de l'emplacement de la photo</b></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Cette photo d�crit : </td>
      <td width="450" align="left" valign="top"><select size="1" name="table" style="width:330px" onchange="check_table();">
        <option value="0">(choisir dans la liste)</option>
        <option value="producteur">un producteur</option>
        <option value="produit">un produit</option>
        <option value="exploitation">une exploitation</option>
        <option value="activite">une activit�</option>
      </select></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Producteur concern� : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_producteur" style="width:330px" onchange="select_producteur();">
        <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+$nbrlignes_producteurs4+4; $i++) {

			if ($producteurs[$i] == "-PEPA-") {
				echo "      <optgroup label=\"Producteurs PEPA\">";
			}
			elseif ($producteurs[$i] == "-nonPEPA-"){
				echo "      </optgroup>\n      <optgroup label=\"Producteurs non PEPA\">";
			}
			elseif ($producteurs[$i] == "-commerce-"){
				echo "      </optgroup>\n      <optgroup label=\"Commerces\">";
			}
			elseif ($producteurs[$i] == "-resto-"){
				echo "      </optgroup>\n      <optgroup label=\"Restaurants\">";
			}
			else {
				echo "        <option value=\"$producteurs_id[$i]\">$producteurs[$i]</option>\n";
			}
	}
?>
      </optgroup></select></td>
    </tr>
  </table>
  <div id="produit">
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Produit concern� : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_produit" style="width:330px">
<?php
	if ($nbrlignes_produits != 0) {
		for ($i=0; $i<$nbrlignes_produits; $i++) {
			echo "      <option value=\"".$produits_id[$i]."\" ". $photo_produit[$produits_id[$i]].">".$produits[$i]."<br>\n";
		}
	}
	else {
		if ($id_producteur == "" OR $id_producteur == "0") {
			echo "        <option value=\"0\">(veuillez choisir un producteur...)</option>\n";
		}
		else {
			echo "        <option value=\"0\">(aucun produit enregistr�)</option>\n";
		}
	}
?>
      </select></td>
    </tr>
  </table>
  </div>
  <div id="exploitation">
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Exploitation concern�e : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_exploitation" style="width:330px">
<?php
	if ($nbrlignes_exploitations != 0) {
		for ($i=0; $i<$nbrlignes_exploitations; $i++) {
			echo "      <option value=\"".$exploitations_id[$i]."\" ". $photo_exploitation[$exploitations_id[$i]].">".$exploitations[$i]."<br>\n";
		}
	}
	else {
		if ($id_producteur == "" OR $id_producteur == "0") {
			echo "        <option value=\"0\">(veuillez choisir un producteur...)</option>\n";
		}
		else {
			echo "        <option value=\"0\">(aucune exploitation enregistr�e)</option>\n";
		}
	}
?>
      </select></td>
    </tr>
  </table>
  </div>
  <div id="activite">
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Activit� concern�e : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_activite" style="width:330px">
<?php
	if ($nbrlignes_activites != 0) {
		for ($i=0; $i<$nbrlignes_activites; $i++) {
			echo "      <option value=\"".$activites_id[$i]."\" ". $photo_activite[$activites_id[$i]].">".$activites[$i]."<br>\n";
		}
	}
	else {
		if ($id_producteur == "" OR $id_producteur == "0") {
			echo "        <option value=\"0\">(veuillez choisir un producteur...)</option>\n";
		}
		else {
			echo "        <option value=\"0\">(aucune activit� enregistr�e)</option>\n";
		}
	}
?>
      </select></td>
    </tr>
  </table>
  </div>
  <table>
    <tr>
      <td width="700" colspan="2" align="left" valign="top"><br><b>2. Informations sur la photo</b><br>
      La taille des images est limit�e � 3 Mo. Merci d'utiliser du JPG en haute r�solution si vous souhaitez que ces images puissent �tre r�utilis�es pour la production de documents papier (flyer, brochure, etc...)<br><br>
      </p></td>
    </tr>
  </table>
  <center>
  <table>
    <tr>
      <td width="250" align="right" valign="top">* Photo : </td>
      <td width="450" valign="top">
      <input type="file" name="photo" size="50" onchange="displayLocalFile(document.formulaire.photo.value,'preview');"><br>
      <img name="preview" src="images/blanc.gif" width="70" height="70" border="0" align="middle"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Titre de la photo : </td>
      <td width="450" valign="top"><input type="text" name="titre" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Description : <br><i>(<a href="code.html" target="_blank">r�gles de mise en forme</a>)</i></td>
      <td width="450" valign="top">
      <textarea rows="5" name="description" cols="40"></textarea></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Nom de l'auteur : </td>
      <td width="450" valign="top"><input type="text" name="auteur" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Date de prise de vue : </td>
      <td width="450" valign="top"><input type="text" name="date_prise_de_vue" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Mention, copyright : </td>
      <td width="450" valign="top"><input type="text" name="mention" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Cette photo peut-elle �tre utilis�e par l'ADPE pour la 
      promotion des produits r�gionaux et du tourisme rural ? </td>
      <td width="450" valign="top">
      <input type="radio" value="oui" checked name="utilisation_adpe">oui
      <input type="radio" name="utilisation_adpe" value="non">non<br></td>
    </tr>
  </table>
  </center>
  <table>
    <tr>
      <td width="700" colspan="2" align="center" valign="top"><input type="submit" value="Envoyer la photo" name="envoyer"></td>
    </tr>
  </table>
    </td>
  </tr>
</table>
</form>

</body>

</html>