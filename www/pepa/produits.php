<?php
	require "config.php";

	$url_referer = parse_url($_SERVER['HTTP_REFERER']);

	if ($url_referer[path] == "/pepa/listes.php") {
		$referer = $_SERVER['HTTP_REFERER'];
	}
	else {
		$referer = $HTTP_POST_VARS['referer'];
	}

	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	$sqlquery= "SELECT * FROM produits_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_types = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_types; $i++) {
			$produits_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_types[$i] = mysql_result($resultat_sql,$i,"type");
		}


	$sqlquery= "SELECT * FROM produits_lait_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_lait_types = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_lait_types; $i++) {
			$produits_lait_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_lait_types[$i] = mysql_result($resultat_sql,$i,"type");
		}


	$sqlquery= "SELECT * FROM produits_lait_types_laits";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_lait_types_laits = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_lait_types_laits; $i++) {
			$produits_lait_types_laits_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_lait_types_laits[$i] = mysql_result($resultat_sql,$i,"type_lait");
		}


	$sqlquery= "SELECT * FROM produits_lait_traitements_laits";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_lait_traitements_laits = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_lait_traitements_laits; $i++) {

			$produits_lait_traitements_laits_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_lait_traitements_laits[$i] = mysql_result($resultat_sql,$i,"traitement_lait");

		}

	$sqlquery= "SELECT * FROM produits_viande_types";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_viande_types = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_viande_types; $i++) {

			$produits_viande_types_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_viande_types[$i] = mysql_result($resultat_sql,$i,"type");

		}

	$sqlquery= "SELECT * FROM produits_viande_conditionnements";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_viande_conditionnements = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_viande_conditionnements; $i++) {

			$produits_viande_conditionnements_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_viande_conditionnements[$i] = mysql_result($resultat_sql,$i,"conditionnement");

		}

	$sqlquery= "SELECT * FROM produits_marches";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_marches = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_marches; $i++) {

			$produits_marches_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_marches[$i] = mysql_result($resultat_sql,$i,"marche");

		}


	// r�cup�ration des donn�es sur les producteurs :
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE pepa = 'oui' AND fonction = 'producteur' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs1 = mysql_num_rows($resultat_sql);

			$producteurs_id[0] = "0";
			$producteurs[0] = "-PEPA-";
			
		for ($i=0; $i<$nbrlignes_producteurs1; $i++) {
			$producteurs_id[$i+1] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$i+1] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$i+1] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$i+1] = $producteurs_nom[$i+1]." ".$producteurs_prenom[$i+1];
		}
		
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE pepa != 'oui' AND fonction = 'producteur' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs2 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+1] = "0";
			$producteurs[$nbrlignes_producteurs1+1] = "-nonPEPA-";
			
		for ($i=0; $i<$nbrlignes_producteurs2; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+2+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+2+$i] = $producteurs_nom[$nbrlignes_producteurs1+2+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+2+$i];
		}	
	
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE fonction = 'commerce' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs3 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+2] = "0";
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+2] = "-commerce-";
			
		for ($i=0; $i<$nbrlignes_producteurs3; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i] = $producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+3+$i];
		}	
	
	$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE fonction = 'restaurant' ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_producteurs4 = mysql_num_rows($resultat_sql);
			
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+3] = "0";
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+3] = "-resto-";
			
		for ($i=0; $i<$nbrlignes_producteurs4; $i++) {
			$producteurs_id[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"id");
			$producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"nom");
			$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = mysql_result($resultat_sql,$i,"prenom");
			$producteurs[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i] = $producteurs_nom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i]." ".$producteurs_prenom[$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+4+$i];
		}

	$sqlquery= "SELECT * FROM produits_labels";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes_produits_labels = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes_produits_labels; $i++) {

			$produits_labels_id[$i] = mysql_result($resultat_sql,$i,"id");
			$produits_labels[$i] = mysql_result($resultat_sql,$i,"label");

		}


	if ($_GET{'action'} == "edit") {
		$sqlquery= "SELECT * FROM produit_label WHERE id_produit=" . $_GET{'id'};
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes_produit_label = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes_produit_label; $i++) {

				$produit_label_id[$i] = mysql_result($resultat_sql,$i,"id_label");
				$produit_label[$produit_label_id[$i]] = "checked";

			}

		$sqlquery= "SELECT * FROM produit_photo WHERE id_produit=" . $_GET{'id'} . " ORDER BY ordre, id_photo";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes_produit_photo = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes_produit_photo; $i++) {

				$produit_photo_id[$i] = mysql_result($resultat_sql,$i,"id_photo");
				$sqlquery2 = "SELECT fichier, width_miniature, height_miniature FROM photos WHERE id=" . $produit_photo_id[$i];
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$produit_photo[$i] = mysql_result($resultat_sql2,0,"fichier");
					$produit_photo_width[$i] = mysql_result($resultat_sql2,0,"width_miniature");
					$produit_photo_height[$i] = mysql_result($resultat_sql2,0,"height_miniature");

			}

		$sqlquery= "SELECT DISTINCT * FROM produits WHERE produits.id =" . $_GET{'id'};

			$resultat_sql = mysql_query($sqlquery,$connexion);
	
			$nom = mysql_result($resultat_sql,0,"nom");
			$description = mysql_result($resultat_sql,0,"description");
			$utilisation = mysql_result($resultat_sql,0,"utilisation");
			$id_type = mysql_result($resultat_sql,0,"id_type");
			$id_lait_type = mysql_result($resultat_sql,0,"id_lait_type");
			$id_lait_type_lait = mysql_result($resultat_sql,0,"id_lait_type_lait");
			$id_lait_traitement_lait = mysql_result($resultat_sql,0,"id_lait_traitement_lait");
			$lait_specificite = mysql_result($resultat_sql,0,"lait_specificite");
			$lait_mg_es = mysql_result($resultat_sql,0,"lait_mg_es");
			$lait_affinage = mysql_result($resultat_sql,0,"lait_affinage");
			$id_viande_type = mysql_result($resultat_sql,0,"id_viande_type");
			$id_viande_conditionnement = mysql_result($resultat_sql,0,"id_viande_conditionnement");
			$presentation = mysql_result($resultat_sql,0,"presentation");
			$saison = mysql_result($resultat_sql,0,"saison");
			$id_marche = mysql_result($resultat_sql,0,"id_marche");
			$id_producteur = mysql_result($resultat_sql,0,"id_producteur");
			$commentaires = mysql_result($resultat_sql,0,"commentaires");
			$admission_pepa = mysql_result($resultat_sql,0,"admission_pepa");
			$decision_cit = mysql_result($resultat_sql,0,"decision_cit");
			$decision_degust = mysql_result($resultat_sql,0,"decision_degust");
			$produit_phare = mysql_result($resultat_sql,0,"produit_phare");
			$autre_label = mysql_result($resultat_sql,0,"autre_label");
			$logo = mysql_result($resultat_sql,0,"logo");
			$id = $_GET{'id'};

			$msg = "Modification du produit ".$nom.". ";
			$msg .= $_GET{'msg'};

			if ($autre_label != "") {
				$autre_label_checkbox = "checked";
			}
			else {
				$autre_label_checkbox = "";
			}

	}
	else {

			$nom = "";
			$description = "";
			$utilisation = "";
			$id_type = "";
			$id_lait_type = "";
			$id_lait_type_lait = "";
			$id_lait_traitement_lait = "";
			$lait_specificite = "";
			$lait_mg_es = "";
			$lait_affinage = "";
			$id_viande_type = "";
			$id_viande_conditionnement = "";
			$presentation = "";
			$saison = "";
			$id_marche = "";
			$id_producteur = "";
			$commentaires = "";
			$admission_pepa = "";
			$decision_cit = "";
			$decision_degust = "";
			$produit_phare = "";
			$autre_label = "";
			$logo = "";
			$id = "";

			$msg = "Nouveau produit :";
	}

	if ($_GET{'action'} == "save") {

		if ($HTTP_POST_VARS['produit_phare'] == "") {
			$HTTP_POST_VARS['produit_phare']  = "non";
		}

		if ($HTTP_POST_VARS['suppr_logo'] == "oui") {

			$chemin  = "";
		}
		else {
			if ($HTTP_POST_VARS['old_logo']) {
				$chemin  = $HTTP_POST_VARS['old_logo'];
			}
			else {
				$chemin  = "";
			}
		}

		if (isset($_FILES['logo']['tmp_name']) AND ($_FILES['logo']['error'] == UPLOAD_ERR_OK)) {

			$nom_logo = rand() . $_FILES['logo']['name'];
			$chemin_original = $chemin_logos_pepa."originals/" . $nom_logo;
			$chemin = $chemin_logos_pepa . $nom_logo;
			move_uploaded_file($_FILES['logo']['tmp_name'], $chemin_original);
			
			// cr�ation de l'image r�duite :
			$info=getimagesize($chemin_original);

			$w_max="300"; 
			$h_max="300"; 

			$coef=min($w_max/$info[0],$h_max/$info[1]);
			$width=round($info[0]*$coef);
			$height=round($info[1]*$coef);

			if ($info[2]=="1") {
				$img=imagecreatefromgif($chemin_original);
			}
			else if ($info[2]=="2") {
				$img=imagecreatefromjpeg($chemin_original);
			}
			else if ($info[2]=="3") {
				$img=imagecreatefrompng($chemin_original);
			}

			$logo_def=imagecreatetruecolor($width,$height);
			ImageCopyResampled($logo_def,$img,0,0,0,0,$width,$height,$info[0],$info[1]);
			imagejpeg($logo_def,$chemin);

			if ($HTTP_POST_VARS['old_logo'] != "") {

				unlink($chemin_logos_pepa.$HTTP_POST_VARS['old_logo']);
				unlink($chemin_logos_pepa."originals/".$HTTP_POST_VARS['old_logo']);
			}
		}
		else {
			if ($HTTP_POST_VARS['suppr_logo'] == "oui") {

				unlink($chemin_logos_pepa.$HTTP_POST_VARS['old_logo']);
				unlink($chemin_logos_pepa."originals/".$HTTP_POST_VARS['old_logo']);
			}
		}

		if ($HTTP_POST_VARS['id'] == "") {

			$sqlquery= "INSERT INTO produits (nom, description, utilisation, id_type, id_lait_type, id_lait_type_lait, id_lait_traitement_lait, lait_specificite, lait_mg_es, lait_affinage, id_viande_type, id_viande_conditionnement, presentation, saison, id_marche, id_producteur, commentaires, admission_pepa, decision_cit, decision_degust, produit_phare, logo, autre_label, date_creation, date_modification) VALUES ('".addslashes($HTTP_POST_VARS['nom'])."', '".addslashes($HTTP_POST_VARS['description'])."', '".addslashes($HTTP_POST_VARS['utilisation'])."', '".addslashes($HTTP_POST_VARS['id_type'])."', '".addslashes($HTTP_POST_VARS['id_lait_type'])."', '".addslashes($HTTP_POST_VARS['id_lait_type_lait'])."', '".addslashes($HTTP_POST_VARS['id_lait_traitement_lait'])."', '".addslashes($HTTP_POST_VARS['lait_specificite'])."', '".addslashes($HTTP_POST_VARS['lait_mg_es'])."', '".addslashes($HTTP_POST_VARS['lait_affinage'])."', '".addslashes($HTTP_POST_VARS['id_viande_type'])."', '".addslashes($HTTP_POST_VARS['id_viande_conditionnement'])."', '".addslashes($HTTP_POST_VARS['presentation'])."', '".addslashes($HTTP_POST_VARS['saison'])."', '".addslashes($HTTP_POST_VARS['id_marche'])."', '".addslashes($HTTP_POST_VARS['id_producteur'])."', '".addslashes($HTTP_POST_VARS['commentaires'])."', '".addslashes($HTTP_POST_VARS['admission_pepa'])."', '".addslashes($HTTP_POST_VARS['decision_cit'])."', '".addslashes($HTTP_POST_VARS['decision_degust'])."', '".addslashes($HTTP_POST_VARS['produit_phare'])."', '".$nom_logo."', '".addslashes($HTTP_POST_VARS['autre_label'])."', CURDATE(), CURDATE())";

				$resultat_sql = mysql_query($sqlquery,$connexion);
				$new_id = mysql_insert_id();

			$msg = "Nouveau produit enregistr� (".$HTTP_POST_VARS['nom']."). Utilisez le formulaire ci-dessous pour en ajouter un suppl�mentaire.";
		}
		else {
			
			$sqlquery= "UPDATE produits SET nom='".addslashes($HTTP_POST_VARS['nom'])."', description='".addslashes($HTTP_POST_VARS['description'])."', utilisation='".addslashes($HTTP_POST_VARS['utilisation'])."', id_type='".addslashes($HTTP_POST_VARS['id_type'])."', id_lait_type='".addslashes($HTTP_POST_VARS['id_lait_type'])."', id_lait_type_lait='".addslashes($HTTP_POST_VARS['id_lait_type_lait'])."', id_lait_traitement_lait='".addslashes($HTTP_POST_VARS['id_lait_traitement_lait'])."', lait_specificite='".addslashes($HTTP_POST_VARS['lait_specificite'])."', lait_mg_es='".addslashes($HTTP_POST_VARS['lait_mg_es'])."', lait_affinage='".addslashes($HTTP_POST_VARS['lait_affinage'])."', id_viande_type='".addslashes($HTTP_POST_VARS['id_viande_type'])."', id_viande_conditionnement='".addslashes($HTTP_POST_VARS['id_viande_conditionnement'])."', presentation='".addslashes($HTTP_POST_VARS['presentation'])."', saison='".addslashes($HTTP_POST_VARS['saison'])."', id_marche='".addslashes($HTTP_POST_VARS['id_marche'])."', id_producteur='".addslashes($HTTP_POST_VARS['id_producteur'])."', commentaires='".addslashes($HTTP_POST_VARS['commentaires'])."', admission_pepa='".addslashes($HTTP_POST_VARS['admission_pepa'])."', decision_cit='".addslashes($HTTP_POST_VARS['decision_cit'])."', decision_degust='".addslashes($HTTP_POST_VARS['decision_degust'])."', produit_phare='".addslashes($HTTP_POST_VARS['produit_phare'])."', logo='".$nom_logo."', autre_label='".addslashes($HTTP_POST_VARS['autre_label'])."', date_modification=CURDATE() WHERE id='".$HTTP_POST_VARS['id']."'";

				$resultat_sql = mysql_query($sqlquery,$connexion);
				$new_id = $HTTP_POST_VARS['id'];

			$sqlquery= "DELETE FROM produit_label WHERE id_produit='".$new_id."'";
		
				$resultat_sql = mysql_query($sqlquery,$connexion);

			$msg = "Produit \"".$HTTP_POST_VARS['nom']."\" mis � jour.";
		}

		if ($HTTP_POST_VARS['labels']) {
			foreach($HTTP_POST_VARS['labels'] as $id_label) {
				$sqlquery= "INSERT INTO produit_label (id_produit, id_label) VALUES ('".$new_id."', '".$id_label."')";
		
					$resultat_sql = mysql_query($sqlquery,$connexion);
			}
		}

		if ($HTTP_POST_VARS['ordre']) {
			foreach(array_keys($HTTP_POST_VARS['ordre']) as $id_ordre) {
				if ($HTTP_POST_VARS['ordre'][$id_ordre] != "") {
					$sqlquery= "UPDATE produit_photo SET ordre = '".$HTTP_POST_VARS['ordre'][$id_ordre]."' WHERE id_produit = '".$new_id."' AND id_photo = '".$id_ordre."'";
						$resultat_sql = mysql_query($sqlquery,$connexion);
				}
			}
		}

		if ($referer != "" AND $HTTP_POST_VARS['id'] != "") {
			Header ("Location: ".$referer."&msg=".$msg);
			exit();
		}
	}	

	mysql_close($connexion);

?>

<html>

<head>
<meta http-equiv="Content-Language" content="fr-ch">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Gestion des produits</title>
<script language="JavaScript">
<!--
	function afficheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='visible';
			document.getElementById(baliseId).style.display='block';
		}
	}

	function cacheId(baliseId) {
		if (document.getElementById && document.getElementById(baliseId) != null) {
			document.getElementById(baliseId).style.visibility='hidden';
			document.getElementById(baliseId).style.display='none';
		}
	}


	function check_lait() {

		if (document.formulaire.id_type.value == "1") {
			document.formulaire.id_lait_type.disabled=false;
			document.formulaire.id_lait_type_lait.disabled=false;
			document.formulaire.id_lait_traitement_lait.disabled=false;
			document.formulaire.lait_specificite.disabled=false;
			document.formulaire.lait_mg_es.disabled=false;
			document.formulaire.lait_affinage.disabled=false;

			afficheId('lait');
		}
		else {
			document.formulaire.id_lait_type.disabled=true;
			document.formulaire.id_lait_type_lait.disabled=true;
			document.formulaire.id_lait_traitement_lait.disabled=true;
			document.formulaire.lait_specificite.disabled=true;
			document.formulaire.lait_mg_es.disabled=true;
			document.formulaire.lait_affinage.disabled=true;

			cacheId('lait');
		}
	}

	function check_viande() {

		if (document.formulaire.id_type.value == "2") {
			document.formulaire.id_viande_type.disabled=false;
			document.formulaire.id_viande_conditionnement.disabled=false;

			afficheId('viande');
		}
		else {
			document.formulaire.id_viande_type.disabled=true;
			document.formulaire.id_viande_conditionnement.disabled=true;

			cacheId('viande');
		}
	}

	function check_pepa() {

		if (document.formulaire.label_pepa.checked == true) {
			document.formulaire.admission_pepa.disabled=false;
			document.formulaire.decision_cit.disabled=false;
			document.formulaire.decision_degust.disabled=false;
			document.formulaire.utilisation.disabled=false;
			document.formulaire.presentation.disabled=false;
			document.formulaire.commentaires.disabled=false;
			document.formulaire.produit_phare[0].disabled=false;
			document.formulaire.produit_phare[1].disabled=false;
			document.formulaire.logo.disabled=false;

			afficheId('pepa1');
			afficheId('pepa2');
			afficheId('pepa3');
		}
		else {
			document.formulaire.admission_pepa.disabled=true;
			document.formulaire.decision_cit.disabled=true;
			document.formulaire.decision_degust.disabled=true;
			document.formulaire.utilisation.disabled=true;
			document.formulaire.presentation.disabled=true;
			document.formulaire.commentaires.disabled=true;
			document.formulaire.produit_phare[0].disabled=true;
			document.formulaire.produit_phare[1].disabled=true;
			document.formulaire.logo.disabled=true;

			cacheId('pepa1');
			cacheId('pepa2');
			cacheId('pepa3');
		}
	}


	function check_label_autre() {

		if (document.formulaire.autre_label_checkbox.checked == true) {
			document.formulaire.autre_label.disabled=false;
		}
		else {
			document.formulaire.autre_label.disabled=true;
		}
	}


	function parametre(param) {

		var top=(screen.height-400)/2;
		var left=(screen.width-280)/2;

		window.open("parametres.php?parametre="+param, "parametre", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=yes, resizable=yes, width=280, height=400");
	}

	function confirm(type,id) {

		var top=(screen.height-150)/2;
		var left=(screen.width-400)/2;
		window.name = "Listes";

		window.open("delete.php?type="+type+"&id="+id, "delete", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=no, resizable=no, width=400, height=150");
	}

	function test_submit(){

		var ok = "oui";

		if (document.formulaire.id_producteur.value == '' || document.formulaire.id_producteur.value == 0) {
			ok = "non";
		}
		if (document.formulaire.nom.value == '') {
			ok = "non";
		}
		if (document.formulaire.id_type.value == '' || document.formulaire.id_type.value == 0) {
			ok = "non";
		}
		if (document.formulaire.id_marche.value == '' || document.formulaire.id_marche.value == 0) {
			ok = "non";
		}
		if (document.formulaire.id_type.value == 1 && (document.formulaire.id_lait_type.value == '' || document.formulaire.id_lait_type.value == 0)) {
			ok = "non";
		}
		if ((document.formulaire.id_type.value == 2) && (document.formulaire.id_viande_type.value == '' || document.formulaire.id_viande_type.value == 0)) {
			ok = "non";
		}
		if ((document.formulaire.id_type.value == 2) && (document.formulaire.id_viande_conditionnement.value == '' || document.formulaire.id_viande_conditionnement.value == 0)) {
			ok = "non";
		}

		if (ok != "oui") {
			alert("Tous les champs obligatoirs n'ont pas �t� compl�t�s !");
			return false;
		}
		else {
			return true;
		}
	}

 -->
</script>
<style type="text/css">
<!-- 
p {font-family:Arial Narrow, font-size:2; color:black;}
a {font-family:Arial Narrow, font-size:2; color:black; text-decoration:underline; }
td {font-family:Arial Narrow, font-size:2; color:black;}
 -->
</style>
</head>

<body onload="check_lait();check_viande();check_label_autre();check_pepa();">

<form method="POST" enctype="multipart/form-data" action="produits.php?action=save" name="formulaire" onsubmit="return test_submit();">
<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; border: 1px dotted #0000FF" bordercolor="#111111" width="957">
  <tr>
    <td width="250" style="border-collapse: collapse; border: 1px dotted #0000FF" align="left" valign="top"><?php

include "menu.php";

?></td>
    <td width="700" style="border-collapse: collapse; border: 1px dotted #0000FF" valign="top"><b><font face="Arial Narrow">
  <table width="700">
    <tr>
      <td width="700" colspan="2" align="left" valign="top"><b><?php echo $msg; ?><br></b></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Producteur : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_producteur" style="width:330px">
        <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_producteurs1+$nbrlignes_producteurs2+$nbrlignes_producteurs3+$nbrlignes_producteurs4+4; $i++) {

		if (($id_producteur == $producteurs_id[$i]) AND ($producteurs_id[$i] != 0)) {
			echo "        <option value=\"$producteurs_id[$i]\" selected>$producteurs[$i]</option>\n";
		}
		else {
			if ($producteurs[$i] == "-PEPA-") {
				echo "      <optgroup label=\"Producteurs PEPA\">";
			}
			elseif ($producteurs[$i] == "-nonPEPA-"){
				echo "      </optgroup>\n      <optgroup label=\"Producteurs non PEPA\">";
			}
			elseif ($producteurs[$i] == "-commerce-"){
				echo "      </optgroup>\n      <optgroup label=\"Commerces\">";
			}
			elseif ($producteurs[$i] == "-resto-"){
				echo "      </optgroup>\n      <optgroup label=\"Restaurants\">";
			}
			else {
				echo "        <option value=\"$producteurs_id[$i]\">$producteurs[$i]</option>\n";
			}
		}

	}
?>
      </optgroup></select></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Nom du produit : </td>
      <td width="450" align="left" valign="top"><input type="text" name="nom" value="<?php echo $nom; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Label(s) : </td>
      <td width="450" align="left" valign="top">
<?php
	for ($i=0; $i<$nbrlignes_produits_labels; $i++) {

		if ($produits_labels_id[$i] == "1") {
			$texte = " id=\"label_pepa\" onclick=\"check_pepa();\"";
		}
		else {
			$texte = "";
		}

		echo "      <input type=\"checkbox\" name=\"labels[]\" value=\"".$produits_labels_id[$i]."\" ". $produit_label[$produits_labels_id[$i]].$texte.">".$produits_labels[$i]."<br>\n";

	}
?>
      <input type="checkbox" name="autre_label_checkbox" value="oui" onClick="check_label_autre();" <?php echo $autre_label_checkbox; ?>>Autre : <input type="text" name="autre_label" value="<?php echo $autre_label; ?>" size="20"><br></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Type de produit : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_type" onChange="check_lait();check_viande();" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_types; $i++) {

		if ($id_type == $produits_types_id[$i]) {
			echo "      <option value=\"$produits_types_id[$i]\" selected>$produits_types[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_types_id[$i]\">$produits_types[$i]</option>\n";
		}

	}
?>

      </select> (<a href="#" onclick="javascript:parametre('produits_types');">�diter la liste</a>)</td>
    </tr>
  </table>
  <div id="lait">
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top"><font>* Type de produit laitier : </font></td>
      <td width="450" align="left" valign="top"><select size="1" name="id_lait_type" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_lait_types; $i++) {

		if ($id_lait_type == $produits_lait_types_id[$i]) {
			echo "      <option value=\"$produits_lait_types_id[$i]\" selected>$produits_lait_types[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_lait_types_id[$i]\">$produits_lait_types[$i]</option>\n";
		}

	}
?>
      </select><font> (<a href="#" onclick="javascript:parametre('produits_lait_types');">�diter la liste</a>)</font></td>
    </tr>
  </table>
  <div id="pepa1">
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top"><font>* Type de lait : </font></td>
      <td width="450" align="left" valign="top"><select size="1" name="id_lait_type_lait" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_lait_types_laits; $i++) {

		if ($id_lait_type_lait == $produits_lait_types_laits_id[$i]) {
			echo "      <option value=\"$produits_lait_types_laits_id[$i]\" selected>$produits_lait_types_laits[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_lait_types_laits_id[$i]\">$produits_lait_types_laits[$i]</option>\n";
		}

	}
?>
      </select><font> (<a href="#" onclick="javascript:parametre('produits_lait_types_laits');">�diter la liste</a>)</font></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top"><font>* Traitement du lait : </font></td>
      <td width="450" align="left" valign="top"><select size="1" name="id_lait_traitement_lait" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_lait_traitements_laits; $i++) {

		if ($id_lait_traitement_lait == $produits_lait_traitements_laits_id[$i]) {
			echo "      <option value=\"$produits_lait_traitements_laits_id[$i]\" selected>$produits_lait_traitements_laits[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_lait_traitements_laits_id[$i]\">$produits_lait_traitements_laits[$i]</option>\n";
		}

	}
?>
      </select><font> (<a href="#" onclick="javascript:parametre('produits_lait_traitements_laits');">�diter la liste</a>)</font></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top"><font>Sp�cificit�s <i>(ex: croute lav�e)</i> : </font></td>
      <td width="450" align="left" valign="top"><input type="text" name="lait_specificite" value="<?php echo $lait_specificite; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top"><font>Mg/Es : </font></td>
      <td width="450" align="left" valign="top"><input type="text" name="lait_mg_es" value="<?php echo $lait_mg_es; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top"><font>Affinage : </font></td>
      <td width="450" align="left" valign="top"><input type="text" name="lait_affinage" value="<?php echo $lait_affinage; ?>" size="50"></td>
    </tr>
  </table>
  </div>
  </div>
  <div id="viande">
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top"><font>* Type de viande : </font></td>
      <td width="450" align="left" valign="top"><select size="1" name="id_viande_type" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_viande_types; $i++) {

		if ($id_viande_type == $produits_viande_types_id[$i]) {
			echo "      <option value=\"$produits_viande_types_id[$i]\" selected>$produits_viande_types[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_viande_types_id[$i]\">$produits_viande_types[$i]</option>\n";
		}

	}
?>
      </select><font> (<a href="#" onclick="javascript:parametre('produits_viande_types');">�diter la liste</a>)</font></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top"><font>* Conditionnement : </font></td>
      <td width="450" align="left" valign="top"><select size="1" name="id_viande_conditionnement" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_viande_conditionnements; $i++) {

		if ($id_viande_conditionnement == $produits_viande_conditionnements_id[$i]) {
			echo "      <option value=\"$produits_viande_conditionnements_id[$i]\" selected>$produits_viande_conditionnements[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_viande_conditionnements_id[$i]\">$produits_viande_conditionnements[$i]</option>\n";
		}

	}
?>
      </select><font> (<a href="#" onclick="javascript:parametre('produits_viande_conditionnements');">�diter la liste</a>)</font></td>
    </tr>
  </table>
  </div>
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top">Description : <br><i>(<a href="code.html" target="_blank">r�gles de mise en forme</a>)</i></td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="description" cols="40"><?php echo $description; ?></textarea></td>
    </tr>
  </table>
  <div id="pepa2">
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top">Utilisation recommand�e <i>(ex: id�al pour le dessert)</i> : </td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="utilisation" cols="40"><?php echo $utilisation; ?></textarea></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Pr�sentation : </td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="presentation" cols="40"><?php echo $presentation; ?></textarea></td>
    </tr>
  </table>
  </div>
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top">Saison : </td>
      <td width="450" align="left" valign="top"><input type="text" name="saison" value="<?php echo $saison; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">* Commercialisation : </td>
      <td width="450" align="left" valign="top"><select size="1" name="id_marche" style="width:330px">
      <option value="0">(choisir dans la liste)</option>
<?php
	for ($i=0; $i<$nbrlignes_produits_marches; $i++) {

		if ($id_marche == $produits_marches_id[$i]) {
			echo "      <option value=\"$produits_marches_id[$i]\" selected>$produits_marches[$i]</option>\n";
		}
		else {
			echo "      <option value=\"$produits_marches_id[$i]\">$produits_marches[$i]</option>\n";
		}

	}
?>
      </select> (<a href="#" onclick="javascript:parametre('produits_marches');">�diter la liste</a>)</td>
    </tr>
  </table>
  <div id="pepa3">
  <table width="700">
    <tr>
      <td width="250" align="right" valign="top">Commentaires : </td>
      <td width="450" align="left" valign="top"><textarea rows="5" name="commentaires" cols="40"><?php echo $commentaires; ?></textarea></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Admission PEPA : </td>
      <td width="450" align="left" valign="top"><input type="text" name="admission_pepa" value="<?php echo $admission_pepa; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">D�cision CIT (date) :</td>
      <td width="450" align="left" valign="top"><input type="text" name="decision_cit" value="<?php echo $decision_cit; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">D�cision comm. d�gust. (date) :</td>
      <td width="450" align="left" valign="top"><input type="text" name="decision_degust" value="<?php echo $decision_degust; ?>" size="50"></td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Produit phare : </td>
      <td width="450" align="left" valign="top"><input type="radio" value="oui" name="produit_phare" <?php if ($produit_phare == "oui") {echo "checked";} ?>>oui<input type="radio" name="produit_phare" value="non" <?php if ($produit_phare != "oui") {echo "checked";} ?>>non</td>
    </tr>
    <tr>
      <td width="250" align="right" valign="top">Logo <i>(image jpg, gif ou png, de taille minimale de 300 pixels, sur fond blanc)</i> : </td>
      <td width="450" align="left" valign="top">
<?php
	if ($logo != "") {
		echo "        <img src=\"" . $chemin_logos_pepa_http . $logo . "\" border=\"0\"><br><input type=\"checkbox\" name=\"suppr_logo\" Value=\"oui\"> Supprimer le logo actuel<br><input type=\"hidden\" name=\"old_logo\" value=\"".$logo."\"><br>Remplacer le logo :\n";
	}
?>
        <input type="file" name="logo" size="50">
      </td>
    </tr>
  </table>
  </div>
<?php
	if ($nbrlignes_produit_photo != 0) {

		echo "  <table width=\"700\">\n";
		echo "    <tr>\n";
		echo "      <td width=\"250\" align=\"right\" valign=\"top\">Photo(s) : </td>\n";
		echo "      <td width=\"450\" align=\"left\" valign=\"top\">\n";
		for ($i=0; $i<$nbrlignes_produit_photo; $i++) {
			echo "        <img src=\"" . $chemin_photos_pepa_http . "thumbs/".$produit_photo[$i]."\" border=\"0\" width=\"".$produit_photo_width[$i]."\" height=\"".$produit_photo_height[$i]."\"> position n�<input type=\"texte\" name=\"ordre[".$produit_photo_id[$i]."]\" value=\"".($i+1)."\" size=\"1\"> (<a href=\"photos_edit.php?id=".$produit_photo_id[$i]."\">editer</a>, <a href=\"#\" onclick=\"javascript:confirm('photo','".$produit_photo_id[$i]."')\">supprimer</a>)<br>\n";
		}

		echo "      </td>\n";
		echo "    </tr>\n";
		echo "  </table>\n";
	}
?>
  <p align="center"><input type="hidden" value="<?php echo $id; ?>" name="id"><input type="hidden" value="<?php echo $referer; ?>" name="referer"><input type="submit" value="Enregister" name="enregistrer"></p>
    </td>
  </tr>
</table>
</form>

</body>

</html>