<?php
global $post;

$BOURSE_DE_EMPLOI_PAGE_ID = 61;
$BOURSE_DE_EMPLOI_URL = "http://www.pays-denhaut.ch/pays-denhaut-region/promotion-economique/bourse-demploi";

function show_field($class, $label, $value) {
	if ($value) {
		echo "<div class=\"$class jobdetail\"><span class=\"detaillabel\">$label: </span>$value</div>";
	}
}

$texts['footer'] = '<p>Pour vous d&eacute;sinscrire, <a href="{unsubscription_url}">cliquez ici</a>.</p>';
$texts['header'] = "<p>Bonjour {name},</p>\n<p>Voici les derni&egrave;res annonces de la <a href=\"" . $BOURSE_DE_EMPLOI_URL . "\">Pays-d'Enhaut R&eacute;gion bourse d'emploi</a>:</p>";

query_posts('showposts=' . nt_option('posts', 10) .
            '&post_status=publish'.
            '&post_parent=' . $BOURSE_DE_EMPLOI_PAGE_ID .
            '&post_type=page'.
            '&orderby=menu_order'.
            '&order=ASC');

?>
<?php echo $texts['header']; ?>

<?php
while (have_posts())
{
    the_post();
?>
	<div class="jobname"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></div>
	<?php
	show_field("pourcent", "Pourcentage", get_meta('pourcent'));
	show_field("natio", "Nationalit&eacute;/Permis", get_meta('natio'));
	show_field("entreprise", "Entreprise", get_meta('entreprise'));
	show_field("adresse", "Adresse", get_meta('adresse'));
	show_field("formation", "Formation requise", get_meta('formation'));
	show_field("datestart", "Date d'engagement", get_meta('dateStart'));
	show_field("dateposted", "En ligne depuis", meo_get_the_date());
	show_field("info", "Informations suppl&eacute;mentaires", get_meta('info'));
	$savoir_plus = get_the_content();
	if (empty($savoir_plus)) {
		$metaplus = get_meta('plus');
		if (is_url($metaplus)) {
			$savoir_plus = "<a href=\"$metaplus\">$metaplus</a>";
		}
		elseif (is_anEmail($metaplus)) {
			$savoir_plus = "<a href=\"mailto:$metaplus\">$metaplus</a>";
		}
		elseif ("pdf" == $metaplus) {
		    $attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
		    foreach($attachments as $attachment => $attachment_array) {
		        $file = get_post($attachment);
		        $savoir_plus = "<a href=\"" . $file->guid . "\"><img src=\"" . get_bloginfo('template_url', 'display') . "/images/icon_pdf.png\" style=\"border:0px;\" /></a>";
		    }
		}
		else {
			$savoir_plus = $metaplus;
		}
	}
	show_field("more", "En savoir plus", $savoir_plus);
	?>
	<div style="height: 30px; clear: both"></div>
<?php
}
?>

<?php echo $texts['footer']; ?>

<?php wp_reset_query(); ?>