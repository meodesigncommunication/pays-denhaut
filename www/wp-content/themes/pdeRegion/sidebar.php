<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Sidebar
*/ ?>

	<div id="sidebar">
		<?php
		$depth = get_depth();
		if($depth == 0){
			if ($post->ID != 324){?><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><?php } ?>
		<ul class="navigation">
			<?php
				wp_list_pages("title_li=&child_of=".$post->ID."&depth=1&sort_column=menu_order&exclude=324,499,592,2076");
				$id= $post->ID;
			?>
		</ul>
		<?php }else{ ?>
			<?php
			$parent_title = get_the_title($post->post_parent);
			$parent_permalink = get_permalink($post->post_parent);
			echo "<h3><a href='".$parent_permalink."'>".$parent_title."</a></h3>";
			?>
			<ul class="navigation">
				<?php
					wp_list_pages("title_li=&child_of=".$post->post_parent."&depth=2&sort_column=menu_order&exclude=324,499,592,2076");
					$id= $post->ID;
				?>
			</ul>

			<?php } ?>
			<ul class="menuLinks">
			<?php
				wp_list_pages("exclude=".$post->ID.",".$post->post_parent.",2,5,11,127,266,324,499,592,2076&title_li=&depth=1&sort_column=menu_order");
			?>
			</ul>
	</div>
