<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Inexactitude
*/

get_header();
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Pays-d'Enhaut R&eacute;gion</span></div>

<div id="page_wrapper">

	<?php get_sidebar(); ?>

	<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
	<div id="titlePage"><?php the_title(); ?></div><br /><br /><br />
	<?php $fields = array();

$formdata = array(
		array('Page concernée|'.$_GET['page_inexacte'],'textfield',0,1,0,0,1),
		array('Votre nom|Nom/prénom','textfield',0,1,0,1,0),
		array('Votre email','textfield',0,1,1,0,0),
		array('L\'inexactitude observée','textarea',0,0,0,0,0),
		array('Entrez le code','captcha',1,0,0,0,0)
		);

$i=0;
foreach ( $formdata as $field ) {
	$fields['label'][$i]        = $field[0];
	$fields['type'][$i]         = $field[1];
	$fields['isdisabled'][$i]   = $field[2];
	$fields['isreq'][$i]        = $field[3];
	$fields['isemail'][$i]      = $field[4];
	$fields['isclear'][$i]      = $field[5];
	$fields['isreadonly'][$i++] = $field[6];
}

insert_custom_cform($fields,'5'); ?>
	<div style="border-bottom:1px dashed black; font-size: 10pt; line-height: 18px;">
	<?php if(get_the_content() != ""){ the_content(); }
	endwhile; endif; ?></div>
	</div>

</div>



<?php get_footer(); ?>
