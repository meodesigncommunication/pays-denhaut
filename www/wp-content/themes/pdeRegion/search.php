<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>
<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt"><?php global $wp_query;
echo $wp_query->query['s'] ?></span></div>
<div id="page_wrapper">
<div id="sidebar">
	<ul class="menuLinks">
	<?php
		wp_list_pages("exclude=".$post->ID.",".$post->post_parent.",2,5,11,127,266,2076&title_li=&depth=1&sort_column=menu_order");
	?>
	</ul>
</div>
<div id="content">

	<?php if (have_posts()) : ?>


	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
	 <div class="postWithoutImg">
			<div id="titlePage">Résultats de la recherche</div>
		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class(); ?>>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php the_title(); ?></a></h3>
			</div>

		<?php endwhile; ?>
	</div>


	<?php else : ?>

		<h2 class="center">Aucune correspondance, veuillez essayer avec un autre mot clé.</h2>
	<?php endif; ?>

	</div>
</div>
<?php get_footer(); ?>
