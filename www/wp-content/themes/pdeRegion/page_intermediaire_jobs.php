<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Page interm&eacute;diaire jobs
*/

get_header();

if (in_array(6,get_post_ancestors($post)) OR $post->ID == 6) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Pays-d'Enhaut R&eacute;gion</span></div>

<?php
} elseif (in_array(8,get_post_ancestors($post)) OR $post->ID == 8) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/entreprendre.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Pays où entreprendre</span></div>

<?php
} elseif (in_array(9,get_post_ancestors($post)) OR $post->ID == 9) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/vivre.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Vivre au Pays-d'Enhaut</span></div>

<?php
} elseif (in_array(11,get_post_ancestors($post)) OR $post->ID == 11) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Contact</span></div>

<?php
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 229) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/authentique.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Produits authentiques</span></div>

<?php
}
?>
<div id="page_wrapper">

	<?php get_sidebar(); ?>

	<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
	<div id="titlePage"><?php the_title(); ?></div>
	<?php if(get_the_content() != ""){ ?><div style="font-size: 10pt; line-height: 18px; text-align: justify;"><?php the_content(); ?></div><?php } ?>

	<?php
 		global $post;
 		$myposts = get_posts("post_parent=".$id."&post_type=page&numberposts=-1&orderby=menu_order&order=ASC");
		 foreach($myposts as $post) :
		 setup_postdata($post);
	?>
			<div id="titlePageJobs"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
			<span style="line-height: 18px; font-size: 10pt">
	<?php
		if ($post->post_parent == 61) {
	?>
			<table>
				<tr>
					<td style="width:300px"><strong>Pourcentage: </strong><?php echo get_meta('pourcent'); ?></td><td style="width:300px"><strong>Nationalité/Permis: </strong><?php echo get_meta('natio'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Entreprise: </strong><?php echo get_meta('entreprise'); ?></td><td style="width:300px"><strong>Adresse: </strong><?php echo get_meta('adresse'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Formation requise: </strong><?php echo get_meta('formation'); ?></td><td style="width:300px"><strong>Date d'engagement: </strong><?php echo get_meta('dateStart'); ?></td>
				</tr>
				<tr>
					<td colspan="2"><strong>En ligne depuis: </strong><?php echo meo_get_the_date(); ?></td>
				</tr>
				<tr>
					<td colspan="2"><strong>Informations supplémentaires: </strong><?php echo get_meta('info'); ?></td>
				</tr>
				<tr>
					<td colspan="2"><strong>En savoir plus: </strong><br />
					<?php if (get_the_content() != ""){
						// Ignore the "plus" field and use the content of the post, if there is any
						the_content(); ?>
					<?php } elseif (is_url(get_meta('plus'))) {
						?><a href="<?php echo get_meta('plus') ?>"><?php echo get_meta('plus'); ?></a>
					<?php } elseif (is_anEmail(get_meta('plus'))) {
						?><a href="mailto:<?php echo get_meta('plus') ?>"><?php echo get_meta('plus'); ?></a>
					<?php } elseif (get_meta('plus') == "pdf") {
						$attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
						foreach($attachments as $attachment => $attachment_array) {
							$file = get_post($attachment);
							?><a href="<?php echo $file->guid ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px;" /></a><?php
						}
					} else {
						echo get_meta('plus');
					}?>
					</td>
				</tr>
			</table>
 		<?php
	} else {
			?><div class="postWithoutImg">
			<div style="text-align: justify"><span style="line-height: 18px; font-style: italic; font-size: 10pt;"><?php the_advanced_excerpt('length=40&use_words=1&exclude_tags=img,p,strong'); ?><a href="<?php the_permalink(); ?>" class="postMore">+ d'infos</a></span></div>
		</div><?php
	}
	?></span>

	<?php endforeach; ?>

	<?php endwhile; else: ?>
	<?php endif; ?>

	</div>

</div>

<?php get_footer(); ?>
