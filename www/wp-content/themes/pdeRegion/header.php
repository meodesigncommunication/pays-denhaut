<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen" />
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/superfish.js" type="text/javascript"></script>
	<?php wp_head(); ?>

<script type="text/javascript">

    $(document).ready(function() {

    	$(".sf-menu li ul li ul").remove();

        $('ul.sf-menu').superfish({
            delay:       800,
            speed:       'slow',
            autoArrows:  false,
            dropShadows: false
        });
    });

</script>
<?php if (!defined('IS_PRODUCTION') || IS_PRODUCTION) { ?>
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-8981759-2");
	pageTracker._trackPageview();
	} catch(err) {}</script>
	<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-16314046-1");
	pageTracker._trackPageview();
	} catch(err) {}</script>
	<?php comments_popup_script(400, 500); ?>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-17125254-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
<?php } ?>
</head>
<body <?php body_class(); ?>>
<div id="container">
	<div id="header">
		<div class="logo_pde">
			<a title="pdeRegion" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo_pde_v2.png" alt="Pays-d'Enhaut R&eacute;gion" /></a>
		</div>
		<div class="logo_vaud">
			<a title="vaud" href="http://www.vaud.ch"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo_vaud.jpg" alt="Vaud.ch" /></a>
		</div>
		<div id="searchWP">
	    		<?php include_once('searchform.php'); ?>
		</div>
		<div id="menu">
			<div id="menuRoll">
				<ul class="sf-menu">
					<?php wp_list_pages('sort_column=menu_order&title_li=&exclude=266,324,499,592,2076');?>
				</ul>
			</div>
		</div>
	</div>
