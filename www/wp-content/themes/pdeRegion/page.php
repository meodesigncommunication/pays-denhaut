<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Page contenu
*/

get_header();

if (in_array(6,get_post_ancestors($post)) OR $post->ID == 6) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Pays-d'Enhaut R&eacute;gion</span></div>

<?php
} elseif (in_array(8,get_post_ancestors($post)) OR $post->ID == 8) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/entreprendre.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Pays où entreprendre</span></div>

<?php
} elseif (in_array(9,get_post_ancestors($post)) OR $post->ID == 9) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/vivre.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Vivre au Pays-d'Enhaut</span></div>

<?php
} elseif (in_array(11,get_post_ancestors($post)) OR $post->ID == 11) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/standard.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Contact</span></div>

<?php
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 229) {
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/authentique.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Produits authentiques</span></div>

<?php
}
?>
<div id="page_wrapper">

	<?php get_sidebar(); ?>

	<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div><!-- breadcrumb -->
	<div id="titlePage"><span class="sharelinks"><?php
	if ($post->ID != 53) { /* Devenir membre */
		if( function_exists('meo_getShareLinks')) {
			echo meo_getShareLinks(get_permalink());
		} elseif( function_exists('ADDTOANY_SHARE_SAVE_KIT')) {
			ADDTOANY_SHARE_SAVE_KIT();
		}
		?><a href="<?php echo pe_get_pdf_link(get_permalink()); ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px; vertical-align: top;" /></a><?php
	}
	?></span><span class="textTitle"><?php
	if ($post->ID == 2076 && !empty($_GET)) {
		echo "bourse d'emploi";
	}
	else {
		the_title();
	}
	?></span></div><!-- titlePage -->
	<div style="border-bottom:1px dashed black; font-size: 10pt; line-height: 18px; text-align: justify;">
	<?php if ($post->post_parent == 61) {
	?><table>
				<tr>
					<td style="width:300px"><strong>Pourcentage: </strong><?php echo get_meta('pourcent'); ?></td><td style="width:300px"><strong>Nationalité/Permis: </strong><?php echo get_meta('natio'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Entreprise: </strong><?php echo get_meta('entreprise'); ?></td><td style="width:300px"><strong>Adresse: </strong><?php echo get_meta('adresse'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Formation requise: </strong><?php echo get_meta('formation'); ?></td><td style="width:300px"><strong>Date d'engagement: </strong><?php echo get_meta('dateStart'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En ligne depuis: </strong><?php echo meo_get_the_date(); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>Informations supplémentaires: </strong><?php echo get_meta('info'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En savoir plus: </strong>
					<?php if (is_url(get_meta('plus'))) {
						?><a href="<?php echo get_meta('plus') ?>"><?php echo get_meta('plus'); ?></a>
					<?php } elseif (is_anEmail(get_meta('plus'))) {
						?><a href="mailto:<?php echo get_meta('plus') ?>"><?php echo get_meta('plus'); ?></a>
					<?php } elseif (get_meta('plus') == "pdf") {
						$attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
						foreach($attachments as $attachment => $attachment_array) {
							$file = get_post($attachment);
							?><a href="<?php echo $file->guid ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px;" /></a><?php
						}
					} else {
						echo get_meta('plus');
					}?>
					</td>
				</tr>
			</table>
	<?php
	} elseif ($post->post_parent == 9999999) {
	?><table>
				<tr>
					<td style="width:300px"><strong>Date: </strong><?php echo get_meta('date'); ?></td><td style="width:300px"><strong>Heure: </strong><?php echo get_meta('heure'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>Description: </strong><?php echo get_meta('descr'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>Réservation: </strong><?php echo get_meta('reservation'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En savoir plus: </strong>
					<?php if (is_url(get_meta('plusManif'))) {
						?><a href="<?php echo get_meta('plusManif') ?>"><?php echo get_meta('plusManif'); ?></a>
					<?php } elseif (is_anEmail(get_meta('plusManif'))) {
						?><a href="mailto:<?php echo get_meta('plusManif') ?>"><?php echo get_meta('plusManif'); ?></a>
					<?php } elseif (get_meta('plusManif') == "pdf") {
						$attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
						foreach($attachments as $attachment => $attachment_array) {
							$file = get_post($attachment);
							?><a href="<?php echo $file->guid ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px;" /></a><?php
						}
					} else {
						echo get_meta('plusManif');
					}?>
					</td>
				</tr>
			</table>
	<?php
	}
	if(get_the_content() != ""){ the_content(); } ?>
	</div><!-- end of post -->
	<div><!-- Comments -->
	<?php comments_template(); ?>
	<?php if(get_meta('signal') == "on"){ ?><a href="?page_id=324&page_inexacte=<?php the_ID() ?>">Signaler une inexactitude</a>

	<?php }
	endwhile; endif; ?>

	</div>

</div>



<?php get_footer(); ?>
