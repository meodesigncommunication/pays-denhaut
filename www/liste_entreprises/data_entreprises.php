<?php
function prepareJSON($input) {
	//This will convert ASCII/ISO-8859-1 to UTF-8.
	//Be careful with the third parameter (encoding detect list), because
	//if set wrong, some input encodings will get garbled (including UTF-8!)
	$imput = mb_convert_encoding($input, 'UTF-8', 'ASCII,UTF-8,ISO-8859-1');

	//Remove UTF-8 BOM if present, json_decode() does not like it.
	if(substr($input, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $input = substr($input, 3);
	return $input;
}

$json_url = "entreprises.json";
$json = file_get_contents($json_url);
$datas = json_decode(prepareJSON($json), true);

echo '<table id="table_entreprises" class="table table-bordered table-condensed table-body-center">'
		.'<thead>'
		.'<tr>'
			.'<th>Entreprise</th>'
			.'<th>Localité</th>'
			.'<th>Activité</th>'
			.'<th>Site internet</th>'
			.'</tr>'
		.'</thead>';		

	for ( $i=0; $i < count(  $datas ); $i++ ) 
	{
		if($datas[$i]['Affiliation'] == true AND $datas[$i]['Entreprise']!='*p.physique*') 
		{
			
			$id 			= $datas[$i]['Id'];
			$nom 			= $datas[$i]['Entreprise'];
			$adresse 		= $datas[$i]['Adresse'];
			$localite 		= $datas[$i]['Localite'];
			$activite 		= $datas[$i]['ActiviteEconomique'];
			$activite2		= $datas[$i]['ActiviteEconomique2'];
			$site			= $datas[$i]['SiteInternet'];
			$affiliation	= $datas[$i]['Affiliation'];
			
			$activiteA		= explode("-", $activite2, 2);

			echo '<tr>'
		   		.'<td><img src="images/membre.jpg" class="membre" /><a href="entreprise_details.php?id='.$id.'">'.$nom.'</a></td>'
		   		.'<td style="width:200px;">'.$localite.'</td>'
		   		.'<td>'.$activiteA[1].'<span style="display:none;">'.$activite.'</span></td>'
		   		.'<td><a href="http://'.$site.'" target="_blank" title="partenaire Pays d\'Enhaut Région" onclick="openInNewTab();">'.$site.'</a></td>'
		   		.'</tr>';
			
			
      		}
       } echo '<p>La liste des entreprises du Pays-d’Enhaut est régulièrement mise à jour. Toutefois, nous vous remercions de bien vouloir nous signaler toute inexactitude en écrivant à <a href="mailto:info@pays-denhaut.ch">info@pays-denhaut.ch</a>.</p>'
       			.'<p><a href="BD_entreprises_categories.pdf" tarfet="_blank">Télécharger la liste des catégories et mots-clés pour la recherche d’une entreprise</a></p>';
       
       	for ( $i=0; $i < count(  $datas ); $i++ ) 
	{
		if($datas[$i]['Affiliation'] == false AND $datas[$i]['Entreprise']!='*p.physique*')
		{

			$id 			= $datas[$i]['Id'];
			$nom 			= $datas[$i]['Entreprise'];
			$adresse 		= $datas[$i]['Adresse'];
			$localite 		= $datas[$i]['Localite'];
			$activite 		= $datas[$i]['ActiviteEconomique'];
			$activite2		= $datas[$i]['ActiviteEconomique2'];
			$site			= $datas[$i]['SiteInternet'];
			$affiliation	= $datas[$i]['Affiliation'];
			
			$activiteA		= explode("-", $activite2, 2);
			
			echo '<tr data-href="entreprise_details.php?id='.$id.'">'
		   		.'<td><a href="entreprise_details.php?id='.$id.'">'.$nom.'</a></td>'
		   		.'<td>'.$localite.'</td>'
		   		.'<td>'.$activiteA[1].'<span style="display:none;">'.$activite.'</span></td>'
		   		.'<td>'.$site.'</td>'
		   		.'</tr>';
      		}
       } 

echo '</table><a href="http://www.pays-denhaut.ch/contact" target="_blank" title="entreprise pays d\'enhaut">Liste établie par Pays-d’Enhaut Région, Place du Village 6, CH – 1660 Château-d’Oex.</a><p>&nbsp; </p>';



?>