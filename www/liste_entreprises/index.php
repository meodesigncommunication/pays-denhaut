<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Recherche entreprise</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
        <script src="js/jquery-2.1.3.min.js"></script>
		<script src="js/jquery.quicksearch.js"></script>
        
		<script>
		$('#id_search').keypress(function(e){
    		if( e.which == 13 ){
        		alert("Vous avez appuyé sur la touche entrée.");
			}
		});

		$(document).ready( initialize );

		function initialize(){
		 $('#id_search').keypress(
		     function(event){
		         if(event.keyCode == 13){
		             event.preventDefault();
		         }
		     }
		 );
		}
 
	
 
			$(function () {
				$('input#id_search').quicksearch('table#table_entreprises tbody tr');				
			});
			
		</script>
	</head>
	<body>

		<form action="#">
			<fieldset>
				<input type="text" name="search" value="" id="id_search" placeholder="Trier par nom, activité ou localité" autofocus />
			</fieldset>
		</form>
		<div class="responsive-table-line" style="margin:0px auto;max-width:100%;">
		<?php include_once 'data_entreprises.php';
		?>
		<p></p>
		</div>
		
	</body>
</html>