<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Détail entreprise</title>
		
		<link rel="stylesheet" href="css/style.css" type="text/css" media="all" title="" />
		</head>
		<body>
		<div class="block_detail_entreprise">
		<h3>Détail entreprise </h3>
		<?php
		function prepareJSON($input) {
			//This will convert ASCII/ISO-8859-1 to UTF-8.
			//Be careful with the third parameter (encoding detect list), because
			//if set wrong, some input encodings will get garbled (including UTF-8!)
			$imput = mb_convert_encoding($input, 'UTF-8', 'ASCII,UTF-8,ISO-8859-1');
		
			//Remove UTF-8 BOM if present, json_decode() does not like it.
			if(substr($input, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $input = substr($input, 3);
			return $input;
		}
		$json_url = "entreprises.json";
		$json = file_get_contents($json_url);
		$json_array = json_decode(prepareJSON($json), true);

$id = $_GET['id'];

  for ( $i=0; $i < count(  $json_array ); $i++ ) // All elements are reviewed
          { 
           if($json_array[$i]['Id'] == $id ) // check condition
                     { 
                     	$entreprise 		= $json_array[$i]['Entreprise'];
                     	$adresse			= $json_array[$i]['Adresse'];
                     	$localite 			= $json_array[$i]['Localite'];
                     	$telephones			= $json_array[$i]['TelephoneEntreprise'];
                     	$email				= $json_array[$i]['EmailEntreprise'];
						//$emailDirection		= $json_array[$i]['EmailPersonne'];
                     	$siteWeb			= $json_array[$i]['SiteInternet'];
                     	$nomDirection 		= $json_array[$i]['NomPersonne'];
						$prenomDirection 	= $json_array[$i]['PrenomPersonne'];
						//$telephoneDirection = $json_array[$i]['TelephonePersonne'];
                     	$affiliation 		= $json_array[$i]['Affiliation'];
						$activite2 			= $json_array[$i]['ActiviteEconomique2'];
						$activite 			= $json_array[$i]['ActiviteEconomique'];
						$entrepFormatrice   = $json_array[$i]['Formatrice'];
						
                     	$telephone 			= str_replace(chr(10),'</br>',$telephones);
						$activiteA			= explode("-", $activite, 2);
						$activiteB			= explode("-", $activite2, 2);
						$siteWebLink = '<a href="http://'.$siteWeb.'" target="_blank" title="pays d\'enhaut région">'.$siteWeb.'</a>';
						$prenomsDirection 	= explode(";",$prenomDirection);
						$nomsDirection		= explode(";", $nomDirection);
						$telephonesDirection = explode(";", $telephoneDirection);
						$emailsDirection	= explode(";",$emailDirection);


                     	
                     	$labelTelephone='';
                     	if (!empty($telephones)){
                     		$labelTelephone='Téléphone(s) : ';
                     	}
                     	$labelEmail='';
                     	if(!empty($email)){
                     		$labelEmail='Email : ';
                     	}
                     	$labelSiteInternet='';
                     	if(!empty($siteWeb)){
                     		$labelSiteInternet = 'Site internet : ';
                     	}
						/*$labelTelephoneDirection='';
                     	if (!empty($telephoneDirection)){
                     		$labelTelephoneDirection='Téléphone : ';
                     	}*/
						$labelDirection='';
                     	if(!empty($nomDirection)){
                     		$labelDirection='<p><h4>Direction:</h4></p>';
                     	}
						/*$labelEmailDirection='';
                     	if(!empty($emailDirection)){
                     		$labelEmailDirection='Email : ';
                     	}*/
						$labelEntrepFormatrice ='';
						if($entrepFormatrice == TRUE){
						    $labelEntrepFormatrice ='<p>Entreprise formatrice</p>';
                        }
                     	
                     	
                echo '<h1>'.$entreprise.'</h1>'
						.'<p></p>'
						.'<p>'.$activiteB[1].'<p>'
                        .$labelEntrepFormatrice
                    	.'<p>'.$adresse.'</p>'
                    	.'<p>'.$localite.'</p>'
						.'<p>'.$labelTelephone.'</br> '.$telephone.'</p>'
						.'<p>'.$labelEmail.'<a href="mailto:'.$email.'">'.$email.'</a></p>'
						.'<p>'.$labelSiteInternet.''.$siteWebLink.'</p>'
						.'<p></p>'
						.'<p></p>'
						.$labelDirection
						.'<p>'.$prenomsDirection[0].' '.$nomsDirection[0].'</p>'
                    	.'<p>'.$prenomsDirection[1].' '.$nomsDirection[1].'</p>'
						//.'<p>'.$labelTelephoneDirection.''.$telephonesDirection[0].' '.$telephonesDirection[1].'</p>'
						//.'<p>'.$labelEmailDirection.'<br><a href="mailto:'.$emailsDirection[0].'">'.$emailsDirection[0].'</a> 
						.'<br><a href="mailto:'.$emailsDirection[1].'"> '.$emailsDirection[1].'</a></p>';
                          }
             } 
             

?>
<a href="#" onclick="history.go(-1);" style="text-decoration:none"><input type="button" value="Retour" /></a>
		</div>
	</body>
</html>
