<?php
	$titres_tables = Array(

		'producteurs'	=> 'Producteurs',
		'produits'	=> 'Produits',
		'exploitations'	=> 'Exploitations',
		'activites'	=> 'Activit�s',

	);

	$titres_champs_producteurs = Array(

		'producteurs.nom'			=> 'Producteur',
		'producteurs.pepa'			=> 'PEPA',
		'producteurs.description_courte'	=> 'Chapeau',
		'producteurs.description'		=> 'Description',
		'producteur_langue'			=> 'Langue(s)',
		'producteurs.responsable'		=> 'Personne responsable',
		'producteurs.rue'			=> 'Rue',
		'producteurs.no_postal'			=> 'N� postal',
		'producteurs.ville'			=> 'Ville, village',
		'producteurs.tel'			=> 'T�l�phone',
		'producteurs.fax'			=> 'Fax',
		'producteurs.mobile'			=> 'T�l�phone portable',
		'producteurs.email'			=> 'Email',
		'producteurs.site_web'			=> 'Site Internet',
		'producteurs.vente_correspondance'	=> 'Vente par correspondance',

	);

	$titres_champs_produits = Array(

		'produits.nom'				=> 'Produit',
		'produit_label'				=> 'Label(s)',
		'produits.id_type'			=> 'Type de produit',
		'produits.id_lait_type'			=> 'Type de produit laitier',
		'produits.id_lait_type_lait'		=> 'Type de lait',
		'produits.id_lait_traitement_lait'	=> 'Traitement du lait',
		'produits.lait_specificite'		=> 'Sp�cificit�',
		'produits.lait_mg_es'			=> 'Mati�re grasse',
		'produits.lait_affinage'		=> 'Affinage',
		'produits.id_viande_type'		=> 'Type de viande',
		'produits.id_viande_conditionnement'	=> 'Conditionnement de la viande',
		'produits.description'			=> 'Description',
		'produits.utilisation'			=> 'Utilisation',
		'produits.presentation'			=> 'Pr�sentation',
		'produits.saison'			=> 'Saison',
		'produits.id_marche'			=> 'Commercialisation',
		'produits.commentaires'			=> 'Commentaires',
		'produits.admission_pepa'		=> 'Admission PEPA',
		'produits.decision_cit'			=> 'D�cision CIT',
		'produits.decision_degust'		=> 'D�cision commission de d�gustation',
		'produits.produit_phare'		=> 'Produit phare',
	);

	$titres_champs_exploitations = Array(

		'exploitations.nom'			=> 'Exploitation',
		'exploitations.alpage'			=> 'Alpage',
		'exploitations.etivaz'			=> 'Fabrication de L\'Etivaz',
		'exploitations.altitude'		=> 'Altitude',
		'exploitations.tel'			=> 'T�l�phone',
		'exploitations.saison'			=> 'Saison',
		'exploitation_animal'			=> 'Animal(aux)',
		'exploitations.commentaires'		=> 'Commentaires',
		'exploitations.id_commune'		=> 'Commune',
		'exploitations.endicapes'		=> 'Acc�s pour endicap�s',
		'exploitation_activite'			=> 'Activit�s li�es',
	);

	$titres_champs_activites = Array(

		'activites.nom'				=> 'Activit�',
		'activite_type'				=> 'Type d\'activit�',
		'activites.description'			=> 'Description',
		'activite_exploitation'			=> 'Exploitations/lieux',
		'activite_vente_produit'		=> 'Produits en vente',
		'activite_vente_producteur_pepa'	=> 'Vente producteurs PEPA',
		'activites.vente_producteur_non_pepa'	=> 'Vente producteurs non PEPA',
	);



	$width_champs_producteurs = Array(

		'producteurs.nom'			=> 25,
		'producteurs.pepa'			=> 5,
		'producteurs.description_courte'	=> 30,
		'producteurs.description'		=> 30,
		'producteur_langue'			=> 10,
		'producteurs.responsable'		=> 20,
		'producteurs.rue'			=> 15,
		'producteurs.no_postal'			=> 10,
		'producteurs.ville'			=> 15,
		'producteurs.tel'			=> 15,
		'producteurs.fax'			=> 15,
		'producteurs.mobile'			=> 15,
		'producteurs.email'			=> 20,
		'producteurs.site_web'			=> 25,
		'producteurs.vente_correspondance'	=> 21,

	);

	$width_champs_produits = Array(

		'produits.nom'				=> 25,
		'produit_label'				=> 25,
		'produits.id_type'			=> 15,
		'produits.id_lait_type'			=> 15,
		'produits.id_lait_type_lait'		=> 15,
		'produits.id_lait_traitement_lait'	=> 15,
		'produits.lait_specificite'		=> 15,
		'produits.lait_mg_es'			=> 5,
		'produits.lait_affinage'		=> 15,
		'produits.id_viande_type'		=> 15,
		'produits.id_viande_conditionnement'	=> 15,
		'produits.description'			=> 30,
		'produits.utilisation'			=> 30,
		'produits.presentation'			=> 30,
		'produits.saison'			=> 30,
		'produits.id_marche'			=> 15,
		'produits.commentaires'			=> 30,
		'produits.admission_pepa'		=> 15,
		'produits.decision_cit'			=> 15,
		'produits.decision_degust'		=> 15,
		'produits.produit_phare'		=> 15,
	);

	$width_champs_exploitations = Array(

		'exploitations.nom'			=> 25,
		'exploitations.alpage'			=> 10,
		'exploitations.etivaz'			=> 10,
		'exploitations.altitude'		=> 10,
		'exploitations.tel'			=> 15,
		'exploitations.saison'			=> 30,
		'exploitation_animal'			=> 10,
		'exploitations.commentaires'		=> 30,
		'exploitations.id_commune'		=> 15,
		'exploitations.endicapes'		=> 10,
		'exploitation_activite'			=> 35,
	);

	$width_champs_activites = Array(

		'activites.nom'				=> 25,
		'activite_type'				=> 15,
		'activites.description'			=> 30,
		'activite_exploitation'			=> 30,
		'activite_vente_produit'		=> 20,
		'activite_vente_producteur_pepa'	=> 45,
		'activites.vente_producteur_non_pepa'	=> 20,
	);


	$titres_champs = array_merge ($titres_champs_producteurs,$titres_champs_produits,$titres_champs_exploitations,$titres_champs_activites, Array('id' => 'N�'));
	$width_champs = array_merge ($width_champs_producteurs,$width_champs_produits,$width_champs_exploitations,$width_champs_activites, Array('id' => 5));

?>