<?php
	require "config_consultation.php";
	include "champs_attributs.php";

	include('bbcode.php');


	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	$sqlquery= "SELECT * FROM produits_types ORDER BY id";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {
			$id = mysql_result($resultat_sql,$i,"id");
			if ($id == "1") {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
				$sqlquery2= "SELECT * FROM produits_lait_types ORDER BY id";
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);
					$nbrlignes2 = mysql_num_rows($resultat_sql2);

					for ($j=0; $j<$nbrlignes2; $j++) {
						$id2 = mysql_result($resultat_sql2,$j,"id");
						$titres[$id][$id2] = mysql_result($resultat_sql2,$j,"type");
					}
			}
			elseif ($id == "2") {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
				$sqlquery2= "SELECT * FROM produits_viande_types ORDER BY id";
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);
					$nbrlignes2 = mysql_num_rows($resultat_sql2);

					for ($j=0; $j<$nbrlignes2; $j++) {
						$id2 = mysql_result($resultat_sql2,$j,"id");
						$titres[$id][$id2] = mysql_result($resultat_sql2,$j,"type");
					}
			}
			else {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
			}
		}


	if ($_GET{'view'} == "producteurs") {
		$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE pepa='oui' ORDER BY nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$name = mysql_result($resultat_sql,$i,"nom");
				$prenom = mysql_result($resultat_sql,$i,"prenom");
				$id = mysql_result($resultat_sql,$i,"id");
				$nom[0][0][$id] = $name . " " . $prenom;
			}

		$titre = "Les producteurs du label \"Pays-d'Enhaut Produits Authentiques\"";
	}
	elseif ($_GET{'view'} == "produits") {
		$sqlquery= "SELECT DISTINCT produits.id, produits.nom, id_type, id_lait_type, id_viande_type, id_producteur FROM produits, produit_label, producteurs WHERE produit_label.id_produit = produits.id AND produit_label.id_label = '1' AND produits.id_producteur = producteurs.id AND producteurs.pepa='oui' ORDER BY id_type, id_lait_type, id_viande_type, nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");
				$type = mysql_result($resultat_sql,$i,"id_type");
				if ($type == "1") {
					$soustype = mysql_result($resultat_sql,$i,"id_lait_type");
				}
				elseif ($type == "2") {
					$soustype = mysql_result($resultat_sql,$i,"id_viande_type");
				}
				else {
					$soustype = 0;
				}
				$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"nom");
			}

		$titre = "Les produits labellis�s \"Pays-d'Enhaut Produits Authentiques\"";
	}
	elseif ($_GET{'view'} == "bio") {

		$produits_pepa = Array();
		$producteurs_bio = Array();

		$sqlquery= "SELECT id FROM produits LEFT JOIN produit_label ON produit_label.id_produit = produits.id WHERE produit_label.id_label = '1'";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {

				array_push ($produits_pepa, mysql_result($resultat_sql,$i,"id"));
			}

		$sqlquery= "SELECT DISTINCT produits.id, produits.nom, id_type, id_lait_type, id_viande_type, id_producteur, producteurs.nom, producteurs.prenom FROM produits, produit_label, producteurs WHERE produit_label.id_produit = produits.id AND produit_label.id_label = '2' AND produits.id_producteur = producteurs.id AND producteurs.pepa='oui' ORDER BY id_type, id_lait_type, id_viande_type, producteurs.nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");

				if (in_array($id,$produits_pepa) == TRUE) {
					$type = mysql_result($resultat_sql,$i,"id_type");
					if ($type == "1") {
						$soustype = mysql_result($resultat_sql,$i,"id_lait_type");
					}
					elseif ($type == "2") {
						$soustype = mysql_result($resultat_sql,$i,"id_viande_type");
					}
					else {
						$soustype = 0;
					}
					$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"nom");
					$id_producteur_bio = mysql_result($resultat_sql,$i,"id_producteur");

					if (array_key_exists($id_producteur_bio,$producteurs_bio) == FALSE) {
						$producteurs_bio[$id_producteur_bio] = mysql_result($resultat_sql,$i,"producteurs.nom") . " " . mysql_result($resultat_sql,$i,"producteurs.prenom");
					}
				}
			}

		$titre = "Les produits et producteurs biologiques";
	}
	elseif ($_GET{'view'} == "ambassadeurs") {
		$sqlquery= "SELECT id, nom, prenom, fonction FROM producteurs WHERE ambassadeur='oui' ORDER BY fonction, nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$name = mysql_result($resultat_sql,$i,"nom");
				$prenom = mysql_result($resultat_sql,$i,"prenom");
				$id = mysql_result($resultat_sql,$i,"id");
				$fonction = mysql_result($resultat_sql,$i,"fonction");
				$nom[0][$fonction][$id] = $name . " " . $prenom;
			}

		$titre = "Les ambassadeurs des produits labellis�s";
	}
	elseif ($_GET{'view'} == "vente") {
		$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE ambassadeur='oui' ORDER BY nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$name = mysql_result($resultat_sql,$i,"nom");
				$prenom = mysql_result($resultat_sql,$i,"prenom");
				$id = mysql_result($resultat_sql,$i,"id");
				$nom["Les ambassadeurs \"Pays-d'Enhaut Produits Authentiques\""][$id] = $name . " " . $prenom;
			}

		$sqlquery= "SELECT id, nom, prenom FROM producteurs WHERE fonction='commerce' ORDER BY nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$name = mysql_result($resultat_sql,$i,"nom");
				$prenom = mysql_result($resultat_sql,$i,"prenom");
				$id = mysql_result($resultat_sql,$i,"id");
				$nom["Les commerces locaux"][$id] = $name . " " . $prenom;
			}

		$titre = "Les points de vente locaux";
	}
	else {
		$titre = "Pays-d'Enhaut, Produits Authentiques";
	}

	mysql_close($connexion);
?>
<HTML>

<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<TITLE>Pays-dEnhaut.ch - Producteurs</TITLE>

</HEAD>

<BODY BGCOLOR="#339966" TOPMARGIN="0" LEFTMARGIN="0" TEXT="#ffffff" LINK="#ffffff" VLINK="#ffffff" ALINK="#ffffff">

<TABLE BORDER="0" CELLPADDING="7" WIDTH="775" CELLSPACING="0">
  <TR BGCOLOR="#006666">
    <TD>
      <H3><B><U><FONT FACE="Times New Roman, Times, serif" COLOR="#FFFFFF"><?php echo $titre; ?></FONT></U></B></H3>
    </TD>
  </TR>
  <TR>
    <TD>

<?php


	if ($_GET{'view'} == "producteurs") {
		echo "  <ul>\n";
		foreach ($nom[0][0] as $id => $nom) {
			echo "- <a href=\"producteur.php?id=$id\">$nom</a><br>\n";
		}
		echo "  </ul>\n";

		echo "<br><a href=\"../tourismerural/frame_tr.html\" target=\"content\">Ensemble des adresses de tourisme rural et vente de produits r�gionaux au Pays-d'Enhaut.</a>\n";
	}
	elseif ($_GET{'view'} == "produits") {
		echo "  <ul>\n";
		foreach ($nom as $type => $suite1) {
			echo "    <b>".$titres[$type][0]." :</b><br><ul>\n";
			foreach ($suite1 as $soustype => $suite2) {
				if ($soustype != 0) {
					echo "      - <b>".$titres[$type][$soustype]." :</b><br><ul>\n";
				}
				foreach ($suite2 as $id => $nom) {
					echo "        - <a href=\"produit.php?id=$id\">$nom</a><br>\n";
				}
				if ($soustype != 0) {
					echo "      </ul>\n";
				}
			}
			echo "    </ul>\n";
		}
		echo "  </ul>\n";

		echo "<br><a href=\"../tourismerural/frame_tr.html\" target=\"content\">Ensemble des produits locaux et activit�s de tourisme r�gional.</a>\n";
	}
	elseif ($_GET{'view'} == "bio") {

		echo "      <P><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#ffffff\"><IMG SRC=\"images/bourgeon.gif\" ALIGN=\"right\" BORDER=\"0\" WIDTH=\"54\" HEIGHT=\"82\"></FONT><B><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#ffffff\">Label\n";
		echo "      Bourgeon :</FONT></B></P>\n";
		echo "      <P><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#ffffff\">Le label\n";
		echo "      bourgeon garantit une production et une transformation du produit conforme\n";
		echo "      aux cahiers des charges de Bio-Suisse, Association Suisse des\n";
		echo "      Organisations d'Agriculture Biologique. Pour plus de renseigements : <A HREF=\"http://www.bio-suisse.ch/f_index.html\" TARGET=\"blank_\">www.bio-suisse.ch</A></FONT>\n";

		echo "      <HR NOSHADE SIZE=\"1\" COLOR=\"#FFFFFF\">\n";

		echo "  <B>Les producteurs bio du label \"Pays-d'Enhaut Produits Authentiques\" :</B>\n";
		echo "  <ul>\n";
		foreach ($producteurs_bio as $id => $name) {
			echo "        - <a href=\"producteur.php?id=$id\">$name</a><br>\n";
		}
		echo "  </ul>\n";
		echo "  <HR NOSHADE SIZE=\"1\" COLOR=\"#FFFFFF\">\n";
		echo "  <B>Les produits labellis�s \"Pays-d'Enhaut Produits Authentiques\" :</B>\n";
		echo "  <ul>\n";
		foreach ($nom as $type => $suite1) {
			echo "    <b>".$titres[$type][0]." :</b><br><ul>\n";
			foreach ($suite1 as $soustype => $suite2) {
				if ($soustype != 0) {
					echo "      - <b>".$titres[$type][$soustype]." :</b><br><ul>\n";
				}
				foreach ($suite2 as $id => $nom) {
					echo "        - <a href=\"produit.php?id=$id\">$nom</a><br>\n";
				}
				if ($soustype != 0) {
					echo "      </ul>\n";
				}
			}
			echo "    </ul>\n";
		}
		echo "  </ul>\n";

		echo "<br><a href=\"../tourismerural/frame_tr.html\" target=\"content\">Ensemble des produits locaux et activit�s de tourisme r�gional.</a>\n";
	}
	elseif ($_GET{'view'} == "ambassadeurs") {
		echo "  <ul>\n";

		foreach ($nom[0] as $type => $suite) {
			if ($type == "commerce") {
				echo "    <br><b>Les commerces</b><br><ul>\n";
			}
			if ($type == "restaurant") {
				echo "    <br><b>Les restaurants</b><br><ul>\n";
			}
			foreach ($suite as $id => $nom) {
				echo "    - <a href=\"producteur.php?id=$id\">$nom</a><br>\n";
			}
				echo "    </ul>\n";
		}
		echo "  </ul>\n";
	}
	elseif ($_GET{'view'} == "vente") {
		echo "      <P ALIGN=\"justify\"><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#FFFFFF\"><BR>\n";
		echo "      Les produits authentiques du Pays-d'Enhaut sont disponibles dans diff�rents\n";
		echo "      commerces de la r�gion. Nous vous recommandons plus particuli�rement les commerces et restaurants <a href=\"liste.php?view=ambassadeurs\">ambassadeurs</a> de notre label.</FONT></P>\n";
		echo "      <P ALIGN=\"justify\"><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#FFFFFF\">En\n";
		echo "      dehors du Pays-d'Enhaut, vous trouverez le fromage d'alpage L'Etivaz\n";
		echo "      AOC dans la plupart des localit�s importantes de Suisse. Les produits\n";
		echo "      de Michel Beroud, fromagerie de Rougemont, et de Jean-Robert Henchoz,\n";
		echo "      produits au lait de brebis, sont distribu�s dans de nombreux points de\n";
		echo "      vente de Suisse romande. Les produits biologiques sont quant � eux\n";
		echo "      distribu�s dans le r�seau de commerce sp�cialis�s de toute la Suisse.</FONT></P>\n";
		echo "      <P ALIGN=\"justify\"><FONT FACE=\"Times New Roman, Times, Sherif\" COLOR=\"#FFFFFF\">L'aire de commercialisation est mentionn�e sur la fiche de chaque produit. N'h�sitez\n";
		echo "      pas � contacter les producteurs pour\n";
		echo "      conna�tre plus pr�cis�ment les possibilit�s d'acc�der aux saveurs du\n";
		echo "      Pays-d'Enhaut les plus proches de votre domicile, ou pour vous renseigner\n";
		echo "      sur les possibilit�s de distribution et de vente directe.</FONT></P>\n";
		echo "      <HR SIZE=\"1\" NOSHADE COLOR=\"#FFFFFF\">\n";
		echo "  <ul>\n";
		foreach ($nom as $type => $suite1) {
			echo "    <b>".$type." :</b><br><ul>\n";
			foreach ($suite1 as $id => $nom) {
				echo "        - <a href=\"producteur.php?id=$id\">$nom</a><br>\n";
			}
			echo "    </ul><br>\n";
		}
		echo "  </ul>\n";

		echo "<br><a href=\"/html/indexFrame-5.htm#produits\" target=\"content\">Les produits labellis�s \"Pays-d'Enhaut Produits Authentiques\" se pr�sentent (manifestations de d�gustation - vente � venir).</a>\n";
		echo "<br><br><a href=\"../tourismerural/frame_tr.html\" target=\"content\">Adresses de tourisme rural et vente de produits r�gionaux.</a>\n";
	}
	else {
		echo "  <ul>\n";
		echo "        <br><b><a href=\"liste.php?view=producteurs\">Les producteurs des produits authentiques</a></b><br>\n";
		echo "        <br><b><a href=\"liste.php?view=produits\">Les produits labellis�s \"Pays-d'Enhaut Produits Authentiques\"</a></b><br>\n";
		echo "        <br><b><a href=\"liste.php?view=ambassadeurs\">Les ambassadeurs des produits lab�lis�s</a></b><br>\n";
		echo "        <br><b><a href=\"liste.php?view=vente\">Les points de vente locaux</a></b><br>\n";
		echo "  </ul>\n";

		echo "<br><a href=\"../tourismerural/frame_tr.html\" target=\"content\">Adresses de tourisme rural et vente de produits r�gionaux.</a>\n";
	}
?>

    </TD>
  </TR>
</TABLE>

</BODY>

</HTML>