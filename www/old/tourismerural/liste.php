<?php
	require "config_consultation.php";
	include "champs_attributs.php";

	include('bbcode.php');


	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	$sqlquery= "SELECT * FROM produits_types ORDER BY id";
		$resultat_sql = mysql_query($sqlquery,$connexion);

		$nbrlignes = mysql_num_rows($resultat_sql);

		for ($i=0; $i<$nbrlignes; $i++) {
			$id = mysql_result($resultat_sql,$i,"id");
			if ($id == "1") {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
				$sqlquery2= "SELECT * FROM produits_lait_types ORDER BY id";
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);
					$nbrlignes2 = mysql_num_rows($resultat_sql2);

					for ($j=0; $j<$nbrlignes2; $j++) {
						$id2 = mysql_result($resultat_sql2,$j,"id");
						$titres[$id][$id2] = mysql_result($resultat_sql2,$j,"type");
					}
			}
			elseif ($id == "2") {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
				$sqlquery2= "SELECT * FROM produits_viande_types ORDER BY id";
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);
					$nbrlignes2 = mysql_num_rows($resultat_sql2);

					for ($j=0; $j<$nbrlignes2; $j++) {
						$id2 = mysql_result($resultat_sql2,$j,"id");
						$titres[$id][$id2] = mysql_result($resultat_sql2,$j,"type");
					}
			}
			else {
				$titres[$id][0] = mysql_result($resultat_sql,$i,"type");
			}
		}


	if ($_GET{'view'} == "producteurs") {
		$sqlquery= "SELECT id, nom, prenom, fonction, ville FROM producteurs ORDER BY fonction DESC, ville ASC, nom ASC, prenom ASC";
			$resultat_sql = mysql_query($sqlquery,$connexion);
			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$name = mysql_result($resultat_sql,$i,"nom");
				$prenom = mysql_result($resultat_sql,$i,"prenom");
				$id = mysql_result($resultat_sql,$i,"id");
				$type = mysql_result($resultat_sql,$i,"fonction");
				$soustype = mysql_result($resultat_sql,$i,"ville")." :";
				$nom[$type][$soustype][$id] = $name . " " . $prenom;
			}

		$titre = "Entreprises qui proposent des produits r�gionaux et des activit�s de tourisme rural";
	}
	elseif ($_GET{'view'} == "produits") {
		$sqlquery= "SELECT DISTINCT id, nom, id_type, id_lait_type, id_viande_type, id_producteur FROM produits LEFT JOIN produit_label ON produit_label.id_produit = produits.id ORDER BY id_type, id_lait_type, id_viande_type, nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");
				$type = mysql_result($resultat_sql,$i,"id_type");
				if ($type == "1") {
					$soustype = mysql_result($resultat_sql,$i,"id_lait_type");
				}
				elseif ($type == "2") {
					$soustype = mysql_result($resultat_sql,$i,"id_viande_type");
				}
				else {
					$soustype = 0;
				}
				$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"nom");
				$id_producteur = mysql_result($resultat_sql,$i,"id_producteur");

				$sqlquery2 = "SELECT nom, prenom FROM producteurs WHERE id = $id_producteur";
					$resultat_sql2 = mysql_query($sqlquery2,$connexion);

					$texte[$id] = " (" . mysql_result($resultat_sql2,0,"nom") . " " .  mysql_result($resultat_sql2,0,"prenom") . ")";
			}

		$titre = "Les produits r�gionaux (avec ou sans label)";
	}
	elseif ($_GET{'view'} == "exploitations") {
		$sqlquery= "SELECT DISTINCT exploitations.id, nom, altitude, commune FROM exploitations, exploitations_communes WHERE exploitations.id_commune = exploitations_communes.id AND alpage = 'oui' ORDER BY commune, nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			$type = 1;
			$nom[$type][0][0] = "Les alpages";

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");
				$soustype = "Commune de " . mysql_result($resultat_sql,$i,"commune");

				$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"nom") . ", " . mysql_result($resultat_sql,$i,"altitude")."m";
			}

		$sqlquery= "SELECT DISTINCT exploitations.id, nom, commune FROM exploitations, exploitations_communes WHERE exploitations.id_commune = exploitations_communes.id AND alpage <> 'oui' ORDER BY commune, nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			$type = 2;
			$nom[$type][0][0] = "Les exploitations de plaine";

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");
				$soustype = "Commune de " . mysql_result($resultat_sql,$i,"commune");

				$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"nom");
			}

		$titre = "Les alpages et les exploitations qui proposent une activit� de tourisme rural";
	}
	elseif ($_GET{'view'} == "activites") {
		$sqlquery= "SELECT activites.id, activites.nom, activites_types.id, activites_types.type, producteurs.nom, producteurs.prenom, producteurs.ville FROM activites, activites_types, activite_type, producteurs WHERE activites.id = activite_type.id_activite AND activite_type.id_type = activites_types.id AND activites.id_producteur = producteurs.id ORDER BY activites_types.id, producteurs.ville, activites.nom";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"activites.id");
				$type = mysql_result($resultat_sql,$i,"activites_types.id");

				$soustype = mysql_result($resultat_sql,$i,"producteurs.ville");

				$nom[$type][$soustype][$id] = mysql_result($resultat_sql,$i,"activites.nom");
				$titres[$type][0] = mysql_result($resultat_sql,$i,"activites_types.type");
				$texte[$id] = " (" . mysql_result($resultat_sql,$i,"producteurs.prenom") . " " . mysql_result($resultat_sql,$i,"producteurs.nom") . ")";
			}

		$titre = "Les activit�s de tourisme rural class�es par lieu de domicile de l'entreprise";
	}
	else {
		$sqlquery= "SELECT id, type FROM activites_types ORDER BY id";
			$resultat_sql = mysql_query($sqlquery,$connexion);

			$nbrlignes = mysql_num_rows($resultat_sql);

			for ($i=0; $i<$nbrlignes; $i++) {
				$id = mysql_result($resultat_sql,$i,"id");
				$texte[$id] = mysql_result($resultat_sql,$i,"type");
			}

		$titre = "Produits r�gionaux et tourisme rural au Pays-d'Enhaut";
	}

	mysql_close($connexion);
?>
<HTML>

<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<TITLE>Pays-dEnhaut.ch - <?php echo $titre; ?></TITLE>

</HEAD>

<BODY BGCOLOR="<?php echo $couleur_fond; ?>" TOPMARGIN="0" LEFTMARGIN="0" TEXT="<?php echo $couleur_texte; ?>" LINK="<?php echo $couleur_link; ?>" VLINK="<?php echo $couleur_vlink; ?>" ALINK="<?php echo $couleur_alink; ?>">

<TABLE BORDER="0" CELLPADDING="7" WIDTH="<?php echo $width_page; ?>" CELLSPACING="0">
  <TR BGCOLOR="<?php echo $couleur_tableau1; ?>">
    <TD>
      <H3><B><U><FONT FACE="<?php echo $police; ?>" COLOR="<?php echo $couleur_texte; ?>"><?php echo $titre; ?></FONT></U></B></H3>
    </TD>
  </TR>
  <TR>
    <TD><FONT FACE="<?php echo $police; ?>" COLOR="<?php echo $couleur_texte; ?>">

<?php


	if ($_GET{'view'} == "producteurs") {
		echo "    - <b><a href=\"#producteurs\">producteurs et agriculteurs</a></b> : entreprises qui proposent des produits r�gionaux ou des activit�s de tourisme rural (accueil, h�bergement, ...)<br>\n";
		echo "    - <b><a href=\"#commerces\">commerces locaux</a></b> proposant des produits r�gionaux<br>\n";
		echo "    - <b><a href=\"#restaurants\">restaurants</a></b> : restaurants ambassadeurs des \"Produits Authentiques du Pays-d'Enhaut\"<br>\n";
		echo "  <br><hr size=\"1\" color=\"#ffffff\">\n";
		echo "  <ul>\n";
		if (count($nom["producteur"]) > 0) {
			echo "    <br><a name=\"producteurs\"><b>producteurs et agriculteurs :</b></a><br><ul>\n";

				foreach ($nom["producteur"] as $soustype => $suite2) {
					echo "      <br><b>$soustype</b><br><ul>\n";
					foreach ($suite2 as $id => $name) {
						echo "        - <a href=\"producteur.php?id=$id\">$name<a><br>\n";
					}
					echo "      </ul>\n";
				}

			echo "    </ul>\n";
		}
		if (count($nom["commerce"]) > 0) {
			echo "    <br><a name=\"commerces\"><b>commerces locaux :</b><br><ul>\n";

				foreach ($nom["commerce"] as $soustype => $suite2) {
					echo "      <br><b>$soustype</b><br><ul>\n";
					foreach ($suite2 as $id => $name) {
						echo "        - <a href=\"producteur.php?id=$id\">$name<a><br>\n";
					}
					echo "      </ul>\n";
				}

			echo "    </ul>\n";
		}
		if (count($nom["restaurant"]) > 0) {
			echo "    <br><a name=\"restaurants\"><b>restaurants :</b><br><ul>\n";

				foreach ($nom["restaurant"] as $soustype => $suite2) {
					echo "      <br><b>$soustype</b><br><ul>\n";
					foreach ($suite2 as $id => $name) {
						echo "        - <a href=\"producteur.php?id=$id\">$name<a><br>\n";
					}
					echo "      </ul>\n";
				}

			echo "    </ul>\n";
		}
		echo "  </ul>\n";
	}
	elseif ($_GET{'view'} == "produits") {
		echo "  <ul>\n";
		foreach ($nom as $type => $suite1) {
			echo "    <br><b>".$titres[$type][0]." :</b><br><ul>\n";
			foreach ($suite1 as $soustype => $suite2) {
				if ($soustype != 0) {
					echo "      <br><b>".$titres[$type][$soustype]." :</b><br><ul>\n";
				}
				foreach ($suite2 as $id => $nom) {
					echo "        - <a href=\"produit.php?id=$id\">$nom<a>$texte[$id]<br>\n";
				}
				if ($soustype != 0) {
					echo "      </ul>\n";
				}
			}
			echo "    </ul>\n";
		}
		echo "  </ul>\n";
	}
	elseif ($_GET{'view'} == "exploitations") {
		echo "  Liste des exploitations de vall�e et alpestre du Pays-d'Enhaut et les alpages de L'Etivaz AOC o� se d�roulent les activit�s de tourisme rural.<br>\n";
		echo "  <br><hr size=\"1\" color=\"#ffffff\">\n";

		echo "  <ul>\n";
		echo "    <b>".$nom[1][0][0]." :</b><br><ul>\n";
		foreach ($nom[1] as $soustype => $suite2) {
			if ($soustype != "0") {
				echo "    <br><b>".$soustype." :</b><br><ul>\n";
			}
			foreach ($suite2 as $id => $name) {
				if ($id != 0) {
					echo "        - <a href=\"exploitation.php?id=$id\">$name<a>$texte[$id]<br>\n";
				}
			}
			if ($soustype != "0") {
				echo "    </ul>\n";
			}
		}
		echo "    </ul><br><b>".$nom[2][0][0]." :</b><br><ul>\n";
		foreach ($nom[2] as $soustype => $suite2) {
			if ($soustype != "0") {
				echo "    <br><b>".$soustype." :</b><br><ul>\n";
			}
			foreach ($suite2 as $id => $name) {
				if ($id != 0) {
					echo "        - <a href=\"exploitation.php?id=$id\">$name<a>$texte[$id]<br>\n";
				}
			}
			if ($soustype != "0") {
				echo "    </ul>\n";
			}
		}
		echo "  </ul></ul>\n";
	}
	elseif ($_GET{'view'} == "activites") {
		if ($_GET{'select'} == "tout") {
			echo "  <ul>\n";
			foreach ($nom as $type => $suite1) {
				echo "    <br><b>".$titres[$type][0]." :</b><br><ul>\n";
				foreach ($suite1 as $soustype => $suite2) {
					echo "    <br><b>".$soustype." :</b><br><ul>\n";
					foreach ($suite2 as $id => $nom) {
						echo "        - <a href=\"activite.php?id=$id\">$nom<a>$texte[$id]<br>\n";
					}
					echo "    </ul>\n";
				}
				echo "    </ul>\n";
			}
			echo "  </ul>\n";
		}
		elseif (is_numeric($_GET{'select'}) == true) {
			echo "  <ul>\n";
			foreach ($nom as $type => $suite1) {
				if ($type == $_GET{'select'}) {
					echo "    <br><b>".$titres[$type][0]." :</b><br><ul>\n";
					foreach ($suite1 as $soustype => $suite2) {
						echo "    <br><b>".$soustype." :</b><br><ul>\n";
						foreach ($suite2 as $id => $nom) {
							echo "        - <a href=\"activite.php?id=$id\">$nom<a>$texte[$id]<br>\n";
						}
						echo "    </ul>\n";
					}
					echo "    </ul>\n";
				}
			}
			echo "  </ul>\n";
		}
		else {
			echo "  <ul>\n";
			foreach ($nom as $type => $suite1) {
					echo "    <br><b><a href=\"liste.php?view=activites&select=".$type."\">".$titres[$type][0]."</a></b><br>\n";
			}
			echo "  </ul>\n";
			echo "    <br><a href=\"http://www.gruyere-paysdenhaut.ch/calendrier\" target=\"_blank\">L'agenda des activit�s de d�couverte du Parc Naturel R�gional Gruy�re - Pays-d'Enhaut</a><br>\n";

		}
	}
	else {
		echo "  <ul>\n";
		echo "    <br><b><a href=\"liste.php?view=producteurs\">Entreprises qui proposent des produits r�gionaux et des activit�s de tourisme rural</a></b><br>\n";
		echo "    <br><b><a href=\"liste.php?view=produits\">Les produits r�gionaux (avec ou sans label)</a></b><br>\n";
		echo "    <br><b><a href=\"liste.php?view=activites\">Les activit�s de tourisme rural par cat�gorie</a> :</b><br>\n";
		echo "    <ul>\n";
		foreach ($texte as $id => $texte) {
			echo "      - <a href=\"liste.php?view=activites&select=".$id."\">".$texte."</b></a><br>\n";
		}
		echo "      <br><a href=\"liste.php?view=activites&select=tout\">toutes les activit�s</b></a><br>\n";
		echo "    </ul>\n";
		echo "    <br><b><a href=\"liste.php?view=exploitations\">Les alpages et les exploitations qui proposent une activit� de tourisme rural</a></b><br>\n";
		echo "  </ul>\n";
	}
?>

    </FONT></TD>
  </TR>
</TABLE>

</BODY>

</HTML>