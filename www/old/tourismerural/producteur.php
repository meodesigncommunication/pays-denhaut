<?php
	require "config_consultation.php";
	include "champs_attributs.php";

	include('bbcode.php');
	
	$connexion = mysql_connect ("$dbhost","$user","$password");

	if (!$connexion) {
		echo "Impossible d'effectuer la connexion";
		exit;
	}

	$db = mysql_select_db("$usebdd", $connexion);

	if (!$db) {
		echo "Impossible de s�lectionner cette base donn�es";
		exit;
	}

	$sqlquery= "SELECT DISTINCT * FROM producteurs WHERE producteurs.id =" . $_GET{'id'};
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur = mysql_num_rows($resultat_sql);

		if ($nbrlignes_producteur == 0) {
			echo "Producteur/commerce inexistant ou non visible.";
			exit();
		}
	
		$nom = mysql_result($resultat_sql,0,"nom");
		$prenom = mysql_result($resultat_sql,0,"prenom");
		$fonction = mysql_result($resultat_sql,0,"fonction");
		$description = mysql_result($resultat_sql,0,"description");
		$description = bbcode($description);
		$description_courte = mysql_result($resultat_sql,0,"description_courte");
		$description_courte = bbcode($description_courte);
		$responsable = mysql_result($resultat_sql,0,"responsable");
		$rue = mysql_result($resultat_sql,0,"rue");
		$no_postal = mysql_result($resultat_sql,0,"no_postal");
		$ville = mysql_result($resultat_sql,0,"ville");
		$tel = mysql_result($resultat_sql,0,"tel");
		$fax = mysql_result($resultat_sql,0,"fax");
		$mobile = mysql_result($resultat_sql,0,"mobile");
		$email = mysql_result($resultat_sql,0,"email");
		$site_web = mysql_result($resultat_sql,0,"site_web");
		$vente_correspondance = mysql_result($resultat_sql,0,"vente_correspondance");
		$logo = mysql_result($resultat_sql,0,"logo");
		$id = $_GET{'id'};

	$sqlquery= "SELECT * FROM producteur_photo WHERE id_producteur=" . $_GET{'id'}." ORDER BY ordre, id_photo";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur_photo = mysql_num_rows($resultat_sql);
	
		for ($i=0; $i<$nbrlignes_producteur_photo; $i++) {
			$producteur_photo_id[$i] = mysql_result($resultat_sql,$i,"id_photo");
			$sqlquery2 = "SELECT id,fichier, width_miniature, height_miniature, width, height FROM photos WHERE id=" . $producteur_photo_id[$i];
			$resultat_sql2 = mysql_query($sqlquery2,$connexion);
	
			$producteur_photo_id[$i] = mysql_result($resultat_sql2,0,"id");
			$producteur_photo[$i] = mysql_result($resultat_sql2,0,"fichier");
			$producteur_photo_width[$i] = mysql_result($resultat_sql2,0,"width_miniature");
			$producteur_photo_height[$i] = mysql_result($resultat_sql2,0,"height_miniature");
			$producteur_photo_width2[$i] = mysql_result($resultat_sql2,0,"width");
			$producteur_photo_height2[$i] = mysql_result($resultat_sql2,0,"height");
		}

	$sqlquery= "SELECT id_langue FROM producteur_langue WHERE id_producteur = '".$_GET{'id'}."'";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur_langue = mysql_num_rows($resultat_sql);
	
		for ($i=0; $i<$nbrlignes_producteur_langue; $i++) {
			$producteur_langue_id[$i] = mysql_result($resultat_sql,$i,"id_langue");
			$sqlquery2 = "SELECT id, langue FROM producteurs_langues WHERE id=" . $producteur_langue_id[$i];
			$resultat_sql2 = mysql_query($sqlquery2,$connexion);
	
			$producteur_langue_id[$i] = mysql_result($resultat_sql2,0,"id");
			$producteur_langue[$i] = mysql_result($resultat_sql2,0,"langue");
		}

	$sqlquery= "SELECT id, nom FROM produits WHERE id_producteur=" . $_GET{'id'}." ORDER BY produit_phare, nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur_produits = mysql_num_rows($resultat_sql);
	
		for ($i=0; $i<$nbrlignes_producteur_produits; $i++) {
			$producteur_produit_id[$i] = mysql_result($resultat_sql,$i,"id");
			$producteur_produit[$i] = mysql_result($resultat_sql,$i,"nom");
		}

	$sqlquery= "SELECT id, nom, altitude FROM exploitations WHERE id_producteur=" . $_GET{'id'}." ORDER BY alpage, nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur_exploitations = mysql_num_rows($resultat_sql);
	
		for ($i=0; $i<$nbrlignes_producteur_exploitations; $i++) {
			$producteur_exploitation_id[$i] = mysql_result($resultat_sql,$i,"id");
			$producteur_exploitation[$i] = mysql_result($resultat_sql,$i,"nom");
			$altitude = mysql_result($resultat_sql,$i,"altitude");
			if ($altitude != "") {
				$producteur_exploitation[$i] .= " (alpage : ".$altitude."m)";
			}
		}

	$sqlquery= "SELECT id, nom FROM activites WHERE id_producteur=" . $_GET{'id'}." ORDER BY nom";
		$resultat_sql = mysql_query($sqlquery,$connexion);
		$nbrlignes_producteur_activites = mysql_num_rows($resultat_sql);
	
		for ($i=0; $i<$nbrlignes_producteur_activites; $i++) {
			$producteur_activite_id[$i] = mysql_result($resultat_sql,$i,"id");
			$producteur_activite[$i] = mysql_result($resultat_sql,$i,"nom");
		}

	mysql_close($connexion);

	$adresse = Array();

	array_push($adresse, "<b>".$nom." ".$prenom."</b>");

	if ($responsable != "") {
		array_push($adresse, "<b>".$responsable."</b>");
	}
	
	if ($rue != "") {
		array_push($adresse, "<b>".$rue."</b>");
	}

	array_push($adresse, "<b>".$no_postal." ".$ville."</b>");

	if ($tel != "") {
		array_push($adresse, "t�l�phone : "."<b>".$tel."</b>");
	}
	
	if ($fax != "") {
		array_push($adresse, "fax : "."<b>".$fax."</b>");
	}
	
	if ($mobile != "") {
		array_push($adresse, "mobile : "."<b>".$mobile."</b>");
	}
	
	if ($email != "") {
		array_push($adresse, "email :<b> <a href=\"mailto:".$email."\">".$email."</a>"."</b>");
	}
	
	if ($site_web != "") {
		array_push($adresse, "site internet :<b> <a href=\"http://".$site_web."\" target=\"_blank\">".$site_web."</a>"."</b>");
	}

	$langue = "";

	for ($i=0; $i<$nbrlignes_producteur_langue; $i++) {
		if ($i != 0) {
			$langue .= ", ";
		}
		$langue .= $producteur_langue[$i];
	}

	if ($langue != "" OR $vente_correspondance == "oui") {
		array_push($adresse, "");
	}

	if ($langue != "") {
		array_push($adresse, "Langues : "."<b>".$langue."</b>");
	}

	if ($vente_correspondance == "oui") {
		array_push($adresse, "<b>Vente par correspondance</b>");
	}

	$photo = "";

	for ($i=0; $i<$nbrlignes_producteur_photo; $i++) {
		if (($i+1) % 3 == 1) {
			$photo .= "  <tr>\n    <td width=\"33%\" valign=\"middle\" align=\"center\">";
		}
		if (($i+1) % 3 == 2) {
			$photo .= "</td>\n    <td width=\"33%\" valign=\"middle\" align=\"center\">";
		}
		if (($i+1) % 3 == 0) {
			$photo .= "</td>\n    <td width=\"33%\" valign=\"middle\" align=\"center\">";
		}
		$photo .= "<a href=\"javascript:photo(".$producteur_photo_id[$i].",".$producteur_photo_width2[$i].",".$producteur_photo_height2[$i].")\"><img src=\"../produits/photos/thumbs/".$producteur_photo[$i]."\" width=\"".$producteur_photo_width[$i]."\" height=\"".$producteur_photo_height[$i]."\" border=\"0\"></a>";
		if (($i+1) % 3 == 0) {
			$photo .= "</td>\n  </tr>\n";
		}
	}

	if ($i % 3 == 1) {
		$photo .= "</td>\n    <td></td>\n    <td></td>\n  </tr>\n";
	}
	if ($i % 3 == 2) {
		$photo .= "</td>\n    <td></td>\n  </tr>\n";
	}

	if ($photo != "") {
		$photo = "Photo(s): <br>\n<ul><table borer=\"0\" width=\"100%\">\n".$photo."</table></ul>";
	}

?>
<html>

	<head>
		<meta http-equiv="content-type" content="text/html; charset=windows-1252">
		<title>Pays-dEnhaut.ch - Producteurs/commerces</title>
		<script language="JavaScript">
			function photo(id,largeur,hauteur) {
				var top=(screen.height-(hauteur+100))/2;
				var left=(screen.width-(largeur+40))/2;

				window.open("photo.php?id="+id, "photo", "top="+top+", left="+left+", toolbar=no, status=yes, scrollbars=yes, resizable=yes, width="+(largeur+40)+", height="+(hauteur+100));
			}
		</script>
	</head>

	<body bgcolor="<?php echo $couleur_fond;?>" topmargin="0" leftmargin="0" text="<?php echo $couleur_texte;?>" link="<?php echo $couleur_link;?>" vlink="<?php echo $couleur_vlink;?>" alink="<?php echo $couleur_alink;?>">

		<table border="0" cellpadding="7" width="<?php echo $width_page;?>" cellspacing="0">
			<tr bgcolor="<?php echo $couleur_tableau1;?>">
				<td>
					<h3><b><u><font face="<?php echo $police;?>" color="<?php echo $couleur_texte;?>"><?php echo $nom." ".$prenom; ?></font></u></b></h3>
				</td>
			</tr>
			<tr>
				<td>
<?php
	if (($logo != "")) {
		echo "      <table border=\"0\" width=\"100%\" cellpadding=\"5\">\n";
		echo "        <tr>\n";
		echo "          <td><img src=\"".$chemin_logos_pepa_http.$logo."\"></td>\n";
		echo "        </tr>\n";
		echo "      </table>\n";
	}

	if ($description_courte != "") {
		echo "      <p align=\"justify\"><i><font face=\"$police\" color=\"$couleur_texte\">". $description_courte."</font></i></p>\n";
	}

	if ($description != "") {
		echo "      <p align=\"justify\"><b><font face=\"$police\" color=\"$couleur_texte\">".$description."</font></b></p>\n";
	}

	if ($photo != "") {
		echo "      <p align=\"justify\"><b><font face=\"$police\" color=\"$couleur_texte\">".$photo."</font></b></p>\n";
	}

	if ($nbrlignes_producteur_produits > 0) {
		echo "      <p align=\"justify\"><b><font face=\"$police\" color=\"$couleur_texte\">Nos produits :</font></b></p>\n";
		echo "      <ul><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n";

		for($i=0; $i<$nbrlignes_producteur_produits; $i++) {
			if ($i % 2 == 0) {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau1\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"produit.php?id=".$producteur_produit_id[$i]."\">".$producteur_produit[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
			else {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau2\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"produit.php?id=".$producteur_produit_id[$i]."\">".$producteur_produit[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
		}

		echo "      </table></ul>\n";
	}

	if ($nbrlignes_producteur_exploitations > 0) {
		echo "      <p align=\"justify\"><b><font face=\"$police\" color=\"$couleur_texte\">Nos exploitations :</font></b></p>\n";
		echo "      <ul><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n";

		for($i=0; $i<$nbrlignes_producteur_exploitations; $i++) {
			if ($i % 2 == 0) {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau1\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"exploitation.php?id=".$producteur_exploitation_id[$i]."\">".$producteur_exploitation[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
			else {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau2\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"exploitation.php?id=".$producteur_exploitation_id[$i]."\">".$producteur_exploitation[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
		}

		echo "      </table></ul>\n";
	}

	if ($nbrlignes_producteur_activites > 0) {
		echo "      <p align=\"justify\"><b><font face=\"$police\" color=\"$couleur_texte\">Nos activit�s :</font></b></p>\n";
		echo "      <ul><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n";

		for($i=0; $i<$nbrlignes_producteur_activites; $i++) {
			if ($i % 2 == 0) {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau1\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"activite.php?id=".$producteur_activite_id[$i]."\">".$producteur_activite[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
			else {
				echo "        <tr>\n";
				echo "          <td bgcolor=\"$couleur_tableau2\"><b><font face=\"$police\" color=\"$couleur_texte\"><a href=\"activite.php?id=".$producteur_activite_id[$i]."\">".$producteur_activite[$i]."</a></font></b>&nbsp;</td>\n";
				echo "        </tr>\n";
			}
		}

		echo "      </table></ul>\n";
	}
?>

					<br><b><font face="<?php echo $police; ?>" color="<?php echo $couleur_texte; ?>">Contact :</b></font><br><br>

					<ul><table border="0" cellspacing="0" cellpadding="2" width="100%">
						<tr><td bgcolor="<?php echo $couleur_tableau1; ?>"><font face="<?php echo $police; ?>" color="<?php echo $couleur_texte; ?>">
<?php

	for($i=0; $i<count($adresse); $i++) {
		echo "							".$adresse[$i]."<br>\n";
	}

?>
						</font></td></tr>
					</table></ul>
				</td>
			</tr>
		</table>
	</body>
</html>