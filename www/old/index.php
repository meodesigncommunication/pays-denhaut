<?php 
$site = getenv("SERVER_NAME");
?>
<html>
<head>
<base href="http://www.demonstration.ch">
<title><?=$site?> - Infomaniak Network SA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#09254B" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="780" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="63" colspan="3"><a href="http://web.infomaniak.ch" target="_self"><img src="http://www.demonstration.ch/img.php?site=<?=$site?>" width="780" height="63" border="0"></a></td>
  </tr>
  <tr> 
    <td width="5" background="img/background_cotegauche.gif">&nbsp;</td>
    <td width="708" valign="top" bgcolor="#E5E5E5">
<p>&nbsp;</p>
      <table width="560" height="268" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="140" height="115">&nbsp;</td>
          <td width="420"> 
            <table width="420" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><img src="img/tableau-haut-bienvenue.gif" width="420" height="22"></td>
              </tr>
              <tr> 
                <td background="img/background-tableau-bienvenue.gif"><div align="center"><br>
                    <strong><span class="Arial12Noir">Ce site est actuellement 
                    en construction.<br>
                    N'h&eacute;sitez pas &agrave; venir le visiter prochainement 
                    !</span></strong> <br>
                    <br>
                  </div></td>
              </tr>
              <tr> 
                <td><img src="img/tableau-bas-bienvenue.gif" width="420" height="2"></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td height="19" colspan="2">&nbsp;</td>
        </tr>
        <tr> 
          <td height="134" colspan="2" valign=top><table width="560" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="560"><img src="img/tableau-haut-proprietaire.gif" width="560" height="22"></td>
              </tr>
              <tr> 
                <td background="img/background-tableau-propriet.gif">
				<img src="/img/espaceur.gif" height=22 width=1>
                    <table width="100%" height="468" border="0" cellpadding="0" cellspacing="0">
                    <tr> 
                      <td width="29">&nbsp;</td>
                      <td width="92"><a href="http://start.infomaniak.ch" target="_blank"><img src="img/img_start.gif" width="92" height="114" border="0"></a></td>
                      <td width="23">&nbsp;</td>
                      <td valign="top"><table width="100%" height="114" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height=21><img src="img/petit-tableau-haut-miseenro.gif" width="383"></td>
                          </tr>
                          <tr> 
                            <td align="center" valign="top" background="img/background-petit-tableau.gif"><table width="352" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top" class="Tahoma10Noir" height=15></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Cette 
                                    page vous expliquera pas &agrave; pas la mise 
                                    en service de votre site Internet en 6 &eacute;tapes.</td>
                                </tr>
                                <tr> 
                                  <td height=6></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Lisez-la 
                                    attentivement, elle vous &eacute;pargnera 
                                    un temps pr&eacute;cieux.</td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr> 
                            <td height="8"><img src="img/petit-tableau-bas.gif" width="383"></td>
                          </tr>
                        </table></td>
                      <td width="33">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td colspan="5" height=26></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td valign="top"><a href="http://admin.infomaniak.ch" target="_blank"><img src="img/img_pageperso.gif" width="92" height="114" border="0"></a></td>
                      <td>&nbsp;</td>
                      <td valign="top"><table width="100%" height="114" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height=24><img src="img/petit-tableau-haut-personna.gif" width="383" height="24"></td>
                          </tr>
                          <tr> 
                            <td align="center" valign="top" background="img/background-petit-tableau.gif"><table width="352" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top" class="Tahoma10Noir" height=5></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">V&eacute;rifiez 
                                    bien que la page d'accueil de votre site web 
                                    s'appelle<strong> index.html</strong> ou <strong>index.php</strong>.</td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir" height=6></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Pour supprimer 
                                    l'affichage de cette page, vous devez effacer 
                                    le fichier index.php qui se trouve &agrave; 
                                    la racine de votre site.</td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir" height=6></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Par d&eacute;faut, 
                                    les extensions Frontpage sont d&eacute;sactiv&eacute;es. 
                                    Si vous d&eacute;sirez activer ces extensions 
                                    nous vous invitons &agrave; en faire la demande 
                                    en vous rendant sur la console d'administration. 
                                  </td>
                                </tr>
                              </table> </td>
                          </tr>
                          <tr> 
                            <td height="8"><img src="img/petit-tableau-bas.gif" width="383"></td>
                          </tr>
                        </table></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td colspan="5" height=26></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td valign="top"><a href="http://admin.infomaniak.ch" target="_blank"><img src="img/img_admin.gif" width="92" height="114" border="0"></a></td>
                      <td>&nbsp;</td>
                      <td valign="top"><table width="100%" height="114" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height=24><img src="img/petit-tableau-haut-consoled.gif" width="383" height="24"></td>
                          </tr>
                          <tr> 
                            <td align="center" valign="top" background="img/background-petit-tableau.gif"><table width="352" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir" height=5></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Cette 
                                    interface permet la cr&eacute;ation et suppression 
                                    de vos adresses e-mail et comptes FTP, la 
                                    gestion de vos mailings listes, conna&icirc;tre 
                                    le nombre de visiteurs (statistiques), faire 
                                    une sauvegarde de votre site, etc.</td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir" height=6></td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Nous vous 
                                    recommandons d'ajouter cette page dans vos 
                                    favoris afin que vous puissiez y acc&eacute;der 
                                    plus facilement.</td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr> 
                            <td height="8"><img src="img/petit-tableau-bas.gif" width="383"></td>
                          </tr>
                        </table></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td colspan="5" height=26></td>
                    </tr>
                    <tr> 
                      <td height="19">&nbsp;</td>
                      <td valign="top"><a href="http://support.infomaniak.ch" target="_blank"><img src="img/img_support.gif" width="92" height="114" border="0"></a></td>
                      <td>&nbsp;</td>
                      <td valign="top"><table width="100%" height="114" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height=24><img src="img/petit-tableau-haut-supportt.gif" width="383" height="24"></td>
                          </tr>
                          <tr> 
                            <td align="center" valign="middle" background="img/background-petit-tableau.gif">
<table width="352" border="0" cellspacing="0" cellpadding="0">
                               
                                <tr> 
                                  <td valign="top" class="Tahoma10Noir">Si vous 
                                    avez un probl&egrave;me sp&eacute;cifique 
                                    ou des questions concernant la mise en ligne 
                                    de votre site Internet nous r&eacute;pondons 
                                    instantan&eacute;ment &agrave; vos questions 
                                    du lundi au vendredi de 9h &agrave; 18h.</td>
                                </tr>
                              </table>
							 </td>
                          </tr>
                          <tr> 
                            <td height="8"><img src="img/petit-tableau-bas.gif" width="383"></td>
                          </tr>
                        </table></td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
				  <img src="/img/espaceur.gif" height=22 width=1>
                    </td>
              </tr>
              <tr> 
                <td><img src="img/tableau-bas-proprietaire.gif" width="560" height="2"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
      <p align="center"><a href="http://www.infomaniak.ch">web.infomaniak.ch</a> <a href="http://www.hebergeur.ch">www.hebergeur.ch</a> <a href="http://www.infomaniak.fr">www.infomaniak.fr</a> <a href="http://www.infomaniak.be">www.infomaniak.be</a> <a href="http://www.infomaniak.com">www.infomaniak.com</a></p>
    </td>
    <td width="67" valign="top"><img src="img/banner-infomaniak-mihaut.gif" width="15" height="71" class="Tahoma10Noir"></td>
  </tr>
</table>
</body>
</html>
